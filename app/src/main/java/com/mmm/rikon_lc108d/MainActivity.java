package com.mmm.rikon_lc108d;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/*import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;*/
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.mmm.rikon_lc108d.BLE.DeviceControlActivity;
import com.mmm.rikon_lc108d.activity.LoginActivity;
import com.mmm.rikon_lc108d.activity.QustomDialogBuilder;
import com.mmm.rikon_lc108d.activity.RegisterActivity;
import com.mmm.rikon_lc108d.activity.SplashActivity;
import com.mmm.rikon_lc108d.db.Login_Table;

import java.io.File;
import java.util.ArrayList;

import static android.content.ContentValues.TAG;
import static com.mmm.rikon_lc108d.LC108DApplication.PREF_DEVICE_ADDRESS;
import static com.mmm.rikon_lc108d.LC108DApplication.PREF_DEVICE_CONNECTION_STATUS;
import static com.mmm.rikon_lc108d.LC108DApplication.PREF_DEVICE_CONNECTION_STATUS_KEY;
import static com.mmm.rikon_lc108d.LC108DApplication.PREF_DEVICE_DETAILS;
import static com.mmm.rikon_lc108d.LC108DApplication.PREF_DEVICE_NAME;

public class MainActivity extends AppCompatActivity  implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks{
    private static final int REQUEST_CODE_PERMISSION = 100;
    //   implements NavigationView.OnNavigationItemSelectedListener {

    public TextView name_device = null, text_device_address, text_device_status;

    private ListView mListView;

    private static final int REQUEST_ENABLE_BT = 1;
    // Stops scanning after 10 seconds.
    private static final long SCAN_PERIOD = 10000;
    public static String FIND_CLASS = "find_class";
    private String sub_class;
    private LeDeviceListAdapter mLeDeviceListAdapter;
    private BluetoothAdapter mBluetoothAdapter;
    private boolean mScanning;
    private Handler mHandler;


    // Device scan callback.
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {

                @Override
                public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mLeDeviceListAdapter.addDevice(device);
                            mLeDeviceListAdapter.notifyDataSetChanged();
                        }
                    });
                }
            };

    private Button mLogoutm,mAddUser;

    public void clearApplicationData() {
        File cache = getCacheDir();
        File appDir = new File(cache.getParent());
        if (appDir.exists()) {
            String[] children = appDir.list();
            for (String s : children) {
                if (!s.equals("lib")) {
                    deleteDir(new File(appDir, s));
                    Log.i("TAG", "**************** File /data/data/APP_PACKAGE/" + s + " DELETED *******************");
                }
            }
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Marshmallow+
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,


            }, REQUEST_CODE_PERMISSION);

        } else {
            // Pre-Marshmallow
        }


        GoogleApiClient mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .enableAutoManage(this, 34992, this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        locationChecker(mGoogleApiClient, MainActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mAddUser = (Button) findViewById(R.id.add_user);
        mLogoutm = (Button) findViewById(R.id.logout);

        SharedPreferences sharedpreferences_login = getSharedPreferences(LC108DApplication.PREF_LOGIN_DETAILS, Context.MODE_PRIVATE);
        String login_status = sharedpreferences_login.getString(LC108DApplication.PREF_LOGIN_STATUS, "");
        String user_type = sharedpreferences_login.getString(LC108DApplication.PREF_LOGIN_USER_TYPE, "");
        String login_user = sharedpreferences_login.getString(LC108DApplication.PREF_LOGIN_USER, "");

        TextView tv = (TextView) findViewById(R.id.login_user_name);
        tv.setText(user_type+" : "+login_user);

        if(user_type.equals(Login_Table.USER)) {
            mAddUser.setVisibility(View.GONE);
        }

        mAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
                alertDialog.setTitle("Password");
                alertDialog.setMessage("Enter Password");

                final EditText input = new EditText(MainActivity.this);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);

                alertDialog.setView(input);

                alertDialog.setPositiveButton("YES",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String password = input.getText().toString();

                                if ("rcs2017".equals(password)) {
                                    Toast.makeText(getApplicationContext(),
                                            "Password Matched", Toast.LENGTH_SHORT).show();

                                      Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                                      intent.putExtra("user_type", Login_Table.USER);
                                      startActivity(intent);


                                } else {
                                    Toast.makeText(getApplicationContext(),
                                            "Wrong Password!", Toast.LENGTH_SHORT).show();

                                }
                            }
                        });

                alertDialog.setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                alertDialog.show();





            }
        });

        mLogoutm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedpreferences_login = getSharedPreferences(LC108DApplication.PREF_LOGIN_DETAILS, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor_login = sharedpreferences_login.edit();
                editor_login.clear();
                editor_login.putString(LC108DApplication.PREF_LOGIN_STATUS, "");
                editor_login.putString(LC108DApplication.PREF_LOGIN_USER, "");
                editor_login.putString(LC108DApplication.PREF_LOGIN_USER_TYPE, "");
                editor_login.commit();
                Toast.makeText(getApplicationContext(), " Logout LC108D ", Toast.LENGTH_SHORT).show();
                // clearApplicationData();
                finish();
//                System.exit(0);
                Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                startActivity(intent);
            }
        });

//        try {
//            String destinationFile = Environment.getExternalStorageDirectory().getAbsolutePath().toString()+"/LC108D/2017_1_2_3336.txt";
//            chmod(destinationFile, 0644);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        name_device = (TextView) findViewById(R.id.name_device);
//        name_device.setText("Device Name :"+Common.DEVICE_NAME);


        text_device_address = (TextView) findViewById(R.id.txt_device_address);
//        text_device_address.setText("Device Address : "+ Common.cDEVICE_ADDRESS);

        text_device_status = (TextView) findViewById(R.id.txt_device_status);
        //   text_device_status.setText("Device Status : "+Common.cDEVICE_STATUS);

        SharedPreferences sharedpreferences = getSharedPreferences(PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
        String device_name = sharedpreferences.getString(PREF_DEVICE_NAME, "");
        String device_address = sharedpreferences.getString(PREF_DEVICE_ADDRESS, "");

        name_device.setText("Device name : " + device_name);
        text_device_address.setText("Device Address : " + device_address);

        SharedPreferences sharedpreferences_ = getSharedPreferences(PREF_DEVICE_CONNECTION_STATUS, Context.MODE_PRIVATE);
        String connect_state = sharedpreferences_.getString(PREF_DEVICE_CONNECTION_STATUS_KEY, "");
        //  text_device_status.setText(connect_state);

        Button mRefresh = (Button) findViewById(R.id.refresh);

        mRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedpreferences = getSharedPreferences(PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
                String device_name = sharedpreferences.getString(PREF_DEVICE_NAME, "");
                String device_address = sharedpreferences.getString(PREF_DEVICE_ADDRESS, "");
                name_device.setText("Device name : " + device_name);
                text_device_address.setText("Device Address : " + device_address);

                SharedPreferences sharedpreferences_ = getSharedPreferences(PREF_DEVICE_CONNECTION_STATUS, Context.MODE_PRIVATE);
                String connect_state = sharedpreferences_.getString(PREF_DEVICE_CONNECTION_STATUS_KEY, "");
                text_device_status.setText(connect_state);
            }
        });

//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
//                R.string.navigation_drawer_open,
//                R.string.navigation_drawer_close);
//        drawer.setDrawerListener(toggle);
//        toggle.syncState();
//
//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);

        mListView = (ListView) findViewById(R.id.device_scan_list);
        mHandler = new Handler();
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }

        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        if (mBluetoothAdapter == null) {
            Toast.makeText(this, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

    }

    public static void locationChecker(GoogleApiClient mGoogleApiClient, final Activity activity) {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    activity, 1000);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        //scanLeDevice(false);
        mLeDeviceListAdapter.clear();
    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    invalidateOptionsMenu();
                }
            }, SCAN_PERIOD);

            mScanning = true;
            mBluetoothAdapter.startLeScan(mLeScanCallback);
        } else {
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
        invalidateOptionsMenu();
    }

//    @Override
//    protected void onStop() {
//        super.onStop();
//        SharedPreferences sharedpreferences = getSharedPreferences(LC108DApplication.PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor_seq_no = sharedpreferences.edit();
//        editor_seq_no.clear();
//        editor_seq_no.putString(LC108DApplication.PREF_DEVICE_NAME, "");
//        editor_seq_no.putString(LC108DApplication.PREF_DEVICE_ADDRESS, "");
//        editor_seq_no.commit();
//
//        SharedPreferences device_status = getSharedPreferences(LC108DApplication.PREF_DEVICE_CONNECTION_STATUS, Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor_ = device_status.edit();
//        editor_.clear();
//        editor_.putString(LC108DApplication.PREF_DEVICE_CONNECTION_STATUS_KEY, "");
//        editor_.commit();
//    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!mBluetoothAdapter.isEnabled()) {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }

        scanLeDevice(true);

        // Initializes list view adapter.
        mLeDeviceListAdapter = new MainActivity.LeDeviceListAdapter();
        mListView.setAdapter(mLeDeviceListAdapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final BluetoothDevice device = mLeDeviceListAdapter.getDevice(position);
                if (device == null) return;
                Intent intent = null;


                intent = new Intent(getApplicationContext(), DeviceControlActivity.class);

                Log.v(TAG, "sssss0 mDeviceName :" + device.getName() + "mDeviceAddress : " + device.getAddress());
                SharedPreferences sharedpreferences = getSharedPreferences(LC108DApplication.PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor_seq_no = sharedpreferences.edit();
                editor_seq_no.clear();
                editor_seq_no.putString(LC108DApplication.PREF_DEVICE_NAME, device.getName());
                editor_seq_no.putString(LC108DApplication.PREF_DEVICE_ADDRESS, device.getAddress());
                editor_seq_no.commit();
                SharedPreferences sharedpreferences1 = getSharedPreferences(LC108DApplication.PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
                String mDeviceName = sharedpreferences1.getString(LC108DApplication.PREF_DEVICE_NAME, "");
                String mDeviceAddress = sharedpreferences1.getString(LC108DApplication.PREF_DEVICE_ADDRESS, "");
                Log.v(TAG, "sssss mDeviceName :" + mDeviceName + "mDeviceAddress : " + mDeviceAddress);
                intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_NAME, device.getName());
                intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_ADDRESS, device.getAddress());
                if (mScanning) {
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    mScanning = false;
                }
                startActivity(intent);
            }
        });

        final TextView name_device = (TextView) findViewById(R.id.name_device);
        //  name_device.setText("Device Name : "+Common.DEVICE_NAME);
        final TextView text_device_address = (TextView) findViewById(R.id.txt_device_address);
        //text_device_address.setText("Device Address : "+ Common.cDEVICE_ADDRESS);

        final TextView text_device_status = (TextView) findViewById(R.id.txt_device_status);
//        text_device_status.setText("Device Status : "+Common.cDEVICE_STATUS);

        SharedPreferences sharedpreferences = getSharedPreferences(PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
        String device_name = sharedpreferences.getString(PREF_DEVICE_NAME, "");
        String device_address = sharedpreferences.getString(PREF_DEVICE_ADDRESS, "");

        name_device.setText("Device name : " + device_name);
        text_device_address.setText("Device Address : " + device_address);

        SharedPreferences sharedpreferences_ = getSharedPreferences(PREF_DEVICE_CONNECTION_STATUS, Context.MODE_PRIVATE);
        String connect_state = sharedpreferences_.getString(PREF_DEVICE_CONNECTION_STATUS_KEY, "");
        //   text_device_status.setText(connect_state);

        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {

            @Override
            public void run() {
                System.out.println("After 2 secs");

                SharedPreferences sharedpreferences = getSharedPreferences(PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
                String device_name = sharedpreferences.getString(PREF_DEVICE_NAME, "");
                String device_address = sharedpreferences.getString(PREF_DEVICE_ADDRESS, "");
                name_device.setText("Device name : " + device_name);
                text_device_address.setText("Device Address : " + device_address);

                SharedPreferences sharedpreferences_ = getSharedPreferences(PREF_DEVICE_CONNECTION_STATUS, Context.MODE_PRIVATE);
                String connect_state = sharedpreferences_.getString(PREF_DEVICE_CONNECTION_STATUS_KEY, "");
                text_device_status.setText(connect_state);
                handler.removeCallbacks(this);
                handler.postDelayed(this, 10000);
            }
        };
        handler.postDelayed(runnable, 10000);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // User chose not to enable Bluetooth.
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            finish();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


//    @SuppressWarnings("StatementWithEmptyBody")
//    @Override
//    public boolean onNavigationItemSelected(MenuItem item) {
//        // Handle navigation view item clicks here.
//        int id = item.getItemId();
//
//        if (id == R.id.nav_home) {
//            // Handle the camera action
//            Intent i = new Intent(getApplicationContext(), DeviceScanActivity.class);
//            i.putExtra(DeviceScanActivity.FIND_CLASS, "home");
//            startActivity(i);
//        } else if (id == R.id.nav_switch) {
//            Intent i = new Intent(getApplicationContext(), DeviceScanActivity.class);
//            i.putExtra(DeviceScanActivity.FIND_CLASS, "switch");
//            startActivity(i);
//        } else if (id == R.id.nav_log_request) {
//            Intent i = new Intent(getApplicationContext(), DeviceScanActivity.class);
//            i.putExtra(DeviceScanActivity.FIND_CLASS, "log_request");
//            startActivity(i);
//        } else if (id == R.id.nav_manage) {
//            Intent i = new Intent(getApplicationContext(), MobileDetails.class);
//            startActivity(i);
//        } else if (id == R.id.nav_mode) {
//            Intent i = new Intent(getApplicationContext(), ModeActivity.class);
//            startActivity(i);
//        }
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
//        return true;
//    }

    public void updateMessageStatus(String mMessage) {
        Log.v("QQQQM", " String Message for update : " + mMessage);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharedPreferences sharedpreferences = getSharedPreferences(LC108DApplication.PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor_seq_no = sharedpreferences.edit();
        editor_seq_no.clear();
        editor_seq_no.putString(LC108DApplication.PREF_DEVICE_NAME, "");
        editor_seq_no.putString(LC108DApplication.PREF_DEVICE_ADDRESS, "");
        editor_seq_no.commit();

        SharedPreferences device_status = getSharedPreferences(LC108DApplication.PREF_DEVICE_CONNECTION_STATUS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor_ = device_status.edit();
        editor_.clear();
        editor_.putString(LC108DApplication.PREF_DEVICE_CONNECTION_STATUS_KEY, "");
        editor_.commit();

       // scanLeDevice(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        if (!mScanning) {
            menu.findItem(R.id.menu_stop).setVisible(false);
            menu.findItem(R.id.menu_scan).setVisible(true);
            menu.findItem(R.id.menu_refresh).setActionView(null);
        } else {
            menu.findItem(R.id.menu_stop).setVisible(true);
            menu.findItem(R.id.menu_scan).setVisible(false);
            menu.findItem(R.id.menu_refresh).setActionView(
                    R.layout.actionbar_indeterminate_progress);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_scan:
                mLeDeviceListAdapter.clear();
                scanLeDevice(true);
                break;
            case R.id.menu_stop:
                scanLeDevice(false);
                break;
            case R.id.menu_help:
                QustomDialogBuilder qustomDialogBuilder1 = new QustomDialogBuilder(this).
                        setTitle("Help").
                        setTitleColor("#000000").
                        setTitleBackgroundColor("#1AD9EA").
                        setCustomView(R.layout.help, this);

                qustomDialogBuilder1.show();
                break;
            case R.id.menu_about:

                QustomDialogBuilder qustomDialogBuilder = new QustomDialogBuilder(this).
                        setTitle("About").
                        setTitleColor("#000000").
                        setTitleBackgroundColor("#1AD9EA").
                        setCustomView(R.layout.about, this);

                qustomDialogBuilder.show();

                break;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return true;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    /* @Override
     public void onConnected(@Nullable Bundle bundle) {

     }

     @Override
     public void onConnectionSuspended(int i) {

     }

     @Override
     public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

     }
 */
    static class ViewHolder {
        TextView deviceName;
        TextView deviceAddress;
        Button mConnect;
    }

    // Adapter for holding devices found through scanning.
    private class LeDeviceListAdapter extends BaseAdapter {
        private ArrayList<BluetoothDevice> mLeDevices;
        private LayoutInflater mInflator;

        public LeDeviceListAdapter() {
            super();
            mLeDevices = new ArrayList<BluetoothDevice>();
            mInflator = MainActivity.this.getLayoutInflater();
        }

        public void addDevice(BluetoothDevice device) {
            if (!mLeDevices.contains(device)) {
                mLeDevices.add(device);
            }
        }

        public BluetoothDevice getDevice(int position) {
            return mLeDevices.get(position);
        }

        public void clear() {
            mLeDevices.clear();
        }

        @Override
        public int getCount() {
            return mLeDevices.size();
        }

        @Override
        public Object getItem(int i) {
            return mLeDevices.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            MainActivity.ViewHolder viewHolder;
            // General ListView optimization code.
            if (view == null) {
                view = mInflator.inflate(R.layout.listitem_device, null);
                viewHolder = new MainActivity.ViewHolder();
                viewHolder.deviceAddress = (TextView) view.findViewById(R.id.device_address);
                viewHolder.deviceName = (TextView) view.findViewById(R.id.device_name);

                viewHolder.mConnect = (Button) view.findViewById(R.id.btn_connect);
                view.setTag(viewHolder);
            } else {
                viewHolder = (MainActivity.ViewHolder) view.getTag();
            }

            BluetoothDevice device = mLeDevices.get(i);
            final String deviceName = device.getName();
            if (deviceName != null && deviceName.length() > 0)
                viewHolder.deviceName.setText(deviceName);
            else
                viewHolder.deviceName.setText(R.string.unknown_device);
            viewHolder.deviceAddress.setText(device.getAddress());

            viewHolder.mConnect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final BluetoothDevice device = mLeDeviceListAdapter.getDevice(i);
                    if (device == null) return;
                    Intent intent = null;

                    intent = new Intent(getApplicationContext(), DeviceControlActivity.class);
                    Log.v(TAG, "UUUUU mConnect LC108DApplication.FLAG_REQUEST_DEVICE_STATUS = 1.");
                    LC108DApplication.FLAG_REQUEST_DEVICE_STATUS = 1;

                    Log.v(TAG, "sssss0 mDeviceName :" + device.getName() + "mDeviceAddress : " + device.getAddress());
                    SharedPreferences sharedpreferences = getSharedPreferences(LC108DApplication.PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor_seq_no = sharedpreferences.edit();
                    editor_seq_no.clear();
                    editor_seq_no.putString(LC108DApplication.PREF_DEVICE_NAME, device.getName());
                    editor_seq_no.putString(LC108DApplication.PREF_DEVICE_ADDRESS, device.getAddress());
                    editor_seq_no.commit();
                    SharedPreferences sharedpreferences1 = getSharedPreferences(LC108DApplication.PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
                    String mDeviceName = sharedpreferences1.getString(LC108DApplication.PREF_DEVICE_NAME, "");
                    String mDeviceAddress = sharedpreferences1.getString(LC108DApplication.PREF_DEVICE_ADDRESS, "");
                    Log.v(TAG, "sssss mDeviceName :" + mDeviceName + "mDeviceAddress : " + mDeviceAddress);
                    intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_NAME, device.getName());
                    intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_ADDRESS, device.getAddress());
//                    if (mScanning) {
//                        mBluetoothAdapter.stopLeScan(mLeScanCallback);
//                        mScanning = false;
//                    }
                    startActivity(intent);
                }
            });
            return view;
        }
    }
}