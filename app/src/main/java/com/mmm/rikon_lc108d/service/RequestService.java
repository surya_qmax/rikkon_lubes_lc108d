package com.mmm.rikon_lc108d.service;

import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.mmm.rikon_lc108d.BLE.DeviceControlActivity;
import com.mmm.rikon_lc108d.db.MobileDetailsTable;
import com.mmm.rikon_lc108d.db.ModeTimeTable;
import com.mmm.rikon_lc108d.db.Switch_Status_Table;

import static com.mmm.rikon_lc108d.db.ModeTimeTable.PRESSURE_LIMIT;
import static com.mmm.rikon_lc108d.db.ModeTimeTable.WHEEL_COUNT;

/**
 * Created by MMM on 11/21/2016.
 */

public class RequestService extends Service {
    private static String LOG_TAG = "BoundService";
    private IBinder mBinder = new RequestService.MyBinder();
    public Context mCtx;

    @Override
    public void onCreate() {
        super.onCreate();
        mCtx = getApplicationContext();
        Log.v(LOG_TAG, "in onCreate");
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.v(LOG_TAG, "in onBind");
        return mBinder;
    }

    @Override
    public void onRebind(Intent intent) {
        Log.v(LOG_TAG, "in onRebind");
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.v(LOG_TAG, "in onUnbind");
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.v(LOG_TAG, "UUUUU in onDestroy");
    }





    public void setWheelsPerDayPassed(final byte[] bytes, final Context context)
    {
        final Context ctx = context;



        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(final Void... params) {
                try {
                    StringBuilder sb1 = new StringBuilder();
                    for (byte bb : bytes) {
                        if (sb1.length() > 0) {
                            sb1.append(':');
                        }sb1.append(String.format("%02x", bb));
                    }

                    String max_wheels_limit="10000";

                    int maxmsb = bytes[2];
                    int maxlsb = bytes[3];

                    if(maxlsb<0)
                    {
                        maxlsb = maxlsb + 256;
                    }

                    if(maxmsb<0)
                    {
                        maxmsb = maxmsb + 256;
                    }




                    Log.v("MMMEE", " UUUUUU max msb wheels per day : "+ maxmsb + " max lsb wheels per day : "+ maxlsb);
                    int maxresult = (maxmsb << 8) + maxlsb;

                    Log.v("MMMEE", " UUUUUU max msb wheels per day : "+ maxmsb + " max lsb wheels per day : "+ maxlsb + " max result wheels per day : "+ maxresult);
                    max_wheels_limit = String.valueOf(maxresult);



                    String min_wheels_limit="1";

                    int minmsb = bytes[4];
                    int minlsb = bytes[5];

                    if(minlsb<0)
                    {
                        minlsb = minlsb + 256;
                    }

                    if(minmsb<0)
                    {
                        minmsb = minmsb + 256;
                    }



                    Log.v("MMMEE", " UUUUUU msb min wheels per day : "+ minmsb + " min lsb wheels per day : "+ minlsb);
                    int minresult = (minmsb << 8) + minlsb;

                    Log.v("MMMEE", " UUUUUU min msb wheels per day : "+ minmsb + " min lsb wheels per day: "+ minlsb + " result min wheels per day: "+ minresult);
                    min_wheels_limit = String.valueOf(minresult);

                    Log.v("UUUU", "UUUU Request service setModeStatus wheels per day (bytes); : "+ sb1);



                    String wheels_passed="1";

                    int wheelsPassedmsb = bytes[6];
                    int wheelsPassedlsb = bytes[7];

                    if(wheelsPassedlsb<0)
                    {
                        wheelsPassedlsb = wheelsPassedlsb + 256;
                    }

                    if(wheelsPassedmsb<0)
                    {
                        wheelsPassedmsb = wheelsPassedmsb + 256;
                    }

                    Log.v("MMMEE", " UUUUUU msb passed wheels per day: "+ wheelsPassedmsb + " passed lsb wheels per day: "+ wheelsPassedlsb);
                    int wheelsPassedresult = (wheelsPassedmsb << 8) + wheelsPassedlsb;

                    Log.v("MMMEE", " UUUUUU msb passed wheels per day: "+ wheelsPassedmsb + " lsb passed wheels per day: "+ wheelsPassedlsb + " result : wheels per day"+ wheelsPassedresult);
                    wheels_passed = String.valueOf(wheelsPassedresult);

                    Log.v("UUUU", "UUUU Request service setModeStatus (bytes); : "+ sb1);

                    String pressureLimit = "0";

                    int msb = bytes[8];
                    int lsb = bytes[9];
                    if(lsb<0)
                    {
                        lsb = lsb + 256;
                    }

                    if(msb<0)
                    {
                        msb = msb + 256;
                    }

                    Log.v("MMMEE", " UUUUUU msb : "+ msb + " lsb : "+ lsb);
                    int result = (msb << 8) + lsb;

                    Log.v("MMMEE", " UUUUUU msb : "+ msb + " lsb : "+ lsb + " result : "+ result);
                    pressureLimit = String.valueOf(result);

                    /*Cursor checkExists = getContentResolver().
                            query(ModeTimeTable.CONTENT_URI,
                                    null,
                                    ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.TRACK_MODE},
                                    null);

                    String  getStatusReset = "false";

                    if (checkExists != null && checkExists.moveToFirst()) {

                            getStatusReset = checkExists.getString(checkExists.getColumnIndex(ModeTimeTable.RESET_WHEELS_PASSED));

                            Log.v("LC108D-LOG", "UUUUU 2 ModeTimeTable updated mode reset val"+getStatusReset);


                    }

                    else
                    {
                        getStatusReset = checkExists.getString(checkExists.getColumnIndex(ModeTimeTable.RESET_WHEELS_PASSED));

                        Log.v("LC108D-LOG", "UUUUU 2 ModeTimeTable updated mode reset val"+getStatusReset);

                    }*/
                    Log.v("LC108D-LOG", "UUUUU 2 ModeTimeTable updated mode wheels per day"+wheels_passed);




                    ContentValues values = new ContentValues();
                    values.put(ModeTimeTable.WHEELS_PER_DAY_MAX_LIMIT, max_wheels_limit);
                    values.put(ModeTimeTable.WHEELS_PER_DAY_MIN_LIMIT, min_wheels_limit );
                    values.put(ModeTimeTable.TOTAL_WHEELS_PASSED_PER_DAY, wheels_passed);
                    values.put(ModeTimeTable.RESET_WHEELS_PASSED,"false");
                    values.put(ModeTimeTable.PRESSURE_LIMIT,pressureLimit);


                    ctx.getContentResolver().update(ModeTimeTable.CONTENT_URI, values, ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.TRACK_MODE});
                    Log.v("LC108D-LOG", "UUUUU 2 ModeTimeTable updated");



                    Cursor valuetest = ctx.getContentResolver().
                            query(ModeTimeTable.CONTENT_URI,
                                    null,
                                    ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.TRACK_MODE},
                                    null);

                    if (valuetest != null && valuetest.moveToFirst()) {

                        String getStatuswheelspassed = valuetest.getString(valuetest.getColumnIndex(ModeTimeTable.TOTAL_WHEELS_PASSED_PER_DAY));

                        Log.v("LC108D-LOG", "UUUUU 2 ModeTimeTable updated mode reset val"+getStatuswheelspassed);


                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(final Void aVoid) {
                Log.v("LC108D-LOG", "UUUUU onPostExecute");

            }
        }.execute();
        ///////////////////////////////////////////////////////////////////////////////////////////////



    }



    public void setModeStatus(final byte[] bytes, final Context context) {

        final Context ctx = context;

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(final Void... params) {
                try {
                        StringBuilder sb1 = new StringBuilder();
                        for (byte bb : bytes) {
                            if (sb1.length() > 0) {
                                sb1.append(':');
                            }
                            sb1.append(String.format("%02x", bb));
                        }

                        Log.v("UUUU", "UUUU Request service setModeStatus (bytes); : "+ sb1);
                        int getSelectedModeValue = bytes[2];
                        Log.v("UUUU", "UUUU Request service getSelectedModeValue (bytes[3]); : "+ getSelectedModeValue);
                        String changeModeName = "";
                        String strTime = "";
                        String pressureLimit = "0";
                        String wheelCount = "0";
                        if(getSelectedModeValue == 1) {
                            changeModeName = ModeTimeTable.PROGRESSIVE_MODE;
                        } else if(getSelectedModeValue == 2) {
                            changeModeName = ModeTimeTable.TRACK_MODE;
                            int msb = bytes[8];
                            int lsb = bytes[9];
                            Log.v("MMMEE", " UUUUUU msb : "+ msb + " lsb : "+ lsb);

                            if(lsb<0)
                            {
                                lsb = lsb + 256;
                            }

                            if(msb<0)
                            {
                                msb = msb + 256;
                            }

                            int result = (msb << 8) + lsb;


                            Log.v("MMMEE ", " wheelcount UUUUUU msb : "+ msb + " lsb : "+ lsb + " result : "+ result);
                            wheelCount = String.valueOf(result);
                        } else if(getSelectedModeValue == 3) {
                            changeModeName = ModeTimeTable.INJECTOR_MODE;


                            int msb = bytes[8];
                            int lsb = bytes[9];
                            if(lsb<0)
                            {
                                lsb = lsb + 256;
                            }

                            if(msb<0)
                            {
                                msb = msb + 256;
                            }

                            Log.v("MMMEE", " UUUUUU msb : "+ msb + " lsb : "+ lsb);
                            int result = (msb << 8) + lsb;

                            Log.v("MMMEE", " UUUUUU msb : "+ msb + " lsb : "+ lsb + " result : "+ result);
                            pressureLimit = String.valueOf(result);
                        } else if(getSelectedModeValue == 4) {
                            changeModeName = ModeTimeTable.DUAL_LINE_MODE;
                        }

                        //getTime
                        String StringonMin = "", StringonSec = "", StringoffHour = "", StringOffMin = "", StringOffSec = "";
                        int onMin = bytes[3];
                        int lengthOnMin = String.valueOf(onMin).length();
                        Log.v("UUUUU", " lengthOnMin length : "+ lengthOnMin + " value : "+onMin);
                        if(lengthOnMin==1) {
                            StringonMin = "0"+String.valueOf(onMin);
                        } else StringonMin = String.valueOf(onMin);
                    Log.v("UUUUU", "Final Value onMin : "+ StringonMin);
                    int onSec = bytes[4];
                        int lengthOnSec = String.valueOf(onSec).length();
                    Log.v("UUUUU", " lengthOnSec : "+ lengthOnSec+" value : "+onSec);
                        if(lengthOnSec==1) {
                            StringonSec = "0"+String.valueOf(onSec);
                        } else StringonSec = String.valueOf(onSec);
                    Log.v("UUUUU", "Final Value onSec : "+ StringonSec);
                        int offHours = bytes[5];
                        int lengthOffHour = String.valueOf(offHours).length();
                    Log.v("UUUUU", " lengthOffHour : "+ lengthOffHour+ " value : "+offHours);
                        if(lengthOffHour==1) {
                            StringoffHour = "0"+String.valueOf(offHours);
                        } else StringoffHour = String.valueOf(offHours);
                    Log.v("UUUUU", "Final Value offHour : "+ StringoffHour);
                        int offMin = bytes[6];
                        int lengthoffMin = String.valueOf(offMin).length();
                    Log.v("UUUUU", " lengthoffMin : "+ lengthoffMin+" value : "+offMin);
                        if(lengthoffMin==1) {
                            StringOffMin = "0"+String.valueOf(offMin);
                        } else StringOffMin = String.valueOf(offMin);
                    Log.v("UUUUU", "Final Value OffMin : "+ StringOffMin);
                        int offSec = bytes[7];
                        int lengthoffSec = String.valueOf(offSec).length();
                    Log.v("UUUUU", " lengthoffMin : "+ lengthoffSec+" value : "+offSec);
                        if(lengthoffSec==1) {
                            StringOffSec = "0"+String.valueOf(offSec);
                        } else StringOffSec = String.valueOf(offSec);
                    Log.v("UUUUU", "Final Value OffSec : "+ StringOffSec);
                        strTime = "00" + StringonMin + StringonSec +
                                StringoffHour + StringOffMin + StringOffSec;
                        Log.v("MMMEE", " UUUUUU strOnOffTime { " + strTime);

                    if (changeModeName.equals(ModeTimeTable.INJECTOR_MODE)) {
                        ///Pressure Switch
                        boolean getPressureSwitch = false;
                        if (bytes[7] == 0x01) {
                            getPressureSwitch = true;
                        }
                        ContentValues values = new ContentValues();
                        values.put(Switch_Status_Table.PRESSURE_SWITCH,getPressureSwitch );

                        String where = Switch_Status_Table.MODE_NAME + " = '" + changeModeName + "'";

                        ctx.getContentResolver().update(Switch_Status_Table.CONTENT_URI, values, where, null);
                        Log.v("LC108D-LOG", "UUUUU Pressure switch update : ");
                    }

                    boolean getMobileOneStatus = false;
                        boolean getMobileTwoStatus = false;
                        boolean getMobileThreeStatus = false;
                        boolean getMobileFourStatus = false;

                        if(bytes[11] == 0x01) {
                            getMobileOneStatus = true;
                        }
                        if(bytes[12] == 0x01) {
                            getMobileTwoStatus = true;
                        }
                        if(bytes[13] == 0x01) {
                            getMobileThreeStatus = true;
                        }
                        if(bytes[14] == 0x01) {
                            getMobileFourStatus = true;
                        }

                        Log.v("LC108D-LOG", "UUUUU Mobile settings getMobileOneStatus : "+ getMobileOneStatus);
                        Log.v("LC108D-LOG", "UUUUU Mobile settings getMobileTwoStatus : "+ getMobileTwoStatus);
                        Log.v("LC108D-LOG", "UUUUU Mobile settings getMobileThreeStatus : "+ getMobileThreeStatus);
                        Log.v("LC108D-LOG", "UUUUU Mobile settings getMobileFourStatus : "+ getMobileFourStatus);

                        Log.v("UUUUU", " Mode Name : "+ changeModeName);
                        Log.v("UUUUU", " Mode STATUS : "+ true);
                        Log.v("UUUUU", " wheelCount Count : "+ wheelCount);
                        Log.v("UUUUU", " PRESSURE_LIMIT Count : "+ wheelCount);
                    Log.v("UUUUU", " PRESSURE_LIMIT Time : "+ strTime);

//            ContentValues values4 = new ContentValues();
//            values4.put(ModeTimeTable.STATUS, false);
//            getContentResolver().update(ModeTimeTable.CONTENT_URI, values4, ModeTimeTable.MODE_NAME + "=?", new String[]{changeModeName});
//            Log.v("LC108D-LOG", "UUUUU 1 ModeTimeTable updated");

//                        ContentValues contentValues = new ContentValues();
//                        contentValues.put(ModeTimeTable.MODE_NAME, changeModeName);
//                        contentValues.put(ModeTimeTable.TIME, "000000000000");
//                        contentValues.put(WHEEL_COUNT, "0");
//                        contentValues.put(PRESSURE_LIMIT, "0");
//                        contentValues.put(ModeTimeTable.STATUS, true);
//                    mCtx.getContentResolver().update(ModeTimeTable.CONTENT_URI, contentValues, ModeTimeTable.MODE_NAME + "=?", new String[]{changeModeName});

//                        Log.v("LC108D-LOG", "UUUUU 1 ModeTimeTable updated");

                    ContentValues values1 = new ContentValues();
                    values1.put(ModeTimeTable.STATUS, false );
                    ctx.getContentResolver().update(ModeTimeTable.CONTENT_URI, values1, null, null);

                    Log.v("mmm", "Mode Name if checkwheelcountvalue 111: " + wheelCount);

                    ContentValues values = new ContentValues();
                        values.put(ModeTimeTable.MODE_NAME, changeModeName);
                        values.put(ModeTimeTable.STATUS, true );
                        values.put(ModeTimeTable.TIME, strTime);
                        values.put(ModeTimeTable.WHEEL_COUNT, wheelCount);
                        values.put(PRESSURE_LIMIT, pressureLimit);

                    ctx.getContentResolver().update(ModeTimeTable.CONTENT_URI, values, ModeTimeTable.MODE_NAME + "=?", new String[]{changeModeName});
                        Log.v("LC108D-LOG", "UUUUU 2 ModeTimeTable updated"+changeModeName);

                    Cursor mCur = ctx.getContentResolver().query(ModeTimeTable.CONTENT_URI,
                            null,
                            null, null,
                            null);

                    String checkwheelcountvalue = "";



                    if (mCur != null && mCur.moveToFirst()) {
                        do {
                            checkwheelcountvalue = mCur.getString(mCur.getColumnIndex(ModeTimeTable.WHEEL_COUNT));

                            Log.v("mmm", "Mode Name if checkwheelcountvalue : " + checkwheelcountvalue);

                        } while (mCur.moveToNext());
                    }



                    //Mobile Configuration
                        ContentValues values_mobile_one = new ContentValues();
                        values_mobile_one.put(MobileDetailsTable.MOBILE_LINE_STATUS, getMobileOneStatus);
                    ctx.getContentResolver().update(MobileDetailsTable.CONTENT_URI, values_mobile_one,
                                MobileDetailsTable.MOBILE_LINE + "=?", new String[]{MobileDetailsTable.MOBILE_LINE_ONE});
                        Log.v("LC108D-LOG", "UUUUU MobileDetailsTable getMobileOneStatus updated");

                    ContentValues values_mobile_two = new ContentValues();
                    values_mobile_two.put(MobileDetailsTable.MOBILE_LINE_STATUS, getMobileTwoStatus);
                    ctx.getContentResolver().update(MobileDetailsTable.CONTENT_URI, values_mobile_two,
                            MobileDetailsTable.MOBILE_LINE + "=?", new String[]{MobileDetailsTable.MOBILE_LINE_TWO});
                        Log.v("LC108D-LOG", "UUUUU MobileDetailsTable getMobileTwoStatus updated");

                    ContentValues values_mobile_three = new ContentValues();
                    values_mobile_three.put(MobileDetailsTable.MOBILE_LINE_STATUS, getMobileThreeStatus);
                    ctx.getContentResolver().update(MobileDetailsTable.CONTENT_URI, values_mobile_three,
                                MobileDetailsTable.MOBILE_LINE + "=?", new String[]{MobileDetailsTable.MOBILE_LINE_THREE});
                        Log.v("LC108D-LOG", "UUUUU MobileDetailsTable getMobileThreeStatus updated");

                        ContentValues values_mobile_four = new ContentValues();
                    values_mobile_four.put(MobileDetailsTable.MOBILE_LINE_STATUS, getMobileFourStatus);
                    ctx.getContentResolver().update(MobileDetailsTable.CONTENT_URI, values_mobile_four,
                                MobileDetailsTable.MOBILE_LINE + "=?", new String[]{MobileDetailsTable.MOBILE_LINE_FOUR});

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(final Void aVoid) {
                Log.v("LC108D-LOG", "UUUUU onPostExecute");

//                Toast.makeText(getApplicationContext(), "Update Mode state and mobile settings", Toast.LENGTH_SHORT).show();

               DeviceControlActivity ObjControlActivity = new DeviceControlActivity();
               ObjControlActivity.requestSwitchStatus(ctx);
            }
        }.execute();
        ///////////////////////////////////////////////////////////////////////////////////////////////
    }

    public void setSwitchStatus(byte[] switchStatus_, Context context) {
        Log.v("LC108D-LOG", "UUUUU setSwitchStatus");
        final Context ctx = context;
        final byte[] switchStatus = switchStatus_;

        //////////////////////////////////////////////////////////////////////////////////////////////

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(final Void... params) {

                try{

                    StringBuilder sb2=new StringBuilder();
                    for (byte bb : switchStatus) {
                        if (sb2.length() > 0) {
                            sb2.append(':');
                        }
                        sb2.append(String.format("%02x", bb));
                    }

                    Log.v("LC108D-LOG", "UUUU Request service setSwitchStatus (bytes-32); : "+ sb2);

                Cursor mCur = ctx.getContentResolver().query(ModeTimeTable.CONTENT_URI,
                        null,
                        null, null,
                        null);

                String nameMode = "";

                if (mCur != null && mCur.moveToFirst()) {
                    do {
                        boolean getStatus = mCur.getInt(mCur.getColumnIndex(ModeTimeTable.STATUS)) > 0;
                        Log.v("mmm", "Mode Name getStatus : " + getStatus);
                        //TO get the
                        if (getStatus) {
                            nameMode = mCur.getString(mCur.getColumnIndex(ModeTimeTable.MODE_NAME));
                            Log.v("mmm", "Mode Name if : " + nameMode);
                        } else {
                            Log.v("mmm", "Mode Name else : " + nameMode);
                        }
                    } while (mCur.moveToNext());
                }

                Log.v("mmm", "Mode Name : " + nameMode);
                boolean switch1 = false;
                if (switchStatus[3]== 0x01)
                    switch1 = true;

                boolean switch2 = false;
                if (switchStatus[4]== 0x01)
                    switch2 = true;

                boolean switch3 = false;
                if (switchStatus[5]== 0x01)
                    switch3 = true;

                boolean switch4 = false;
                if (switchStatus[6]== 0x01)
                    switch4 = true;

                boolean switch5 = false;
                if (switchStatus[7]== 0x01) switch5 = true;

                boolean switch6 = false;
                if (switchStatus[8]== 0x01) switch6 = true;

                boolean switch7 = false;
                if (switchStatus[9]== 0x01) switch7 = true;

                boolean switch8 = false;
                if (switchStatus[10]== 0x01) switch8 = true;

                boolean switch9 = false;
                if (switchStatus[11]== 0x01) switch9 = true;

                boolean switch10 = false;
                if (switchStatus[12]== 0x01) switch10 = true;

                boolean switch_proximity = false;
                if (switchStatus[13]== 0x01) switch_proximity = true;

              /*  boolean switch_pressure = false;
                if (switchStatus[14]== 0x01) switch_pressure = true;
*/
               /* boolean switch_wheel = false;
                if (switchStatus[14]== 0x01) switch_wheel = true;

              */      boolean switch_wheel_daily_status = false;
                    if (switchStatus[14]== 0x01) switch_wheel_daily_status = true;




                    ContentValues values = new ContentValues();
                values.put(Switch_Status_Table.LIMIT_SWITCH1, switch1);
                values.put(Switch_Status_Table.LIMIT_SWITCH2, switch2);
                values.put(Switch_Status_Table.LIMIT_SWITCH3, switch3);
                values.put(Switch_Status_Table.LIMIT_SWITCH4, switch4);
                values.put(Switch_Status_Table.LIMIT_SWITCH5, switch5);
                values.put(Switch_Status_Table.LIMIT_SWITCH6, switch6);
                values.put(Switch_Status_Table.LIMIT_SWITCH7, switch7);
                values.put(Switch_Status_Table.LIMIT_SWITCH8, switch8);
                values.put(Switch_Status_Table.LIMIT_SWITCH9, switch9);
                values.put(Switch_Status_Table.LIMIT_SWITCH10, switch10);
                values.put(Switch_Status_Table.PROXIMITY_SWITCH, switch_proximity);
              //  values.put(Switch_Status_Table.PRESSURE_SWITCH, switch_pressure);
                values.put(Switch_Status_Table.DALIY_WHEEL_REPORT,switch_wheel_daily_status);
//        values.put(Switch_Status_Table.WHEEL_SENSOR, switch_wheel);

                String where = Switch_Status_Table.MODE_NAME + " = '" + nameMode + "'";
                ctx.getContentResolver().update(Switch_Status_Table.CONTENT_URI, values, where, null);
                Log.v("LC108D-LOG", "UUUUU Switch status updated"+switch1+switch2+switch3+switch4+switch5
                                        +switch6+switch7+switch8+switch9+switch10+switch_proximity);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(final Void aVoid) {
                Log.v("LC108D-LOG", "UUUUU onPostExecute switch status ");

//                Toast.makeText(getApplicationContext(), "Update Mode state and mobile settings", Toast.LENGTH_SHORT).show();

                DeviceControlActivity OBJControlActivity = new DeviceControlActivity();
                OBJControlActivity.sendRTCtime(ctx);

            }
        }.execute();

        ////////////////////////////////////////////////////////////////////////////////////////////

    }

    public class MyBinder extends Binder {
        RequestService getService() {
            return RequestService.this;
        }
    }
}