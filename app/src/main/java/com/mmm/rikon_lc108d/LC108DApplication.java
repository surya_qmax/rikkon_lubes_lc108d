package com.mmm.rikon_lc108d;


import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.multidex.MultiDexApplication;
import android.util.Log;
import android.widget.Toast;

import com.mmm.rikon_lc108d.activity.LogActivity;
import com.mmm.rikon_lc108d.db.MobileDetailsTable;
import com.mmm.rikon_lc108d.db.ModeTimeTable;
import com.mmm.rikon_lc108d.db.Switch_Status_Table;

import static com.mmm.rikon_lc108d.db.ModeTimeTable.PRESSURE_LIMIT;
import static com.mmm.rikon_lc108d.db.ModeTimeTable.RESET_WHEELS_PASSED;
import static com.mmm.rikon_lc108d.db.ModeTimeTable.SETTING_PASSED_PER_DAY;
import static com.mmm.rikon_lc108d.db.ModeTimeTable.TOTAL_WHEELS_PASSED_PER_DAY;
import static com.mmm.rikon_lc108d.db.ModeTimeTable.WHEELS_PER_DAY_MAX_LIMIT;
import static com.mmm.rikon_lc108d.db.ModeTimeTable.WHEELS_PER_DAY_MIN_LIMIT;
import static com.mmm.rikon_lc108d.db.ModeTimeTable.WHEEL_COUNT;

/**
 * Created by Buvaneswari on 6/1/16.
 */

public class LC108DApplication extends MultiDexApplication {

    public static final String PREF_DEVICE_DETAILS = "pref_device_details";
    public static final String PREF_DEVICE_NAME = "pref_device_name";
    public static final String PREF_DEVICE_ADDRESS = "pref_device_address";
    public static final String PREF_DEVICE_CONNECTION_STATUS = "pref_device_connection_status";
    public static final String PREF_DEVICE_CONNECTION_STATUS_KEY = "pref_device_connection_status_key";
    public static final String STATUS_CONNECTED = "CONNECTED";
    public static final String STATUS_DISCONNECTED = "DISCONNECTED";
    private static final String TAG = LC108DApplication.class.getSimpleName();

    public static final String PREF_LOGIN_DETAILS = "pref_login_details";
    public static final String PREF_LOGIN_STATUS = "pref_login_status";
    public static final String PREF_LOGIN_USER = "pref_login_user";
    public static final String PREF_LOGIN_USER_TYPE = "pref_user_type";

    public static int FLAG_REQUEST_DEVICE_STATUS = 0;
    public static String FLAG_REQUEST_MODE_NAME = "";

    @Override
    public void onCreate() {
        super.onCreate();
        checkDatabase();
    }

    private void checkDatabase() {
        /**
         * Is first time install app to store setting default specification in table
         */
        SharedPreferences prefs = getSharedPreferences(getApplicationContext().getResources().getString(R.string.firsttimeprefs), MODE_PRIVATE);
        boolean firstRun = prefs.getBoolean(getApplicationContext().getResources().getString(R.string.isfirsttime), true);
        if (firstRun) {
            Log.d(TAG, "Application Installed at first time");

            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean(getApplicationContext().getResources().getString(R.string.isfirsttime), false);
            editor.apply();
            addShortcut();

            SharedPreferences sharedpreferences_login = getSharedPreferences(PREF_LOGIN_DETAILS, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor_login = sharedpreferences_login.edit();
            editor_login.clear();
            editor_login.putString(PREF_LOGIN_STATUS, "");
            editor_login.putString(PREF_LOGIN_USER_TYPE, "");
            editor_login.putString(PREF_LOGIN_USER, "");
            editor_login.commit();

            SharedPreferences sharedpreferences = getSharedPreferences(PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor_seq_no = sharedpreferences.edit();
            editor_seq_no.clear();
            editor_seq_no.putString(PREF_DEVICE_NAME, "");
            editor_seq_no.putString(PREF_DEVICE_ADDRESS, "");
            editor_seq_no.putString(PREF_DEVICE_CONNECTION_STATUS, "");
            editor_seq_no.commit();

            SharedPreferences device_status = getSharedPreferences(PREF_DEVICE_CONNECTION_STATUS, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor_ = device_status.edit();
            editor_.clear();
            editor_.putString(PREF_DEVICE_CONNECTION_STATUS_KEY, "");
            editor_.commit();

            //insert data
            ContentValues values = new ContentValues();
            values.put(Switch_Status_Table.LIMIT_SWITCH1, false);
            values.put(Switch_Status_Table.LIMIT_SWITCH2, false);
            values.put(Switch_Status_Table.LIMIT_SWITCH3, false);
            values.put(Switch_Status_Table.LIMIT_SWITCH4, false);
            values.put(Switch_Status_Table.LIMIT_SWITCH5, false);
            values.put(Switch_Status_Table.LIMIT_SWITCH6, false);
            values.put(Switch_Status_Table.LIMIT_SWITCH7, false);
            values.put(Switch_Status_Table.LIMIT_SWITCH8, false);
            values.put(Switch_Status_Table.LIMIT_SWITCH9, false);
            values.put(Switch_Status_Table.LIMIT_SWITCH10, false);
            values.put(Switch_Status_Table.PROXIMITY_SWITCH, false);
            values.put(Switch_Status_Table.PRESSURE_SWITCH, false);
//            values.put(Switch_Status_Table.WHEEL_SENSOR, false);
            values.put(Switch_Status_Table.MODE_NAME, ModeTimeTable.PROGRESSIVE_MODE);

            Uri uri_insert = null;
            try {
                uri_insert = getContentResolver().insert(Switch_Status_Table.CONTENT_URI, values);
                Log.v(TAG + "LC108D-LOG", "Insert switch");
            } catch (Exception e) {

            }

            //insert data
            ContentValues values1 = new ContentValues();
            values1.put(Switch_Status_Table.LIMIT_SWITCH1, false);
            values1.put(Switch_Status_Table.LIMIT_SWITCH2, false);
            values1.put(Switch_Status_Table.LIMIT_SWITCH3, false);
            values1.put(Switch_Status_Table.LIMIT_SWITCH4, false);
            values1.put(Switch_Status_Table.LIMIT_SWITCH5, false);
            values1.put(Switch_Status_Table.LIMIT_SWITCH6, false);
            values1.put(Switch_Status_Table.LIMIT_SWITCH7, false);
            values1.put(Switch_Status_Table.LIMIT_SWITCH8, false);
            values1.put(Switch_Status_Table.LIMIT_SWITCH9, false);
            values1.put(Switch_Status_Table.LIMIT_SWITCH10, false);
            values1.put(Switch_Status_Table.PROXIMITY_SWITCH, false);
            values1.put(Switch_Status_Table.PRESSURE_SWITCH, false);
            values1.put(Switch_Status_Table.DALIY_WHEEL_REPORT,false);

//            values1.put(Switch_Status_Table.WHEEL_SENSOR, false);
            values1.put(Switch_Status_Table.MODE_NAME, ModeTimeTable.TRACK_MODE);

            try {
                getContentResolver().insert(Switch_Status_Table.CONTENT_URI, values1);
                Log.v(TAG + "LC108D-LOG", "Insert switch");
            } catch (Exception e) {

            }

            //insert data
            ContentValues values2 = new ContentValues();
            values2.put(Switch_Status_Table.LIMIT_SWITCH1, false);
            values2.put(Switch_Status_Table.LIMIT_SWITCH2, false);
            values2.put(Switch_Status_Table.LIMIT_SWITCH3, false);
            values2.put(Switch_Status_Table.LIMIT_SWITCH4, false);
            values2.put(Switch_Status_Table.LIMIT_SWITCH5, false);
            values2.put(Switch_Status_Table.LIMIT_SWITCH6, false);
            values2.put(Switch_Status_Table.LIMIT_SWITCH7, false);
            values2.put(Switch_Status_Table.LIMIT_SWITCH8, false);
            values2.put(Switch_Status_Table.LIMIT_SWITCH9, false);
            values2.put(Switch_Status_Table.LIMIT_SWITCH10, false);
            values2.put(Switch_Status_Table.PROXIMITY_SWITCH, false);
            values2.put(Switch_Status_Table.PRESSURE_SWITCH, false);
//            values2.put(Switch_Status_Table.WHEEL_SENSOR, false);
            values2.put(Switch_Status_Table.MODE_NAME, ModeTimeTable.INJECTOR_MODE);

            try {
                getContentResolver().insert(Switch_Status_Table.CONTENT_URI, values2);
                Log.v(TAG + "LC108D-LOG", "Insert switch");
            } catch (Exception e) {

            }

            //insert data
            ContentValues values3 = new ContentValues();
            values3.put(Switch_Status_Table.LIMIT_SWITCH1, true);
            values3.put(Switch_Status_Table.LIMIT_SWITCH2, true);
            values3.put(Switch_Status_Table.LIMIT_SWITCH3, false);
            values3.put(Switch_Status_Table.LIMIT_SWITCH4, false);
            values3.put(Switch_Status_Table.LIMIT_SWITCH5, false);
            values3.put(Switch_Status_Table.LIMIT_SWITCH6, false);
            values3.put(Switch_Status_Table.LIMIT_SWITCH7, false);
            values3.put(Switch_Status_Table.LIMIT_SWITCH8, false);
            values3.put(Switch_Status_Table.LIMIT_SWITCH9, false);
            values3.put(Switch_Status_Table.LIMIT_SWITCH10, false);
            values3.put(Switch_Status_Table.PROXIMITY_SWITCH, false);
            values3.put(Switch_Status_Table.PRESSURE_SWITCH, false);
//            values3.put(Switch_Status_Table.WHEEL_SENSOR, false);
            values3.put(Switch_Status_Table.MODE_NAME, ModeTimeTable.DUAL_LINE_MODE);

            try {
                getContentResolver().insert(Switch_Status_Table.CONTENT_URI, values3);
                Log.v(TAG + "LC108D-LOG", "Insert switch");
            } catch (Exception e) {
            }

            ContentValues contentValues = new ContentValues();
            contentValues.put(ModeTimeTable.MODE_NAME, ModeTimeTable.PROGRESSIVE_MODE);
            contentValues.put(ModeTimeTable.TIME, "000000000000");
            contentValues.put(WHEEL_COUNT, "0");
            contentValues.put(PRESSURE_LIMIT, "0");

            contentValues.put(ModeTimeTable.STATUS, true);

            try {
                getContentResolver()
                        .insert(ModeTimeTable.CONTENT_URI, contentValues);
                Log.v(TAG + "LC108D-LOG", "Insert");
            } catch (Exception e) {
            }

            ContentValues contentValues2 = new ContentValues();
            contentValues2.put(ModeTimeTable.MODE_NAME, ModeTimeTable.TRACK_MODE);
            contentValues2.put(ModeTimeTable.TIME, "000000000000");
            contentValues2.put(WHEEL_COUNT, "0");
            contentValues2.put(PRESSURE_LIMIT, "0");
            contentValues2.put(WHEELS_PER_DAY_MIN_LIMIT,1);
            contentValues2.put(WHEELS_PER_DAY_MAX_LIMIT,10000);
            contentValues2.put(RESET_WHEELS_PASSED,false);
            contentValues2.put(SETTING_PASSED_PER_DAY,false);
            contentValues2.put(TOTAL_WHEELS_PASSED_PER_DAY,0);
            contentValues2.put(ModeTimeTable.STATUS, false);
            contentValues2.put(ModeTimeTable.BOARD_SERIAL_NO,"000000");
            try {
                getContentResolver()
                        .insert(ModeTimeTable.CONTENT_URI, contentValues2);
                Log.v(TAG + "LC108D-LOG", "Insert");
            } catch (Exception e) {
            }

            ContentValues contentValues3 = new ContentValues();
            contentValues3.put(ModeTimeTable.MODE_NAME, ModeTimeTable.INJECTOR_MODE);
            contentValues3.put(ModeTimeTable.TIME, "000000000000");
            contentValues3.put(WHEEL_COUNT, "0");
            contentValues3.put(PRESSURE_LIMIT, "0");
            contentValues3.put(ModeTimeTable.STATUS, false);

            try {
                getContentResolver()
                        .insert(ModeTimeTable.CONTENT_URI, contentValues3);
                Log.v(TAG + "LC108D-LOG", "Insert");
            } catch (Exception e) {
            }

            ContentValues contentValues4 = new ContentValues();
            contentValues4.put(ModeTimeTable.MODE_NAME, ModeTimeTable.DUAL_LINE_MODE);
            contentValues4.put(ModeTimeTable.TIME, "000000000000");
            contentValues4.put(WHEEL_COUNT, "0");
            contentValues4.put(PRESSURE_LIMIT, "0");
            contentValues4.put(ModeTimeTable.STATUS, false);

            try {
                getContentResolver()
                        .insert(ModeTimeTable.CONTENT_URI, contentValues4);
                Log.v("LC108D-LOG", "Insert");
            } catch (Exception e) {
            }
        }

        ContentValues values = new ContentValues();
        values.put(MobileDetailsTable.MOBILE_LINE, MobileDetailsTable.MOBILE_LINE_ONE);
        values.put(MobileDetailsTable.MOBILE_NUMBER, "");
        values.put(MobileDetailsTable.MOBILE_LINE_STATUS, false);
        try {
            getContentResolver()
                    .insert(MobileDetailsTable.CONTENT_URI, values);
            Log.v("LC108D-LOG", "Insert");
        } catch (Exception e) {
        }

        ContentValues values2 = new ContentValues();
        values2.put(MobileDetailsTable.MOBILE_LINE, MobileDetailsTable.MOBILE_LINE_TWO);
        values2.put(MobileDetailsTable.MOBILE_NUMBER, "");
        values2.put(MobileDetailsTable.MOBILE_LINE_STATUS, false);
        try {
            getContentResolver()
                    .insert(MobileDetailsTable.CONTENT_URI, values2);
            Log.v("LC108D-LOG", "Insert");
        } catch (Exception e) {
        }
        ContentValues values3 = new ContentValues();
        values3.put(MobileDetailsTable.MOBILE_LINE, MobileDetailsTable.MOBILE_LINE_THREE);
        values3.put(MobileDetailsTable.MOBILE_NUMBER, "");
        values3.put(MobileDetailsTable.MOBILE_LINE_STATUS, false);
        try {
            getContentResolver()
                    .insert(MobileDetailsTable.CONTENT_URI, values3);
            Log.v("LC108D-LOG", "Insert");
        } catch (Exception e) {
        }

        ContentValues values4 = new ContentValues();
        values4.put(MobileDetailsTable.MOBILE_LINE, MobileDetailsTable.MOBILE_LINE_FOUR);
        values4.put(MobileDetailsTable.MOBILE_NUMBER, "");
        values4.put(MobileDetailsTable.MOBILE_LINE_STATUS, false);
        try {
            getContentResolver()
                    .insert(MobileDetailsTable.CONTENT_URI, values4);
            Log.v("LC108D-LOG", "Insert");
        } catch (Exception e) {
        }
    }

    private void addShortcut() {
        //on Home screen
        Intent shortcutIntent = new Intent(getApplicationContext(), LogActivity.class);
        shortcutIntent.setAction(Intent.ACTION_MAIN);
        Intent addIntent = new Intent();
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getResources().getString(R.string.app_name));
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
                Intent.ShortcutIconResource.fromContext(getApplicationContext(), R.mipmap.ic_launcher));
        addIntent.setAction("com.android.launcher.action.UNINSTALL_SHORTCUT");
        getApplicationContext().sendBroadcast(addIntent);
        addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        getApplicationContext().sendBroadcast(addIntent);
        Log.d(TAG, "Application ShortCut Created");
        Toast.makeText(getApplicationContext(), getString(R.string.app_name) + " Shortcut created", Toast.LENGTH_SHORT).show();
    }
}
