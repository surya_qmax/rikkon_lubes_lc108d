package com.mmm.rikon_lc108d.db;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.mmm.rikon_lc108d.Common;


public class LC108DDBHelper extends SQLiteOpenHelper {

    static String TAG = "LC108DDBHelper.java";
    private static LC108DDBHelper mInstance = null;

    public LC108DDBHelper(Context context, String name,
                          SQLiteDatabase.CursorFactory factory, int version,
                          DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    public static LC108DDBHelper getInstance(Context context) {
        //Create database
        if (mInstance == null) {
            mInstance = new LC108DDBHelper(context, Common.DB_NAME, null, Common.DB_VERSION, null);
            Log.d(TAG, "New Database Object Created");
        } else {
            Log.d(TAG, "Instance not null,Database object already created");
        }
        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //Create all table

        Switch_Status_Table switch_table = new Switch_Status_Table();
        switch_table.createDB(sqLiteDatabase);

        ModeTimeTable mode_table = new ModeTimeTable();
        mode_table.createDB(sqLiteDatabase);

        MobileDetailsTable mobile_details = new MobileDetailsTable();
        mobile_details.createDB(sqLiteDatabase);

        Login_Table login_table = new Login_Table();
        login_table.createDB(sqLiteDatabase);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        //Upgrade all table
        if (oldVersion < newVersion) {

            Switch_Status_Table switch_table = new Switch_Status_Table();
            switch_table.upgradeDB(sqLiteDatabase);

            ModeTimeTable mode_table = new ModeTimeTable();
            mode_table.upgradeDB(sqLiteDatabase);

            MobileDetailsTable mobile_details = new MobileDetailsTable();
            mobile_details.upgradeDB(sqLiteDatabase);

            Login_Table login_table = new Login_Table();
            login_table.upgradeDB(sqLiteDatabase);

            Log.d(TAG, "New Version updated");
        } else
            Log.d(TAG, "Version already updated");
    }
}
