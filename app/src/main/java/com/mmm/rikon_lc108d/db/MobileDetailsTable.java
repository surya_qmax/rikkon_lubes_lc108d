package com.mmm.rikon_lc108d.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

/**
 * Created by MMM on 11/17/2016.
 */

public class MobileDetailsTable extends ContentProvider {

    // fields for my content provider
    public static final String PROVIDER_NAME = "com.mmm.rikon_lc108d.provider.mobile_details";
    public static final String URL = "content://" + PROVIDER_NAME + "/mobile_details_table";
    public static final Uri CONTENT_URI = Uri.parse(URL);

    public static final String TABLE_NAME = "mobile_details_table";
    public static final String MOBILE_LINE = "mobile_line";
    public static final String MOBILE_NUMBER = "mobile_number";
    public static final String MOBILE_LINE_STATUS = "mobile_line_status";


    public static String MOBILE_LINE_ONE = "mobile_line_1";
    public static String MOBILE_LINE_TWO = "mobile_line_2";
    public static String MOBILE_LINE_THREE = "mobile_line_3";
    public static String MOBILE_LINE_FOUR = "mobile_line_4";

    private static String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "( " +
                    MOBILE_LINE + " VARCHAR(50) ," +
                    MOBILE_NUMBER + " VARCHAR(100)," +
                    MOBILE_LINE_STATUS + " BOOLEAN)";

    // mDataBase declarations
    private SQLiteDatabase mDataBase;

    public MobileDetailsTable() {
    }

    public void createDB(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    public void upgradeDB(SQLiteDatabase sqLiteDatabase) {
        String GENERAL_DROP_TABLE = "DROP TABLE" + TABLE_NAME;
        sqLiteDatabase.execSQL(GENERAL_DROP_TABLE);
        createDB(sqLiteDatabase);
    }

    @Override
    public boolean onCreate() {
        // permissions to be writable
        mDataBase = LC108DDBHelper.getInstance(getContext()).getWritableDatabase();
        return mDataBase != null;
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        // the TABLE_NAME to query on
        queryBuilder.setTables(TABLE_NAME);
        Cursor cursor = queryBuilder.query(mDataBase, projection, selection,
                selectionArgs, null, null, null);
        /**
         * register to watch a content URI for changes
         */
        if (getContext() != null)
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        long row = mDataBase.insert(TABLE_NAME, "", values);

        // If record is added successfully
        if (row > 0) {
            Uri newUri = ContentUris.withAppendedId(CONTENT_URI, row);
            if (getContext() != null)
                getContext().getContentResolver().notifyChange(newUri, null);
            Log.v("mmm", "location added");
            return newUri;
        }
        throw new SQLException("Fail to add a new record into " + uri);
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        int count;
        count = mDataBase.update(TABLE_NAME, values, selection, selectionArgs);
        if (getContext() != null)
            getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        int count;
        count = mDataBase.delete(TABLE_NAME, selection, selectionArgs);
        if (getContext() != null)
            getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public String getType(@NonNull Uri uri) {
        // TODO Auto-generated method stub
        return null;
    }
}
