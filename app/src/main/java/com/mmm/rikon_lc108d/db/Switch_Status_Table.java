package com.mmm.rikon_lc108d.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

/**
 * Created by MMM on 8/12/2016.
 */

public class Switch_Status_Table extends ContentProvider {

    public static final String _ID = "_id";

    public static final String LIMIT_SWITCH1 = "limit_switch_1";
    public static final String LIMIT_SWITCH2 = "limit_switch_2";
    public static final String LIMIT_SWITCH3 = "limit_switch_3";
    public static final String LIMIT_SWITCH4 = "limit_switch_4";
    public static final String LIMIT_SWITCH5 = "limit_switch_5";
    public static final String LIMIT_SWITCH6 = "limit_switch_6";
    public static final String LIMIT_SWITCH7 = "limit_switch_7";
    public static final String LIMIT_SWITCH8 = "limit_switch_8";
    public static final String LIMIT_SWITCH9 = "limit_switch_9";
    public static final String LIMIT_SWITCH10 = "limit_switch_10";


    public static final String DALIY_WHEEL_REPORT = "daily_wheel_report";



    public static final String PROXIMITY_SWITCH = "proximity_switch";
    public static final String PRESSURE_SWITCH = "pressure_switch";
//    public static final String WHEEL_SENSOR = "wheel_sensor";

    public static final String MODE_NAME = "mode_name";

    // fields for my content provider
    public static final String PROVIDER_NAME = "com.mmm.rikon_lc108d.provider.status";
    public static final String URL = "content://" + PROVIDER_NAME + "/status_table";
    public static final Uri CONTENT_URI = Uri.parse(URL);

    public static final String TABLE_NAME = "switch_status_table";

    private static String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "( " +
                    _ID + " INTEGER , " +
                    LIMIT_SWITCH1 + " BOOLEAN," +
                    LIMIT_SWITCH2 + " BOOLEAN," +
                    LIMIT_SWITCH3 + " BOOLEAN," +
                    LIMIT_SWITCH4 + " BOOLEAN," +
                    LIMIT_SWITCH5 + " BOOLEAN," +
                    LIMIT_SWITCH6 + " BOOLEAN," +
                    LIMIT_SWITCH7 + " BOOLEAN," +
                    LIMIT_SWITCH8 + " BOOLEAN," +
                    LIMIT_SWITCH9 + " BOOLEAN," +
                    LIMIT_SWITCH10 + " BOOLEAN," +
                    PROXIMITY_SWITCH + " BOOLEAN," +
                    PRESSURE_SWITCH + " BOOLEAN," +
                    MODE_NAME + " BOOLEAN," +
                    DALIY_WHEEL_REPORT + " BOOLEAN)";


    // mDataBase declarations
    private SQLiteDatabase mDataBase;

    public Switch_Status_Table() {
    }

    public void createDB(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    public void upgradeDB(SQLiteDatabase sqLiteDatabase) {
        String GENERAL_DROP_TABLE = "DROP TABLE" + TABLE_NAME;
        sqLiteDatabase.execSQL(GENERAL_DROP_TABLE);
        createDB(sqLiteDatabase);
    }

    @Override
    public boolean onCreate() {
        // permissions to be writable
        mDataBase = LC108DDBHelper.getInstance(getContext()).getWritableDatabase();
        return mDataBase != null;
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        // the TABLE_NAME to query on
        queryBuilder.setTables(TABLE_NAME);
        Cursor cursor = queryBuilder.query(mDataBase, projection, selection,
                selectionArgs, null, null, null);
        /**
         * register to watch a content URI for changes
         */
        if (getContext() != null)
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        long row = mDataBase.insert(TABLE_NAME, "", values);

        // If record is added successfully
        if (row > 0) {
            Uri newUri = ContentUris.withAppendedId(CONTENT_URI, row);
            if (getContext() != null)
                getContext().getContentResolver().notifyChange(newUri, null);
            Log.v("mmm", "location added");
            return newUri;
        }
        throw new SQLException("Fail to add a new record into " + uri);
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        int count;
        count = mDataBase.update(TABLE_NAME, values, selection, selectionArgs);
        if (getContext() != null)
            getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        int count;
        count = mDataBase.delete(TABLE_NAME, selection, selectionArgs);
        if (getContext() != null)
            getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public String getType(@NonNull Uri uri) {
        // TODO Auto-generated method stub
        return null;
    }

    public Cursor getSwitchStatus(String selectedModeName) {
        Cursor contactDetails = null;
        try {
            SQLiteDatabase db = LC108DDBHelper.getInstance(getContext()).getReadableDatabase();
            contactDetails = db.rawQuery("select * from switch_status_table where " + MODE_NAME + " = '" + selectedModeName + "' ", null);

        } catch (Exception e) {
            Log.e("MMM", "findRecordRule Error" + e);
        }
        return contactDetails;
    }
}
