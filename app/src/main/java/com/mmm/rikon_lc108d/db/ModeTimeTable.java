package com.mmm.rikon_lc108d.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import static com.mmm.rikon_lc108d.db.LC108DDBHelper.getInstance;

/**
 * Created by MMM on 11/15/2016.
 */

public class ModeTimeTable extends ContentProvider {

    // fields for my content provider
    public static final String PROVIDER_NAME = "com.mmm.rikon_lc108d.provider.mode";
    public static final String URL = "content://" + PROVIDER_NAME + "/mode_table";
    public static final Uri CONTENT_URI = Uri.parse(URL);

    public static final String TABLE_NAME = "mode_time_table";
    public static final String MODE_NAME = "mode_name";
    public static final String TIME = "on_time";
    public static final String STATUS = "status";
    public static final String WHEEL_COUNT = "wheel_count";
    public static final String PRESSURE_LIMIT = "pressure_limit";

    public static final String WHEELS_PER_DAY_MIN_LIMIT = "wheels_per_day_min_limit";
    public static final String WHEELS_PER_DAY_MAX_LIMIT = "wheels_per_day_max_limit";
    public static final String RESET_WHEELS_PASSED = "reset_wheels_passed";
    public static final String TOTAL_WHEELS_PASSED_PER_DAY="total_wheels_passed_per_day";

    public static final String SETTING_PASSED_PER_DAY="setting_wheels_passed_per_day";
    public static final String BOARD_SERIAL_NO="board_serial_no";


    public static String PROGRESSIVE_MODE = "PROGRESSIVE MODE";
    public static String TRACK_MODE = "TRACK MODE";
    public static String INJECTOR_MODE = "INJECTOR MODE";
    public static String DUAL_LINE_MODE = "DUAL LINE MODE";

    private static String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "( " +
                    MODE_NAME + " VARCHAR(50) ," +
                    TIME + " VARCHAR(100)," +
                    STATUS + " BOOLEAN," +
                    WHEEL_COUNT + " VARCHAR(10)," +
                    PRESSURE_LIMIT + " VARCHAR(10)," +
                    BOARD_SERIAL_NO + " VARCHAR(10)," +
                    WHEELS_PER_DAY_MIN_LIMIT + " VARCHAR(10),"+
                    WHEELS_PER_DAY_MAX_LIMIT + " VARCHAR(10),"+
                    RESET_WHEELS_PASSED + " BOOLEAN,"+
                    TOTAL_WHEELS_PASSED_PER_DAY + " VARCHAR(10),"+
                    SETTING_PASSED_PER_DAY + " BOOLEAN)";

    // mDataBase declarations
    private SQLiteDatabase mDataBase;

    public ModeTimeTable() {
    }

    public void createDB(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    public void upgradeDB(SQLiteDatabase sqLiteDatabase) {
        String GENERAL_DROP_TABLE = "DROP TABLE" + TABLE_NAME;
        sqLiteDatabase.execSQL(GENERAL_DROP_TABLE);
        createDB(sqLiteDatabase);
    }

    @Override
    public boolean onCreate() {
        // permissions to be writable
        mDataBase = getInstance(getContext()).getWritableDatabase();
        return mDataBase != null;
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        // the TABLE_NAME to query on
        queryBuilder.setTables(TABLE_NAME);
        Cursor cursor = queryBuilder.query(mDataBase, projection, selection,
                selectionArgs, null, null, null);
        /**
         * register to watch a content URI for changes
         */
        if (getContext() != null)
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        long row = mDataBase.insert(TABLE_NAME, "", values);

        // If record is added successfully
        if (row > 0) {
            Uri newUri = ContentUris.withAppendedId(CONTENT_URI, row);
            if (getContext() != null)
                getContext().getContentResolver().notifyChange(newUri, null);
            Log.v("mmm", "location added");
            return newUri;
        }
        throw new SQLException("Fail to add a new record into " + uri);
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        int count;
        count = mDataBase.update(TABLE_NAME, values, selection, selectionArgs);
        if (getContext() != null)
            getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        int count;
        count = mDataBase.delete(TABLE_NAME, selection, selectionArgs);
        if (getContext() != null)
            getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    public Cursor getCursor_mode() {
        Cursor cursor_mode = null;
        try {
            SQLiteDatabase db = LC108DDBHelper.getInstance(getContext()).getReadableDatabase();
            cursor_mode = db.rawQuery("select * from mode_time_table", null);

        } catch (Exception e) {
            Log.e("KKK", "findRecordRule Error" + e);
            return null;
        }

        return cursor_mode;
    }
}
