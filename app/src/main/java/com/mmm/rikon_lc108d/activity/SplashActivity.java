package com.mmm.rikon_lc108d.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/*import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;*/
import com.mmm.rikon_lc108d.LC108DApplication;
import com.mmm.rikon_lc108d.MainActivity;
import com.mmm.rikon_lc108d.R;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Locale;


public class SplashActivity extends AppCompatActivity {

    private static String TAG = SplashActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v(TAG, "onCreate");
        setContentView(R.layout.activity_splash);




        Calendar c = Calendar.getInstance();
        DateFormatSymbols dfs=new DateFormatSymbols(Locale.getDefault());
        String week[]=dfs.getWeekdays();

        int dayofmonth=c.get((Calendar.DAY_OF_WEEK));
        Log.d("Dayofmonth","::"+dayofmonth);
        String day=week[dayofmonth];
        Log.d("Nameofday","::"+day);
        SharedPreferences sharedpreferences_login = getSharedPreferences(LC108DApplication.PREF_LOGIN_DETAILS, Context.MODE_PRIVATE);
        String login_status = sharedpreferences_login.getString(LC108DApplication.PREF_LOGIN_STATUS, "");
        String user_type = sharedpreferences_login.getString(LC108DApplication.PREF_LOGIN_USER_TYPE, "");
        String login_user = sharedpreferences_login.getString(LC108DApplication.PREF_LOGIN_USER, "");

        if (!login_status.equals("") && !user_type.equals("")) {
            if(user_type.equals("Admin")) {
                Log.v(TAG, "Admin");
                Intent mainIntent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(mainIntent);
                finish();
            } else if(user_type.equals("User")) {
                Log.v(TAG, "User");
                Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(mainIntent);
                finish();
            }
        } else {
            Intent mainIntent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(mainIntent);
            finish();
        }
    }




}
