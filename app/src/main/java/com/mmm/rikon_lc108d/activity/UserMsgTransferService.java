package com.mmm.rikon_lc108d.activity;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Binder;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;

import com.mmm.rikon_lc108d.BLE.BluetoothLeService;
import com.mmm.rikon_lc108d.db.ModeTimeTable;
import com.mmm.rikon_lc108d.db.Switch_Status_Table;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;

import static com.mmm.rikon_lc108d.BLE.BluetoothLeService.countlineno;
import static com.mmm.rikon_lc108d.db.ModeTimeTable.PRESSURE_LIMIT;
import static com.mmm.rikon_lc108d.db.ModeTimeTable.WHEEL_COUNT;


/**
 * Created by MMM on 3/8/2016.
 */
public class UserMsgTransferService extends Service {

    private final static String TAG = "UserMsgTransferService";
    private static String LOG_TAG = "BoundService";
    private FileWriter fstream = null;
    private BufferedWriter fbw;
    private File input_file = null;
    private IBinder mBinder = new MyBinder();
    private String logmonth;
    private int lineflag ;

    private int newline =0 ;
    private int newline1 = 0;
    private int headerflag = 1;


    @Override
    public void onCreate() {
        super.onCreate();
        Log.v(LOG_TAG, "in onCreate");
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.v(LOG_TAG, "in onBind");
        return mBinder;
    }

    @Override
    public void onRebind(Intent intent) {
        Log.v(LOG_TAG, "in onRebind");
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.v(LOG_TAG, "in onUnbind");
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.v(LOG_TAG, "in onDestroy");
    }


//    public String getByte(byte[] userBytes) {
//        try {
//            byte[] filteredByteArray = Arrays.copyOfRange(userBytes, 2, userBytes.length);
//            Log.v("mmm", " onCharacteristicChanged getByte filteredByteArray value :: " + filteredByteArray);
//
//            StringBuilder sb1 = new StringBuilder();
//
//            for (byte bb : filteredByteArray) {
//                if (sb1.length() > 0) {
//                    sb1.append(':');
//                }
//                sb1.append(String.format("%02x", bb));
//            }
//
//            Log.v("KKKKK", "KKKKa filteredByteArray.getByte(bytes); : "+ sb1);
//
//            StringBuilder s2 = new StringBuilder(100);
//            StringBuilder s3 = new StringBuilder(100);
//
//            for (int i = 0; i < 7; i++) {
//                int occurrences = 0;
//                for (char c : String.valueOf(filteredByteArray[i]).toCharArray()) {
//                    occurrences++;
//                }
//
//                String ss = new String(filteredByteArray, "UTF-8");
//
//                Log.v("mmm", "AAAAAAAAAAAA After filter: :: " + ss);
//                if (occurrences == 2) {
//                    if(i == 1) {
//                        String original_value = String.valueOf(filteredByteArray[i]);
//                        Log.v("mmm", "UUUU original_value filter array i == 1: :: " + original_value);
//                        int original_int_value = Integer.parseInt(original_value);
//                        Log.v("mmm", "UUUU original_int_value filter array i == 1: :: " + original_int_value);
//
//                        s2.append("     " + filteredByteArray[i] + "");
//                    } else {
//                        Log.v("mmm", "UUUU original_value filter array i != 1: :: " );
//                        s2.append("     " + filteredByteArray[i] + "");
//                    }
//                } else {
//                    String original_value = String.valueOf(filteredByteArray[i]);
//                    Log.v("mmm", "UUUU original_value filter array i = 1: :: " + original_value);
//                    int original_int_value = Integer.parseInt(original_value);
//                    Log.v("mmm", "UUUU original_int_value filter array i == 1: :: " + original_int_value);
//                    s2.append("     " + filteredByteArray[i] + " ");
//                  }
//            }
//
//            Log.v("KKKKK", "KKKKa filteredByteArray.getByte(bytes) s2 : "+ s2);
//
//            for (int i = 7; i < 14; i++) {
//
//                int occurrences = 0;
//                for (char c : String.valueOf(filteredByteArray[i]).toCharArray()) {
//                    occurrences++;
//                }
//
//                Log.v("mmm", "WWWW onCharacteristicChanged occurrences s3 :: " + occurrences);
//                //check 8th byte
////                if(i==6) {
////                    if(occurrences==2)
////                        s3.append("   "+filteredByteArray[i]);
////                    else
////                        s3.append("   "+filteredByteArray[i]);
////                }
//                //else {
//                if (occurrences == 2)
//                    s3.append("     " + filteredByteArray[i] + "");
//                else
//                    s3.append("     " + filteredByteArray[i] + " ");
//                // }
//            }
//            Log.v("KKKKK", "KKKKa filteredByteArray.getByte(bytes); s3 : "+ s3);
//
//            Log.v("mmm", " onCharacteristicChanged getByte value s3 :: " + s3);
//            try {
//                fstream = new FileWriter(input_file.getAbsolutePath(), true);
//                fbw = new BufferedWriter(fstream);
//                Log.v("KKKKK", "KKKK  fbw.write(String.valueOf(s2)); 1st row : "+ s2.toString());
//
//                fbw.write(s2.toString());
//                fbw.newLine();
//                fbw.close();
//            } catch (Exception e) {
//                System.out.println("Error: " + e.getMessage());
//            }
//
//            try {
//                fstream = new FileWriter(input_file.getAbsolutePath(), true);
//                fbw = new BufferedWriter(fstream);
//                Log.v("KKKKK", "KKKK  fbw.write(String.valueOf(s2)); 1st row : "+ s3.toString());
//                fbw.write(s3.toString());
//                fbw.newLine();
//                fbw.close();
//            } catch (Exception e) {
//                System.out.println("Error: " + e.getMessage());
//            }
////            Log.v(LOG_TAG, "Total Receiving byte : " + input_file.length());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        //Remove first two bytes
//        return "QMAX SYSTEMS";
//    }



    public String getByte(byte[] userBytes) {
        try {
            byte[] filteredByteArray = Arrays.copyOfRange(userBytes, 2, userBytes.length);
            Log.v(TAG," onCharacteristicChanged getByte filteredByteArray value :: "+ filteredByteArray);
            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT getbytecalled: "+ BluetoothLeService.RECEIVE_LINE_COUNT);

            //Write byte to file from receiving bytes from Bluetooth device

            StringBuilder s2 = new StringBuilder(100);
            StringBuilder s3 = new StringBuilder(100);

            StringBuilder sb = new StringBuilder();

            for (byte b : filteredByteArray) {
                if (sb.length() > 0) {
                    sb.append(':');
                }
                sb.append(String.format("%02x", b));
                String va=String.format("%02x", b);
                Log.d(TAG,"veera::"+va);
            }

            Log.v(TAG, "PPPP filteredByteArray : "+ sb);
            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT data" +sb);


            //s2.append("     ");

            for(int i = 0; i<7;i++) {
                int occurrences = 0;
                for (char c : String.valueOf(filteredByteArray[i]).toCharArray()) {
                    occurrences++;
                }
                Log.v(TAG," PPP occurrences :: "+ occurrences);

                int date=Integer.parseInt(Integer.toString(filteredByteArray[0]));
                int Lmonth=Integer.parseInt(Integer.toString(filteredByteArray[1]));

                Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;Lmonth "+ Lmonth);

                if((date>0 && date <32)&& (Lmonth>=0 && Lmonth<=12)) {
                    lineflag=1;
                    if (occurrences == 2) {

                        countlineno++;

                        Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside occurance: "+ countlineno);


                        if(i==0)
                        {
                            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside 00 occurance: "+ i);
                            s2.append("" + filteredByteArray[i] + ":");
                        }
                        else if (i==1) {
                            Log.v("PPPP", "i == 1");
                            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside 11 occurance: "+ i);

                            int month = Integer.parseInt(Integer.toString(filteredByteArray[i]));
                     /*result=(result << 8)-Byte.MIN_VALUE+(int) filteredByteArray[i];*/
                            Log.d("PPPP", "Second position::" + String.valueOf(month));
                            java.util.Calendar cal = java.util.Calendar.getInstance();
                            java.text.SimpleDateFormat monthdate = new java.text.SimpleDateFormat("MMM");
                            cal.set(java.util.Calendar.MONTH, month - 1);
                            logmonth = monthdate.format(cal.getTime());
                            Log.d("PPPP", "Month Name" + logmonth);
                            s2.append("" + logmonth+":");
                        }
                        else if(i==2)
                        {
                            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside 22 occurance: "+ i);

                            s2.append("20" + filteredByteArray[i] + "");
                        } else if (i == 3) {

                            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside 33 occurance: "+ i);

                            Log.v(TAG+"PPPP", "i >2 1");
                            s2.append("     " + filteredByteArray[i] + ":");
                        } else if (i > 3 && i != 6) {
                            Log.v(TAG+"PPPP", "i >2 1");
                            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside 4&5 occurance: "+ i);

                            if (i == 5) {
                                s2.append(filteredByteArray[i]);
                            } else
                                s2.append(filteredByteArray[i] + ":");

                        } else if (i == 6) {
                            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside 66 EC occurance: "+ i);

                            Log.v(TAG+"PPPP", "i != 1");
                            s2.append("     EC" + filteredByteArray[i]);
                            newline =1;
                        } else {

                            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside 77 else EC occurance: "+ i);

                            Log.v(TAG+"PPPP 4444LastByte.....", "i != 1"+"0" + filteredByteArray[i]);

                            Log.v(TAG+"PPPP", "else ");
                            s2.append(filteredByteArray[i]);
                        }

                    } else if (occurrences == 1) {

                        countlineno++;

                        Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside occurance: "+ countlineno);

                        if(i==0)
                        {
                            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside 100 EC occurance: "+ i);

                            s2.append("" + filteredByteArray[i] + ":");
                        }
                        else if (i==1) {

                            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside 111 EC occurance: "+ i);

                            Log.v("PPPP", "i == 1");
                            int month = Integer.parseInt(Integer.toString(filteredByteArray[i]));
                     /*result=(result << 8)-Byte.MIN_VALUE+(int) filteredByteArray[i];*/
                            Log.d("PPPP", "Second position::" + String.valueOf(month));
                            java.util.Calendar cal = java.util.Calendar.getInstance();
                            java.text.SimpleDateFormat monthdate = new java.text.SimpleDateFormat("MMM");
                            cal.set(java.util.Calendar.MONTH, month - 1);
                            logmonth = monthdate.format(cal.getTime());
                            Log.d("PPPP", "Month Name" + logmonth);
                            s2.append("" + logmonth+":");
                        }
                        else if(i==2)
                        {
                            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside 122 EC occurance: "+ i);

                            s2.append("20" + filteredByteArray[i] + "");
                        } else if (i == 3) {
                            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside 133 EC occurance: "+ i);

                            Log.v(TAG+"PPPP", "i >2 1");
                            s2.append("     0" + filteredByteArray[i] + ":");
                        } else if (i > 3 && i != 6) {

                            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside 15,6 EC occurance: "+ i);

                            Log.v(TAG+"PPPP", "i >2 1");
                            if (i == 5) {
                                s2.append("0" + filteredByteArray[i]);
                            } else
                                s2.append("0" + filteredByteArray[i] + ":");
                        } else if (i == 6) {
                            Log.v(TAG+"PPPP", "i != 1");
                            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside newline 166 EC occurance: "+ i);

                            s2.append("     EC" + filteredByteArray[i]);
                            newline = 1;
                        } else {

                            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside  1 else EC occurance: "+ i);

                            Log.v(TAG+"PPPP", "i != 1");
                            Log.v(TAG+"PPPP 3333LastByte.....", "i != 1"+"0" + filteredByteArray[i]);

                            s2.append("0" + filteredByteArray[i]);
                        }
                    }
                }
                else
                {
                    lineflag=0;
                    Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside  11 else lineflag occurance: "+ i+"Date"+date+"Month"+Lmonth);

                    Log.v(TAG," PPP occurrences lineflag else:: "+ occurrences+ " date "+date+ " time "+Lmonth);


                }

            }
            Log.v(TAG,"PPP onCharacteristicChanged getByte value s2 :: "+s2);


            Log.v(TAG," onCharacteristicChanged getByte value s3 :: "+s3);
            try {
                fstream = new FileWriter(input_file.getAbsolutePath(),true);
                fbw = new BufferedWriter(fstream);
                fbw.write(String.valueOf(s2));
                /*if(lineflag==1) {
                    fbw.newLine();
                    lineflag = 0;
                }*/

                if(newline ==1||lineflag==1)
                {
                    fbw.newLine();
                    newline =0;
                    lineflag = 0;
                }

                fbw.close();
            } catch (Exception e) {
                System.out.println("Error: "+ e.getMessage());
            }

           //s3.append("     ");
            for(int i = 7; i<14;i++) {
                int occurrences = 0;
                for (char c : String.valueOf(filteredByteArray[i]).toCharArray()) {
                    occurrences++;
                }
                Log.v(TAG," PPP occurrences :: "+ occurrences);

                int date=Integer.parseInt(Integer.toString(filteredByteArray[8]));
                int Lmonth=Integer.parseInt(Integer.toString(filteredByteArray[9]));

                if((date>0 && date <32)&& (Lmonth>=0 && Lmonth<13)) {
                    lineflag=1;
                    if (occurrences == 2) {

                        countlineno++;

                        Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside occurance:  7772 more than  "+ i);

                        if(i==7)
                        {
                            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside occurance:  777 7 more than  "+ i);

                            s2.append("" + filteredByteArray[i] + ":");
                        }
                        else if (i==8) {

                            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside occurance:  777 8 more than  "+ i);

                            Log.v("PPPP", "i == 1");
                            int month = Integer.parseInt(Integer.toString(filteredByteArray[i]));
                     /*result=(result << 8)-Byte.MIN_VALUE+(int) filteredByteArray[i];*/
                            Log.d("PPPP", "Second position::" + String.valueOf(month));
                            java.util.Calendar cal = java.util.Calendar.getInstance();
                            java.text.SimpleDateFormat monthdate = new java.text.SimpleDateFormat("MMM");
                            cal.set(java.util.Calendar.MONTH, month - 1);
                            logmonth = monthdate.format(cal.getTime());
                            Log.d("PPPP", "Month Name" + logmonth);
                            s2.append("" + logmonth+":");
                        }
                        else if(i==9)
                        {
                            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside occurance:  777 9 more than  "+ i);

                            s2.append("20" + filteredByteArray[i] + "");
                        } else if (i == 10) {

                            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside occurance:  777 10 more than  "+ i);

                            Log.v(TAG+"PPPP", "i >2 1");
                            s3.append("     " + filteredByteArray[i] + ":");
                        } else if (i > 10 && i != 13) {

                            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside occurance:  777 1112 more than  "+ i);

                            Log.v(TAG+"PPPP", "i >2 1");
                            if (i == 12) {

                                Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside occurance:  777 12 more than  "+ i);

                                s3.append(filteredByteArray[i]);
                            } else
                                s3.append(filteredByteArray[i] + ":");
                        } else if (i == 13) {

                            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside occurance:  777 13 more than  "+ i);

                            Log.v(TAG+"PPPP", "i != 1");
                            s3.append("     EC" + filteredByteArray[i]);
                            newline1 =1;
                        } else {

                            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside occurance:  777 else more than  "+ i);

                            Log.v(TAG+"PPPP", "else ");
                            Log.v(TAG+"PPPP 2222LastByte.....", "i != 1"+"0" + filteredByteArray[i]);

                            s3.append(filteredByteArray[i]);
                        }

                    } else if (occurrences == 1) {

                        countlineno++;

                        Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside occurance:  11777 more than  "+ i);

                        if(i==7)
                        {
                            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside occurance:  77 11777 more than  "+ i);

                            s2.append("" + filteredByteArray[i] + ":");
                        }
                        else if (i==8) {

                            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside occurance:  88 11777 more than  "+ i);

                            Log.v("PPPP", "i == 1");
                            int month = Integer.parseInt(Integer.toString(filteredByteArray[i]));
                     /*result=(result << 8)-Byte.MIN_VALUE+(int) filteredByteArray[i];*/
                            Log.d("PPPP", "Second position::" + String.valueOf(month));
                            java.util.Calendar cal = java.util.Calendar.getInstance();
                            java.text.SimpleDateFormat monthdate = new java.text.SimpleDateFormat("MMM");
                            cal.set(java.util.Calendar.MONTH, month - 1);
                            logmonth = monthdate.format(cal.getTime());
                            Log.d("PPPP", "Month Name" + logmonth);
                            s2.append("" + logmonth+":");
                        }
                        else if(i==9)
                        {

                            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside occurance:  99 11777 more than  "+ i+"Date"+date+"Month"+Lmonth);

                            s2.append("20" + filteredByteArray[i] + "");
                        } else if (i == 10) {

                            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside occurance:  10 11777 more than  "+ i);

                            Log.v(TAG+"PPPP", "i >2 1");
                            s3.append("     0" + filteredByteArray[i] + ":");
                        } else if (i > 10 && i != 13) {

                            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside occurance:  11112 11777 more than  "+ i);

                            Log.v(TAG+"PPPP", "i >2 1");
                            if (i == 12) {
                                s3.append("0" + filteredByteArray[i]);
                            } else
                                s3.append("0" + filteredByteArray[i] + ":");
                        } else if (i == 13) {
                            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside occurance:  13 11777 more than  "+ i);


                            Log.v(TAG+"PPPP", "i != 1");
                            s3.append("     EC" + filteredByteArray[i]);
                            newline1 = 1;

                        } else {

                            Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside occurance:  11112 11777 more than  "+ i);

                            Log.v(TAG+"PPPP 1111LastByte.....", "i != 1"+"0" + filteredByteArray[i]);
                            s3.append("0" + filteredByteArray[i]);

                        }
                    }
                }
                else
                {
                    lineflag=0;
                    Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT inside occurance:  lineflag 11777 more than  "+ i);

                }

            }
            Log.v(TAG,"PPP onCharacteristicChanged getByte value s2 :: "+s2);

            try {
                fstream = new FileWriter(input_file.getAbsolutePath(),true);
                fbw = new BufferedWriter(fstream);
                fbw.write(String.valueOf(s3));
               /* if(lineflag==1) {
                    fbw.newLine();
                    lineflag = 0;
                }*/

                if(newline1 ==1||lineflag==1)
                {
                    fbw.newLine();
                    newline1 =0;
                }
                fbw.close();
            } catch (Exception e) {
                System.out.println("Error: "+ e.getMessage());
            }
            Log.v(LOG_TAG, "Total Receiving byte : "+ input_file.length());
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Remove first two bytes
        return "QMAX SYSTEMS";
    }

    public void mCreateReceiveFile(Context mCtx) {
        Log.v(LOG_TAG, "mCreateReceiveFile input file created");

        String fileName = null;
        /**
         * Date and time for filename creation
         */
        input_file = null;
        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat monthdate=new SimpleDateFormat("MMM");

        int filemonth = calendar.get(Calendar.MONTH);
        calendar.set(Calendar.MONTH,filemonth);
        String monthname=monthdate.format(calendar.getTime());

        int year = calendar.get(Calendar.YEAR);


        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);        // 24 hour clock
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);

        String extr = Environment.getExternalStorageDirectory().toString();
        Log.d(LOG_TAG, "Folder path " + extr);
        File mFolder = new File(extr + "/" + "LC108D");
        Log.d(LOG_TAG, "DEVICE COMMUNICATION Folder path1 " + mFolder.getAbsolutePath());
        if (!mFolder.exists()) {
            mFolder.mkdir();
            Log.d(LOG_TAG, "DEVICE COMMUNICATION New Folder Created");
        } else
            Log.d(LOG_TAG, "DEVICE COMMUNICATION Folder already exits");

//        SimpleDateFormat month_date = new SimpleDateFormat("MMM");
//        String month_name = month_date.format(month+1);




        fileName =
                dayOfMonth + "_" + monthname + "_" + year + "_" + hourOfDay + "" + minute + "" + second + ".txt";
        //FILENAME = new File(filefolder + "/" + fileName);

        input_file = new File(mFolder.getAbsolutePath() + "/" + fileName);

        try {
            input_file.createNewFile();

            Log.v(LOG_TAG, "mCreateReceiveFile input file created" + input_file.getAbsolutePath());
//          mReceiveFOS = new FileOutputStream(receiveFile, true);

            // Write header information details are : Switch status, Mode Settings, Error code
            String textToHeaderString = "";
            //Get enable swtich status
            //Display Current date




            String todayDate =   dayOfMonth +  monthname +  year + " " + hourOfDay + ":" + minute + ":" + second ;

            // mode settings

            // mode settings
            Log.v("MMMEEEE", "KKKK DEVICE COMMUNICATION Mode selection selectedModeName1 : "+ todayDate);
/*
            ModeTimeTable nodeTableObj = new ModeTimeTable();

            Cursor mSelectedModeCursor = nodeTableObj.getCursor_mode();*/

            Log.v("MMMEEEE", "KKKK DEVICE COMMUNICATION Mode selection selectedModeName2 : ");



            Cursor valuetestread = mCtx.getContentResolver().
                    query(ModeTimeTable.CONTENT_URI,
                            null,
                            ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.TRACK_MODE},
                            null);

            String BOARD_SERIAL_NO = "";

            if (valuetestread != null && valuetestread.moveToFirst()) {

                BOARD_SERIAL_NO  = valuetestread.getString(valuetestread.getColumnIndex(ModeTimeTable.BOARD_SERIAL_NO));

                Log.v("LC108D-LOG", "UUUUU 2 ModeTimeTable updated mode reset val read "+BOARD_SERIAL_NO);

            }

            headerflag = 1;


            if(headerflag ==1) {

                textToHeaderString = "\t\t" + "S.V.A RIKKON LUBES " + "\n____________________________________________\n" + "\n" + "Board Name : LC108D" + "\n___________________________________________\n" + "\n" + "Date : " + todayDate + "\n____________________________________________\n";


                textToHeaderString = textToHeaderString + "\n Board Serial Number :" + BOARD_SERIAL_NO;


                String selectedModeName = "";
                String getOnTime;
                String ontime_hours = "", ontime_min = "", ontime_sec = "",
                        offtime_hours = "", offtime_min = "", offtime_sec = "";

                String getWheelCount = "";
                String getPressureLimit = "";


               /* Cursor getSelectedMode = mCtx.getContentResolver().query(ModeTimeTable.CONTENT_URI,
                        null,
                        null,
                        null, null);
*/

                Cursor mCur = mCtx.getContentResolver().query(ModeTimeTable.CONTENT_URI,
                        null,
                        null, null,
                        null);

                String checkNameMode = "";

                if (mCur != null && mCur.moveToFirst()) {
                    do {
                        boolean getStatus = mCur.getInt(mCur.getColumnIndex(ModeTimeTable.STATUS)) > 0;
                        Log.v("mmm", "Mode Name getStatus : " + getStatus);
                        //TO get the
                        if (getStatus) {
                            checkNameMode = mCur.getString(mCur.getColumnIndex(ModeTimeTable.MODE_NAME));
                            selectedModeName = checkNameMode;
                            if(selectedModeName.equals(ModeTimeTable.TRACK_MODE))
                            {
                                getWheelCount = mCur.getString(mCur.getColumnIndex(ModeTimeTable.WHEEL_COUNT));
                            }
                            Log.v("mmm", "Mode Name if checkNameMode : " + checkNameMode);
                        } else {
                            Log.v("mmm", "Mode Name else  checkNameMode : " + checkNameMode);
                        }
                    } while (mCur.moveToNext());
                }

                //get selected mode name



/*
                if (getSelectedMode != null && getSelectedMode.moveToFirst()) {
                    boolean b = getSelectedMode.getInt(getSelectedMode.getColumnIndex(ModeTimeTable.STATUS)) > 0;*/
                //    if (b) {
                       // selectedModeName = getSelectedMode.getString(getSelectedMode.getColumnIndex(ModeTimeTable.MODE_NAME));


                if(mCur!= null && mCur.moveToFirst()) {

                    Log.v("MMMEEEE", "DEVICE COMMUNICATION Mode selection selectedModeName3 : " + checkNameMode);

                    textToHeaderString = textToHeaderString + "\n_____________________________________________\n";
                    textToHeaderString = textToHeaderString + "\n Operating In  : " + checkNameMode;
                    textToHeaderString = textToHeaderString + "\n_____________________________________________\n";
                    textToHeaderString = textToHeaderString + "\n Time Setting  : \n";

                    if (checkNameMode.equals(ModeTimeTable.PROGRESSIVE_MODE)) {

                        Log.d(TAG, "MMMEEEE Progressive Mode");

                        getOnTime = mCur.getString(mCur.getColumnIndex(ModeTimeTable.TIME));
                        Log.v("LC108D-LOG", "DEVICE COMMUNICATION getOnTime = " + getOnTime);
                        Log.v("LC108D-LOG", "DEVICE COMMUNICATION getOnTime.charAt(0)+getOnTime.charAt(1) = " + getOnTime.charAt(0) + getOnTime.charAt(1));
                        Log.v("LC108D-LOG", "DEVICE COMMUNICATION getOnTime.charAt(2)+getOnTime.charAt(3) = " + getOnTime.charAt(2) + getOnTime.charAt(3));
                        ontime_hours = String.valueOf(getOnTime.charAt(0) + String.valueOf(getOnTime.charAt(1)));
                        ontime_min = String.valueOf(getOnTime.charAt(2) + String.valueOf(getOnTime.charAt(3)));
                        ontime_sec = String.valueOf(getOnTime.charAt(4) + String.valueOf(getOnTime.charAt(5)));
                        offtime_hours = String.valueOf(getOnTime.charAt(6) + String.valueOf(getOnTime.charAt(7)));
                        offtime_min = String.valueOf(getOnTime.charAt(8) + String.valueOf(getOnTime.charAt(9)));
                        offtime_sec = String.valueOf(getOnTime.charAt(10) + String.valueOf(getOnTime.charAt(11)));

                        textToHeaderString = textToHeaderString + "\t\t\t\t\t\t On Time  : " + ontime_hours + ":" + ontime_min + ":" + ontime_sec + "\n" +
                                "\t\t\t\t\t\t OFF Time : " + offtime_hours + ":" + offtime_min + ":" + offtime_sec + "\n";

                    } else if (checkNameMode.equals(ModeTimeTable.TRACK_MODE)) {

                        Log.d(TAG, "MMMEEEE Track Mode");


                        getOnTime = mCur.getString(mCur.getColumnIndex(ModeTimeTable.TIME));
                        //getWheelCount = mCur.getString(mCur.getColumnIndex(ModeTimeTable.WHEEL_COUNT));
                        Log.v("LC108D-LOG", "getOnTime = " + getOnTime);
                        Log.v("LC108D-LOG", "wheelcount = " + getWheelCount);

                        Log.v("LC108D-LOG", "getOnTime.charAt(0)+getOnTime.charAt(1) = " + getOnTime.charAt(0) + getOnTime.charAt(1));
                        Log.v("LC108D-LOG", "getOnTime.charAt(2)+getOnTime.charAt(3) = " + getOnTime.charAt(2) + getOnTime.charAt(3));
                        int getOnHour = getOnTime.charAt(2) + getOnTime.charAt(3);
                        ontime_hours = String.valueOf(getOnTime.charAt(0) + String.valueOf(getOnTime.charAt(1)));
                        ontime_min = String.valueOf(getOnTime.charAt(2) + String.valueOf(getOnTime.charAt(3)));
                        ontime_sec = String.valueOf(getOnTime.charAt(4) + String.valueOf(getOnTime.charAt(5)));

                        textToHeaderString = textToHeaderString + "\t\t\t\t\t\t On Time  : " + ontime_hours + ":" + ontime_min + ":" + ontime_sec + "\n" +
                                "\t\t\t\t\t\t Wheel Count : " + getWheelCount + "\n";

                    } else if (checkNameMode.equals(ModeTimeTable.INJECTOR_MODE)) {
                        getOnTime = mCur.getString(mCur.getColumnIndex(ModeTimeTable.TIME));
                        getPressureLimit = mCur.getString(mCur.getColumnIndex(PRESSURE_LIMIT));
                        Log.v("LC108D-LOG", "getOnTime = " + getOnTime);
                        Log.v("LC108D-LOG", "getOnTime.charAt(0)+getOnTime.charAt(1) = " + getOnTime.charAt(0) + getOnTime.charAt(1));
                        Log.v("LC108D-LOG", "getOnTime.charAt(2)+getOnTime.charAt(3) = " + getOnTime.charAt(2) + getOnTime.charAt(3));
                        int getOnHour = getOnTime.charAt(2) + getOnTime.charAt(3);
                        ontime_hours = String.valueOf(getOnTime.charAt(0) + String.valueOf(getOnTime.charAt(1)));
                        ontime_min = String.valueOf(getOnTime.charAt(2) + String.valueOf(getOnTime.charAt(3)));
                        ontime_sec = String.valueOf(getOnTime.charAt(4) + String.valueOf(getOnTime.charAt(5)));
                        offtime_hours = String.valueOf(getOnTime.charAt(6) + String.valueOf(getOnTime.charAt(7)));
                        offtime_min = String.valueOf(getOnTime.charAt(8) + String.valueOf(getOnTime.charAt(9)));
                        offtime_sec = String.valueOf(getOnTime.charAt(10) + String.valueOf(getOnTime.charAt(11)));

                        textToHeaderString = textToHeaderString + "\t\t\t\t\t\t On Time  : " + ontime_hours + ":" + ontime_min + ":" + ontime_sec + "\n" +
                                "\t\t\t\t\t\t OFF Time : " + offtime_hours + ":" + offtime_min + ":" + offtime_sec + "\n" +
                                "\t\t\t\t\t\t Pressure Limit : " + getPressureLimit;
                    } else if (checkNameMode.equals(ModeTimeTable.DUAL_LINE_MODE)) {
                        getOnTime = mCur.getString(mCur.getColumnIndex(ModeTimeTable.TIME));
                        Log.v("LC108D-LOG", "DEVICE COMMUNICATION getOnTime = " + getOnTime);
                        Log.v("LC108D-LOG", "DEVICE COMMUNICATION getOnTime.charAt(0)+getOnTime.charAt(1) = " + getOnTime.charAt(0) + getOnTime.charAt(1));
                        Log.v("LC108D-LOG", "DEVICE COMMUNICATION getOnTime.charAt(2)+getOnTime.charAt(3) = " + getOnTime.charAt(2) + getOnTime.charAt(3));
                        ontime_hours = String.valueOf(getOnTime.charAt(0) + String.valueOf(getOnTime.charAt(1)));
                        ontime_min = String.valueOf(getOnTime.charAt(2) + String.valueOf(getOnTime.charAt(3)));
                        ontime_sec = String.valueOf(getOnTime.charAt(4) + String.valueOf(getOnTime.charAt(5)));
                        offtime_hours = String.valueOf(getOnTime.charAt(6) + String.valueOf(getOnTime.charAt(7)));
                        offtime_min = String.valueOf(getOnTime.charAt(8) + String.valueOf(getOnTime.charAt(9)));
                        offtime_sec = String.valueOf(getOnTime.charAt(10) + String.valueOf(getOnTime.charAt(11)));

                        textToHeaderString = textToHeaderString + "\t\t\t\t\t\t On Time  : " + ontime_hours + ":" + ontime_min + ":" + ontime_sec + "\n" +
                                "\t\t\t\t\t\t OFF Time : " + offtime_hours + ":" + offtime_min + ":" + offtime_sec + "\n";
                    }


                }
                   // }
                   /* getSelectedMode.close();
                } else {
                    getSelectedMode.close();
                }*/
                Log.v("MMMEEEE", "DEVICE COMMUNICATION Mode selection selectedModeName4 : " + textToHeaderString);


                textToHeaderString = textToHeaderString + "\n________________________________________________\n";
                textToHeaderString = textToHeaderString + "\n Enabled Switch Status : \n";
                Log.v("MMMEEEE", "DEVICE COMMUNICATION Mode selection selectedModeName5 : " + textToHeaderString);

          /*  Switch_Status_Table getSwitchTable = new Switch_Status_Table();
            Cursor getSwitchStatus = getSwitchTable.getSwitchStatus(selectedModeName);


            Log.v("vvvvvvvvv","sssss"+getSwitchStatus);*/

                Cursor getSwitchStatus = mCtx.getContentResolver().query(Switch_Status_Table.CONTENT_URI,
                        null,
                        Switch_Status_Table.MODE_NAME + "=?", new String[]{selectedModeName},
                        null);
                if (getSwitchStatus != null && getSwitchStatus.moveToFirst()) {
                    boolean switch1 = getSwitchStatus.getInt(getSwitchStatus.getColumnIndex(Switch_Status_Table.LIMIT_SWITCH1)) > 0;
                    if (switch1) textToHeaderString = textToHeaderString + "\n LIMIT_SWITCH1 ";
                    boolean switch2 = getSwitchStatus.getInt(getSwitchStatus.getColumnIndex(Switch_Status_Table.LIMIT_SWITCH2)) > 0;
                    if (switch2) textToHeaderString = textToHeaderString + "\n LIMIT_SWITCH2 ";
                    boolean switch3 = getSwitchStatus.getInt(getSwitchStatus.getColumnIndex(Switch_Status_Table.LIMIT_SWITCH3)) > 0;
                    if (switch3) textToHeaderString = textToHeaderString + "\n LIMIT_SWITCH3 ";
                    boolean switch4 = getSwitchStatus.getInt(getSwitchStatus.getColumnIndex(Switch_Status_Table.LIMIT_SWITCH4)) > 0;
                    if (switch4) textToHeaderString = textToHeaderString + "\n LIMIT_SWITCH4 ";

                    boolean switch5 = getSwitchStatus.getInt(getSwitchStatus.getColumnIndex(Switch_Status_Table.LIMIT_SWITCH5)) > 0;
                    if (switch5) textToHeaderString = textToHeaderString + "\n LIMIT_SWITCH5 ";

                    boolean switch6 = getSwitchStatus.getInt(getSwitchStatus.getColumnIndex(Switch_Status_Table.LIMIT_SWITCH6)) > 0;
                    if (switch6) textToHeaderString = textToHeaderString + "\n LIMIT_SWITCH6 ";

                    boolean switch7 = getSwitchStatus.getInt(getSwitchStatus.getColumnIndex(Switch_Status_Table.LIMIT_SWITCH7)) > 0;
                    if (switch7) textToHeaderString = textToHeaderString + "\n LIMIT_SWITCH7 ";

                    boolean switch8 = getSwitchStatus.getInt(getSwitchStatus.getColumnIndex(Switch_Status_Table.LIMIT_SWITCH8)) > 0;
                    if (switch8) textToHeaderString = textToHeaderString + "\n LIMIT_SWITCH8 ";

                    boolean switch9 = getSwitchStatus.getInt(getSwitchStatus.getColumnIndex(Switch_Status_Table.LIMIT_SWITCH9)) > 0;
                    if (switch9) textToHeaderString = textToHeaderString + "\n PRESSURE_SWITCH ";

                    boolean switch10 = getSwitchStatus.getInt(getSwitchStatus.getColumnIndex(Switch_Status_Table.LIMIT_SWITCH10)) > 0;
                    if (switch10) textToHeaderString = textToHeaderString + "\n LOW LEVEL SWITCH ";

                    boolean proximity = getSwitchStatus.getInt(getSwitchStatus.getColumnIndex(Switch_Status_Table.PROXIMITY_SWITCH)) > 0;
                    if (proximity) textToHeaderString = textToHeaderString + "\n PROXIMITY_SWITCH ";

                    boolean pressure = getSwitchStatus.getInt(getSwitchStatus.getColumnIndex(Switch_Status_Table.PRESSURE_SWITCH)) > 0;
                    if (pressure) textToHeaderString = textToHeaderString + "\n  ";

//                boolean wheel_sensor = getSwitchStatus.getInt(getSwitchStatus.getColumnIndex(Switch_Status_Table.WHEEL_SENSOR)) > 0;
//                if(wheel_sensor) textToHeaderString = textToHeaderString+"\n WHEEL_SENSOR ";
                    getSwitchStatus.close();
                } else {
                    getSwitchStatus.close();
                }


                Log.v("MMMEEEE", "DEVICE COMMUNICATION Mode selection selectedModeName6 : " + textToHeaderString);
                textToHeaderString = textToHeaderString + "\n\n" + "__________________________________________________\n";
                textToHeaderString = textToHeaderString + "\n\n       Code              Description\n" +
                        "__________________________________________________\n" +
                        "        EC1              Limit Switch1 Error\n" +
                        "        EC2              Limit Switch2 Error\n" +
                        "        EC3              Limit Switch3 Error\n" +
                        "        EC4              Limit Switch4 Error\n" +
                        "        EC5              Limit Switch5 Error\n" +
                        "        EC6              Limit Switch6 Error\n" +
                        "        EC7              Limit Switch7 Error\n" +
                        "        EC8              Limit Switch8 Error\n" +
                        "        EC9              Limit Switch9 Error\n" +
                        "        EC10             Limit Low Level Error\n" +
                        "        EC11             Limit High Level Status\n" +
                        "        EC12             Proximity Error\n" +
                        "        EC13             Pressure Error\n" +
                        "        EC14             Time Change Update\n" +
                        "        EC15             Solenoid Error\n" +
                        "        EC16             Max Wheel Per Day Error\n" +
                        "        EC17             Min Wheel Per Day Error\n" +
                        "        EC18             Pressure Switch1 Error\n" +
                        "        EC19             Pressure Switch2 Error\n" +


                        "_____________________________________________\n" +
                        "   DATE       HH:MM:SS     Error Code\n" +
                        "_____________________________________________";
                Log.v("MMMEEEE", "DEVICE COMMUNICATION Mode selection selectedModeName7 : " + textToHeaderString);

                fstream = new FileWriter(input_file.getAbsolutePath(), true);
                fbw = new BufferedWriter(fstream);
                fbw.write(String.valueOf(textToHeaderString));
                Log.d(TAG, "HEADER TEXT ERROR" + String.valueOf(textToHeaderString));
                fbw.newLine();
                fbw.close();
                Log.v(LOG_TAG, "DEVICE COMMUNICATION input file created");

                headerflag = 0;

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public class MyBinder extends Binder {
        UserMsgTransferService getService() {
            return UserMsgTransferService.this;
        }
    }
}