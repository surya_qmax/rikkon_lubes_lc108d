package com.mmm.rikon_lc108d.activity;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.mmm.rikon_lc108d.BLE.BluetoothLeService;
import com.mmm.rikon_lc108d.LC108DApplication;
import com.mmm.rikon_lc108d.R;
import com.mmm.rikon_lc108d.db.MobileDetailsTable;

import java.util.ArrayList;

public class MobileDetails extends AppCompatActivity {
    int i = 1;
    private Button btnUpdate_mobile_details = null;

    private ProgressDialog progressDialog;
    private final static String TAG = MobileDetails.class.getSimpleName();


    TextView text_state;

    public static BluetoothLeService mBluetoothLe;
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    private String mDeviceName;
    private String mDeviceAddress;
    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLe = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLe.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            try {
                mBluetoothLe.connect(mDeviceAddress);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLe = null;
        }
    };
    private boolean mConnected = false;
    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                text_state.setText("State : Connected");
                updateConnectionState(R.string.connected);
                invalidateOptionsMenu();
                Log.v(TAG, " DEVICE CONNECTED : " + BluetoothLeService.ACTION_GATT_CONNECTED.equals(action));
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                text_state.setText("State : DisConnected");
                updateConnectionState(R.string.disconnected);
                invalidateOptionsMenu();
                Log.v(TAG, " DEVICE DISCONNECTED : " + BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action));
                // clearUI();
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
                //displayGattServices(mBluetoothLe.getSupportedGattServices());
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
//                displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
                Log.v("mmmma", " RECEIVING BYTE Switch status : " + intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
            }
        }
    };
    private BluetoothGattCharacteristic mNotifyCharacteristic;

    // If a given GATT characteristic is selected, check for supported features.  This sample
    // demonstrates 'Read' and 'Notify' features.  See
    // http://d.android.com/reference/android/bluetooth/BluetoothGatt.html for the complete
    // list of supported characteristic features.
    private final ExpandableListView.OnChildClickListener servicesListClickListner =
            new ExpandableListView.OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                                            int childPosition, long id) {
                    if (mGattCharacteristics != null) {
                        final BluetoothGattCharacteristic characteristic =
                                mGattCharacteristics.get(groupPosition).get(childPosition);
                        final int charaProp = characteristic.getProperties();
                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
                            // If there is an active notification on a characteristic, clear
                            // it first so it doesn't update the data field on the user interface.
                            if (mNotifyCharacteristic != null) {
                                mBluetoothLe.setCharacteristicNotification(
                                        mNotifyCharacteristic, false);
                                mNotifyCharacteristic = null;
                            }
                            mBluetoothLe.readCharacteristic(characteristic);
                        }
                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                            mNotifyCharacteristic = characteristic;
                            mBluetoothLe.setCharacteristicNotification(
                                    characteristic, true);
                        }
                        return true;
                    }
                    return false;
                }
            };

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BluetoothLeService.EXTRA_DATA);
        return intentFilter;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        try {
//            unbindService(mServiceConnection);
//            mBluetoothLe.disconnect();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        mBluetoothLe = null;


        /*unbindService(mServiceConnection);
        mBluetoothLe = null;*/

    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();

         progressDialog = new ProgressDialog(MobileDetails.this,
                R.style.AppTheme_Dark_Dialog);
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (mBluetoothLe != null) {
            final boolean result = mBluetoothLe.connect(mDeviceAddress);
            Log.d(TAG, "Connect request result=" + result);
            text_state.setText("State : Connected");
        }
    }

    private void updateConnectionState(final int resourceId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                text_state.setText(resourceId);
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gatt_services, menu);
        if (mConnected) {
            menu.findItem(R.id.menu_connect).setVisible(false);
            menu.findItem(R.id.menu_disconnect).setVisible(true);
        } else {
            menu.findItem(R.id.menu_connect).setVisible(true);
            menu.findItem(R.id.menu_disconnect).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        try {
            switch (item.getItemId()) {
                case R.id.menu_connect:
                    mBluetoothLe.connect(mDeviceAddress);
                    return true;
                case R.id.menu_disconnect:
                    mBluetoothLe.disconnect();
                    return true;
                case android.R.id.home:
                    onBackPressed();
                    return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_details_menu);

        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarmobile);
        text_state = (TextView) toolbar.findViewById(R.id.txt_state_mobile);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final EditText etMobileNumber_one = (EditText) findViewById(R.id.edit_number_one);
        final EditText etMobileNumber_two = (EditText) findViewById(R.id.edit_number_two);
        final EditText etMobileNumber_three = (EditText) findViewById(R.id.edit_number_three);
        final EditText etMobileNumber_four = (EditText) findViewById(R.id.edit_number_four);

        final Switch mobile_Switch_one = (Switch) findViewById(R.id.switch_mobile_one);
        final Switch mobile_Switch_two = (Switch) findViewById(R.id.switch_mobile_two);
        final Switch mobile_Switch_three = (Switch) findViewById(R.id.switch_mobile_three);
        final Switch mobile_Switch_four = (Switch) findViewById(R.id.switch_mobile_four);


        SharedPreferences sharedpreferences1 = getSharedPreferences(LC108DApplication.PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
        mDeviceName = sharedpreferences1.getString(LC108DApplication.PREF_DEVICE_NAME, "");
        mDeviceAddress = sharedpreferences1.getString(LC108DApplication.PREF_DEVICE_ADDRESS, "");
        Log.v(TAG, "mDeviceName :" + mDeviceName + "mDeviceAddress : " + mDeviceAddress);

        btnUpdate_mobile_details = (Button) findViewById(R.id.btn_mobile_update);

        Cursor mobileCursor = getContentResolver().query(MobileDetailsTable.CONTENT_URI,
                null,
                null,
                null,
                null);
        ArrayList<String> number = new ArrayList<>();

        ArrayList<Boolean> alStatus = new ArrayList<>();
        if (mobileCursor != null && mobileCursor.moveToFirst()) {
            do {
                String no = mobileCursor.getString(mobileCursor.getColumnIndex(MobileDetailsTable.MOBILE_NUMBER));
                boolean status = mobileCursor.getInt(mobileCursor.getColumnIndex(MobileDetailsTable.MOBILE_LINE_STATUS)) > 0;

                number.add(no);
                alStatus.add(status);
            } while (mobileCursor.moveToNext());
        }
        //Set Mobile Status switch
        etMobileNumber_one.setText(number.get(0));
        if(etMobileNumber_one.getText().toString().equals(""))
        {
            mobile_Switch_one.setEnabled(false);
        }  else {
            mobile_Switch_one.setChecked(alStatus.get(0));
        }
        //
        etMobileNumber_two.setText(number.get(1));
        if(etMobileNumber_two.getText().toString().equals(""))
        {
            mobile_Switch_two.setEnabled(false);
        }  else {
            mobile_Switch_two.setChecked(alStatus.get(1));
        }
        //
        etMobileNumber_three.setText(number.get(2));
        if(etMobileNumber_three.getText().toString().equals(""))
        {
            mobile_Switch_three.setEnabled(false);
        }  else {
            mobile_Switch_three.setChecked(alStatus.get(2));
        }
        //

        etMobileNumber_four.setText(number.get(3));
        if(etMobileNumber_four.getText().toString().equals(""))
        {
            mobile_Switch_four.setEnabled(false);
        }  else {
            mobile_Switch_four.setChecked(alStatus.get(3));
        }



        mobile_Switch_one.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(etMobileNumber_one.getText().toString().length()==10)
                {
                    Log.d(TAG,"Display mobile number one"+etMobileNumber_one);
                }

                else
                {
                    etMobileNumber_one.setError("Enter 10 digit");
                }

            }
        });

        mobile_Switch_two.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(etMobileNumber_two.getText().toString().length()==10)
                {
                    Log.d(TAG,"Display mobile number two"+etMobileNumber_two);
                }

                else
                {
                    etMobileNumber_two.setError("Enter 10 digit");
                }
            }
        });


        mobile_Switch_three.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(etMobileNumber_three.getText().toString().length()==10)
                {
                    Log.d(TAG,"Display mobile number three"+etMobileNumber_three);
                }

                else
                {
                    etMobileNumber_three.setError("Enter 10 digit");
                }
            }
        });

        mobile_Switch_four.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(etMobileNumber_four.getText().toString().length()==10)
                {
                    Log.d(TAG,"Display mobile number three"+etMobileNumber_four);
                }

                else
                {
                    etMobileNumber_four.setError("Enter 10 digit");
                }
            }
        });


        etMobileNumber_one.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
//                            ontime_min.setText(String.valueOf(s.toString().length()));
                if (!s.toString().equals("")) {
                    // Log.v("mmmasdf", "afterTextChanged" +String.valueOf(s.toString().length()));
                    int min_value = etMobileNumber_one.getText().toString().length();
                    if (min_value < 10) {
                        etMobileNumber_one.setError("Enter 10 digit");
                        mobile_Switch_one.setChecked(false);
                        mobile_Switch_one.setEnabled(false);
                    } else {
                        mobile_Switch_one.setEnabled(true);
                    }
                } else {
                    // ontime_min.setText("00");

                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        etMobileNumber_one.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Log.v("mmmmaser", "hasFocus = " + hasFocus);
                if (hasFocus) {
                    // Toast.makeText(getApplicationContext(), "got the focus", Toast.LENGTH_LONG).show();
                } else {
                    // Toast.makeText(getApplicationContext(), "lost the focus"+etMobileNumber_one.getText().toString().length(), Toast.LENGTH_LONG).show();
                    int count = etMobileNumber_one.getText().toString().length();
                    Log.v("mmmmaser", "etMobileNumber_one =  " + count);
                    if (count < 10) {
                        //   Toast.makeText(getApplicationContext(), "lost the focus if" + etMobileNumber_one.getText().toString().length(), Toast.LENGTH_LONG).show();
                        Log.v("mmmmaser", "etMobileNumber_one if =  " + count);
                        etMobileNumber_one.setError("Enter 10 digits");
                        mobile_Switch_one.setChecked(false);
                        mobile_Switch_one.setEnabled(false);
                    } else {
                        mobile_Switch_one.setEnabled(true);
                    }
                }
            }
        });

        etMobileNumber_two.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
//                            ontime_min.setText(String.valueOf(s.toString().length()));
                if (!s.toString().equals("")) {
                    // Log.v("mmmasdf", "afterTextChanged" +String.valueOf(s.toString().length()));
                    int min_value = etMobileNumber_two.getText().toString().length();
                    if (min_value < 10) {
                        etMobileNumber_two.setError("Enter 10 digit");
                        mobile_Switch_two.setChecked(false);
                        mobile_Switch_two.setEnabled(false);
                    } else {
                        mobile_Switch_two.setEnabled(true);
                    }
                } else {
                    // ontime_min.setText("00");
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        etMobileNumber_two.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Log.v("mmmmaser", "etMobileNumber_one if =  " + hasFocus);
                if (hasFocus) {
                } else {
                    int count = etMobileNumber_two.getText().length();
                    if (etMobileNumber_two.getText().length() < 10) {
                        etMobileNumber_two.setError("Enter 10 digits");
                        mobile_Switch_two.setChecked(false);
                        mobile_Switch_two.setEnabled(false);
                    } else {
                        mobile_Switch_two.setEnabled(true);
                    }
                }
            }
        });

        etMobileNumber_three.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
//                            ontime_min.setText(String.valueOf(s.toString().length()));
                if (!s.toString().equals("")) {
                    // Log.v("mmmasdf", "afterTextChanged" +String.valueOf(s.toString().length()));
                    int min_value = etMobileNumber_three.getText().toString().length();
                    if (min_value < 10) {
                        etMobileNumber_three.setError("Enter 10 digit");
                        mobile_Switch_three.setChecked(false);
                        mobile_Switch_three.setEnabled(false);
                    } else {
                        mobile_Switch_three.setEnabled(true);
                    }
                } else {
                    // ontime_min.setText("00");
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        etMobileNumber_three.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                } else {
                    if (etMobileNumber_three.getText().length() < 10) {
                        etMobileNumber_three.setError("Enter 10 digits");
                        mobile_Switch_three.setChecked(false);
                        mobile_Switch_three.setEnabled(false);
                    } else {
                        mobile_Switch_three.setEnabled(true);
                    }
                }
            }
        });
        etMobileNumber_four.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
//                            ontime_min.setText(String.valueOf(s.toString().length()));
                if (!s.toString().equals("")) {
                    // Log.v("mmmasdf", "afterTextChanged" +String.valueOf(s.toString().length()));
                    int min_value = etMobileNumber_four.getText().toString().length();
                    if (min_value < 10) {
                        etMobileNumber_four.setError("Enter 10 digit");
                        mobile_Switch_four.setChecked(false);
                        mobile_Switch_four.setEnabled(false);
                    } else {
                        mobile_Switch_four.setEnabled(true);
                    }
                } else {
                    // ontime_min.setText("00");
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        etMobileNumber_four.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                } else {
                    if (etMobileNumber_four.getText().length() < 10) {
                        etMobileNumber_four.setError("Enter 10 digits");
                        mobile_Switch_four.setChecked(false);
                        mobile_Switch_four.setEnabled(false);
                    } else {
                        mobile_Switch_four.setEnabled(true);
                    }
                }
            }
        });

        btnUpdate_mobile_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean stateSwitchOne = mobile_Switch_one.isChecked();

                boolean stateSwitchTwo = mobile_Switch_two.isChecked();

                boolean stateSwitchThree = mobile_Switch_three.isChecked();

                boolean stateSwitchFour = mobile_Switch_four.isChecked();

                Log.d(TAG,"Mobile Switch state stateSwitchOne"+stateSwitchOne);

                Log.d(TAG,"Mobile Switch state stateSwitchTwo"+stateSwitchTwo);

                Log.d(TAG,"Mobile Switch state stateSwitchThree"+stateSwitchThree);

                Log.d(TAG,"Mobile Switch state stateSwitchFour "+stateSwitchFour);

                int mobileonelen = etMobileNumber_one.getText().toString().length();
                int mobiletwolen = etMobileNumber_two.getText().toString().length();
                int mobilethreelen = etMobileNumber_three.getText().toString().length();
                int mobilefourlen = etMobileNumber_four.getText().toString().length();

                Log.d(TAG,"Mobile number len one"+mobileonelen);
                Log.d(TAG,"Mobile number len two"+mobiletwolen);
                Log.d(TAG,"Mobile number len three"+mobilethreelen);
                Log.d(TAG,"Mobile number len four"+mobilefourlen);



                if(!mobile_Switch_one.isChecked() &&
                        !mobile_Switch_two.isChecked() &&
                        !mobile_Switch_three.isChecked() &&
                        !mobile_Switch_four.isChecked()) {


                    if (mBluetoothLe != null) {
                        try {
                            byte[] BYTE_SEND_MOBILE_CONFIGURATION = {
                                    0x41, 0x54, 0x06, 0x02, 0x00,0x00, 0x00, 0x00,
                                    0x00, 0x00, 0x00, 0x00, 0x00,
                                    0x00, 0x00, 0x01,  0x74,
                                    0x20, 0x0D, 0x0A};
                            StringBuilder sb = new StringBuilder();

                            for (byte b : BYTE_SEND_MOBILE_CONFIGURATION) {
                                if (sb.length() > 0) {
                                    sb.append(':');
                                }
                                sb.append(String.format("%02x", b));
                            }

                            Log.v(TAG, "MBBB DEVICE COMMUNICATION ***** all Received Value from sb : "+ sb);
                            mBluetoothLe.writeCustomCharacteristic(BYTE_SEND_MOBILE_CONFIGURATION);
                            Log.v(TAG, "MBBB DEVICE COMMUNICATION Write all switch disabled...");
                             Toast.makeText(getApplicationContext(), "Mobile Config updated successfully 1", Toast.LENGTH_SHORT).show();
                             finish();


                        } catch (Exception e) {
                            e.printStackTrace();
                            // Toast.makeText(getApplicationContext(), "Bluetooth Connection problem", Toast.LENGTH_SHORT).show();
                        }
                    }

                } else {

                    ContentValues values = new ContentValues();
                    values.put(MobileDetailsTable.MOBILE_LINE, MobileDetailsTable.MOBILE_LINE_ONE);
                    values.put(MobileDetailsTable.MOBILE_NUMBER, etMobileNumber_one.getText().toString());
                    values.put(MobileDetailsTable.MOBILE_LINE_STATUS, mobile_Switch_one.isChecked());
                    getContentResolver().update(MobileDetailsTable.CONTENT_URI, values,
                            MobileDetailsTable.MOBILE_LINE + "=?", new String[]{MobileDetailsTable.MOBILE_LINE_ONE});

                    ContentValues values2 = new ContentValues();
                    values2.put(MobileDetailsTable.MOBILE_LINE, MobileDetailsTable.MOBILE_LINE_TWO);
                    values2.put(MobileDetailsTable.MOBILE_NUMBER, etMobileNumber_two.getText().toString());
                    values2.put(MobileDetailsTable.MOBILE_LINE_STATUS, mobile_Switch_two.isChecked());
                    getContentResolver().update(MobileDetailsTable.CONTENT_URI, values2,
                            MobileDetailsTable.MOBILE_LINE + "=?", new String[]{MobileDetailsTable.MOBILE_LINE_TWO});

                    ContentValues values3 = new ContentValues();
                    values3.put(MobileDetailsTable.MOBILE_LINE, MobileDetailsTable.MOBILE_LINE_THREE);
                    values3.put(MobileDetailsTable.MOBILE_NUMBER, etMobileNumber_three.getText().toString());
                    values3.put(MobileDetailsTable.MOBILE_LINE_STATUS, mobile_Switch_three.isChecked());
                    getContentResolver().update(MobileDetailsTable.CONTENT_URI, values3,
                            MobileDetailsTable.MOBILE_LINE + "=?", new String[]{MobileDetailsTable.MOBILE_LINE_THREE});

                    ContentValues values4 = new ContentValues();
                    values4.put(MobileDetailsTable.MOBILE_LINE, MobileDetailsTable.MOBILE_LINE_FOUR);
                    values4.put(MobileDetailsTable.MOBILE_NUMBER, etMobileNumber_four.getText().toString());
                    values4.put(MobileDetailsTable.MOBILE_LINE_STATUS, mobile_Switch_four.isChecked());
                    getContentResolver().update(MobileDetailsTable.CONTENT_URI, values4,
                            MobileDetailsTable.MOBILE_LINE + "=?", new String[]{MobileDetailsTable.MOBILE_LINE_FOUR});
                    //update switch status

//                final Runnable newThread = new Runnable() {
//                    @Override
//                    public void run() {
//                        while (true) {
//                            Log.d("Msg from BG Thread", "This message will be displayed every 5 seconds");
//                            if (i<4) {
//                                Log.v("MBBB", "Loop : "+ i );
//                                Toast.makeText(getApplicationContext(), "Loop Count : "+ i, Toast.LENGTH_SHORT).show();
//                                i++;
//                                try {
//                                    Thread.sleep(2000);//
//                                } catch (InterruptedException e){
//                                    e.printStackTrace();
//                                }
//                            } else {
//
//                                Log.v("MBBB", "Loop : "+ i );
//                            }
//
//                        }
//                    }
//                };
                    //  newThread.run();


                    AsyncTask<Void, Void, Void> execute = new AsyncTask<Void, Void, Void>() {

                        private boolean status = false;

                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();

//                        progressDialog.setIndeterminate(true);
//                        progressDialog.setMessage("Get Update details authentication...");
//                        progressDialog.show();
                        }

                        @Override
                        protected Void doInBackground(final Void... params) {
                            System.out.println("mmm Found user doInBackground1 ");
                            try {
                                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                                StrictMode.setThreadPolicy(policy);

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (mobile_Switch_one.isEnabled()) {
                                            Log.v("MBBB", "mobile_Switch_one if" + mobile_Switch_one.isChecked());

                                            if(etMobileNumber_one.getText().toString().length()==10) {

                                                String str = etMobileNumber_one.getText().toString();


                                                Log.v("MBBB", " String str = etMobileNumber_one.getText().toString() ; " + str);

                                                char[] charArray = str.toCharArray();
                                                Log.v("MBBB", " charArray[0] ; " + charArray[0] + " : Byte : " + (byte) charArray[0]);
                                                Log.v("MBBB", " charArray[1] ; " + charArray[1] + " : Byte : " + (byte) charArray[1]);
                                                Log.v("MBBB", " charArray[2] ; " + charArray[2] + " : Byte : " + (byte) charArray[2]);
                                                Log.v("MBBB", " charArray[3] ; " + charArray[3] + " : Byte : " + (byte) charArray[3]);
                                                Log.v("MBBB", " charArray[4] ; " + charArray[4] + " : Byte : " + (byte) charArray[4]);
                                                Log.v("MBBB", " charArray[5] ; " + charArray[5] + " : Byte : " + (byte) charArray[5]);
                                                Log.v("MBBB", " charArray[6] ; " + charArray[6] + " : Byte : " + (byte) charArray[6]);
                                                Log.v("MBBB", " charArray[7] ; " + charArray[7] + " : Byte : " + (byte) charArray[7]);
                                                Log.v("MBBB", " charArray[8] ; " + charArray[8] + " : Byte : " + (byte) charArray[8]);
                                                Log.v("MBBB", " charArray[9] ; " + charArray[9] + " : Byte : " + (byte) charArray[9]);

                                                byte switchmobileone = 0x00;

                                                if(mobile_Switch_one.isChecked())
                                                {
                                                    switchmobileone = 0x01;
                                                }


                                                if (mBluetoothLe != null) {
                                                    try {
                                                        byte[] BYTE_SEND_MOBILE_CONFIGURATION = {
                                                                0x41, 0x54, 0x06, 0x01, 0x01, (byte) charArray[0], (byte) charArray[1], (byte) charArray[2],
                                                                (byte) charArray[3], (byte) charArray[4], (byte) charArray[5], (byte) charArray[6], (byte) charArray[7],
                                                                (byte) charArray[8], (byte) charArray[9], switchmobileone, 0x74,
                                                                0x20, 0x0D, 0x0A};
                                                        StringBuilder sb = new StringBuilder();

                                                        for (byte b : BYTE_SEND_MOBILE_CONFIGURATION) {
                                                            if (sb.length() > 0) {
                                                                sb.append(':');
                                                            }
                                                            sb.append(String.format("%02x", b));
                                                        }

                                                        Log.v(TAG, "MBBB DEVICE COMMUNICATION *****11 Received Value from sb : " + sb);
                                                        mBluetoothLe.writeCustomCharacteristic(BYTE_SEND_MOBILE_CONFIGURATION);
                                                        status = true;
                                                        Log.v(TAG, "MBBB DEVICE COMMUNICATION Write 611 mobile switch one state..." + mobile_Switch_one.isChecked());

                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                        Toast.makeText(getApplicationContext(), "Bluetooth Connection problem", Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            }

                                            else {
                                                status = false;
                                                Log.v(TAG, "MBBB DEVICE COMMUNICATION Write 611 mobile number one state..."+etMobileNumber_one.getText().toString());

                                                Log.v("MBBB", "mobile number one state else" + etMobileNumber_one.getText().toString());
                                            }
                                        } else {
                                            status = false;
                                            Log.v(TAG, "MBBB DEVICE COMMUNICATION Write 611 mobile switch one state..."+mobile_Switch_one.isChecked());

                                            Log.v("MBBB", "mobile_Switch_one else" + mobile_Switch_one.isChecked());
                                        }
                                    }
                                });

                            } catch (Exception e) {

                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(final Void aVoid) {
                            //progressDialog.dismiss();

                            //if (status) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                           // Toast.makeText(getApplicationContext(), "Send Request Log File command Successfully ", Toast.LENGTH_SHORT).show();

                                             Toast.makeText(getApplicationContext(), "Mobile config updated Successfully 2", Toast.LENGTH_SHORT).show();

                                            try {
                                                Log.v(TAG, "MBBB ddd DEVICE COMMUNICATION Write 612... sleep start");
                                                Thread.sleep(2000);
                                                // Toast.makeText(getApplicationContext(), "Send Request Log File command Successfully (Write function 613...)", Toast.LENGTH_SHORT).show();
                                                Log.v(TAG, "MBBB ddd DEVICE COMMUNICATION Write 612... sleep end");

                                                AsyncTask<Void, Void, Void> execute1 = new AsyncTask<Void, Void, Void>() {

                                                    private boolean status = false;

                                                    @Override
                                                    protected void onPreExecute() {
                                                        super.onPreExecute();

//                        progressDialog.setIndeterminate(true);
//                        progressDialog.setMessage("Get Update details authentication...");
//                        progressDialog.show();
                                                    }

                                                    @Override
                                                    protected Void doInBackground(final Void... params) {
                                                        System.out.println("mmm Found user doInBackground1 ");
                                                        try {

                                                            runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    if (mobile_Switch_two.isEnabled()) {
                                                                        Log.v("MBBB", "mobile_Switch_two if" + mobile_Switch_two.isChecked());

                                                                        if (etMobileNumber_two.getText().toString().length() == 10) {

                                                                            String str = etMobileNumber_two.getText().toString();
                                                                            Log.v("MBBB", " String str = etMobileNumber_one.getText().toString() ; " + str);

                                                                            char[] charArray = str.toCharArray();
                                                                            Log.v("MBBB", " charArray[0] ; " + charArray[0] + " : Byte : " + (byte) charArray[0]);
                                                                            Log.v("MBBB", " charArray[1] ; " + charArray[1] + " : Byte : " + (byte) charArray[1]);
                                                                            Log.v("MBBB", " charArray[2] ; " + charArray[2] + " : Byte : " + (byte) charArray[2]);
                                                                            Log.v("MBBB", " charArray[3] ; " + charArray[3] + " : Byte : " + (byte) charArray[3]);
                                                                            Log.v("MBBB", " charArray[4] ; " + charArray[4] + " : Byte : " + (byte) charArray[4]);
                                                                            Log.v("MBBB", " charArray[5] ; " + charArray[5] + " : Byte : " + (byte) charArray[5]);
                                                                            Log.v("MBBB", " charArray[6] ; " + charArray[6] + " : Byte : " + (byte) charArray[6]);
                                                                            Log.v("MBBB", " charArray[7] ; " + charArray[7] + " : Byte : " + (byte) charArray[7]);
                                                                            Log.v("MBBB", " charArray[8] ; " + charArray[8] + " : Byte : " + (byte) charArray[8]);
                                                                            Log.v("MBBB", " charArray[9] ; " + charArray[9] + " : Byte : " + (byte) charArray[9]);

                                                                            byte switchmobiletwo = 0x00;

                                                                            if(mobile_Switch_two.isChecked())
                                                                            {
                                                                                switchmobiletwo = 0x01;
                                                                            }



                                                                            if (mBluetoothLe != null) {
                                                                                try {
                                                                                    byte[] BYTE_SEND_LOG_REQUEST = {
                                                                                            0x41, 0x54, 0x06, 0x01, 0x02, (byte) charArray[0], (byte) charArray[1], (byte) charArray[2],
                                                                                            (byte) charArray[3], (byte) charArray[4], (byte) charArray[5], (byte) charArray[6], (byte) charArray[7],
                                                                                            (byte) charArray[8], (byte) charArray[9], switchmobiletwo, 0x74,
                                                                                            0x20, 0x0D, 0x0A};
                                                                                    mBluetoothLe.writeCustomCharacteristic(BYTE_SEND_LOG_REQUEST);
                                                                                    status = true;
                                                                                    Log.v(TAG, "DEVICE COMMUNICATION Write 612...");

                                                                                    StringBuilder sb = new StringBuilder();

                                                                                    for (byte b : BYTE_SEND_LOG_REQUEST) {
                                                                                        if (sb.length() > 0) {
                                                                                            sb.append(':');
                                                                                        }
                                                                                        sb.append(String.format("%02x", b));
                                                                                    }


                                                                                    Log.v(TAG, "MBBB DEVICE COMMUNICATION ***** 22 Received Value from sb : " + sb);


                                                                                    Log.v(TAG, "MBBB DEVICE COMMUNICATION Write 612 mobile switch two state..." + mobile_Switch_two.isChecked());


                                                                                } catch (Exception e) {
                                                                                    e.printStackTrace();
                                                                                    Toast.makeText(getApplicationContext(), "Bluetooth Connection problem", Toast.LENGTH_SHORT).show();
                                                                                }
                                                                            }


                                                                        }

                                                                        else {
                                                                            status = false;
                                                                            Log.v(TAG, "MBBB DEVICE COMMUNICATION Write 612 mobile number two..."+etMobileNumber_two.getText().toString());

                                                                            Log.v("MBBB", "mobile_number_two else" + etMobileNumber_two.getText().toString());
                                                                        }
                                                                    }else {
                                                                        status = false;
                                                                        Log.v(TAG, "MBBB DEVICE COMMUNICATION Write 612 mobile switch two state..."+mobile_Switch_two.isChecked());

                                                                        Log.v("MBBB", "mobile_Switch_two else" + mobile_Switch_two.isChecked());
                                                                    }
                                                                }
                                                            });

                                                        } catch (Exception e) {

                                                        }
                                                        return null;
                                                    }

                                                    @Override
                                                    protected void onPostExecute(final Void aVoid) {
                                                        //progressDialog.dismiss();

                                                       // if (status) {
                                                            runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    try {
                                                                        //  Thread.sleep(2000);
                                                                        Toast.makeText(getApplicationContext(), "Mobile Config updated Successfully 3", Toast.LENGTH_SHORT).show();
                                                                       // Toast.makeText(getApplicationContext(), "Send Request Log File command Successfully ", Toast.LENGTH_SHORT).show();
                                                                        try {
                                                                            Log.v(TAG, "MBBB ddd DEVICE COMMUNICATION Write 613... sleep start");
                                                                            Thread.sleep(2000);
                                                                            // Toast.makeText(getApplicationContext(), "Send Request Log File command Successfully (Write function 613...)", Toast.LENGTH_SHORT).show();
                                                                            Log.v(TAG, "MBBB ddd DEVICE COMMUNICATION Write 613... sleep end");
                                                                            AsyncTask<Void, Void, Void> execute2 = new AsyncTask<Void, Void, Void>() {

                                                                                private boolean status = false;

                                                                                @Override
                                                                                protected void onPreExecute() {
                                                                                    super.onPreExecute();
//                                                                               progressDialog.setIndeterminate(true);
//                                                                               progressDialog.setMessage("Get Update details authentication...");
//                                                                               progressDialog.show();
                                                                                }

                                                                                @Override
                                                                                protected Void doInBackground(final Void... params) {
                                                                                    System.out.println("mmm Found user doInBackground1 ");
                                                                                    try {
                                                                                        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                                                                                        StrictMode.setThreadPolicy(policy);

                                                                                        runOnUiThread(new Runnable() {
                                                                                            @Override
                                                                                            public void run() {
                                                                                                if (mobile_Switch_three.isEnabled()) {
                                                                                                    Log.v("MBBB", "mobile_Switch_three if" + mobile_Switch_three.isChecked());

                                                                                                    if(etMobileNumber_three.getText().toString().length()==10) {

                                                                                                        String str = etMobileNumber_three.getText().toString();
                                                                                                        Log.v("MBBB", " String str = etMobileNumber_one.getText().toString() ; " + str);

                                                                                                        char[] charArray = str.toCharArray();
                                                                                                        Log.v("MBBB", " charArray[0] ; " + charArray[0] + " : Byte : " + (byte) charArray[0]);
                                                                                                        Log.v("MBBB", " charArray[1] ; " + charArray[1] + " : Byte : " + (byte) charArray[1]);
                                                                                                        Log.v("MBBB", " charArray[2] ; " + charArray[2] + " : Byte : " + (byte) charArray[2]);
                                                                                                        Log.v("MBBB", " charArray[3] ; " + charArray[3] + " : Byte : " + (byte) charArray[3]);
                                                                                                        Log.v("MBBB", " charArray[4] ; " + charArray[4] + " : Byte : " + (byte) charArray[4]);
                                                                                                        Log.v("MBBB", " charArray[5] ; " + charArray[5] + " : Byte : " + (byte) charArray[5]);
                                                                                                        Log.v("MBBB", " charArray[6] ; " + charArray[6] + " : Byte : " + (byte) charArray[6]);
                                                                                                        Log.v("MBBB", " charArray[7] ; " + charArray[7] + " : Byte : " + (byte) charArray[7]);
                                                                                                        Log.v("MBBB", " charArray[8] ; " + charArray[8] + " : Byte : " + (byte) charArray[8]);
                                                                                                        Log.v("MBBB", " charArray[9] ; " + charArray[9] + " : Byte : " + (byte) charArray[9]);


                                                                                                        byte switchmobilethree = 0x00;

                                                                                                        if(mobile_Switch_three.isChecked())
                                                                                                        {
                                                                                                            switchmobilethree = 0x01;
                                                                                                        }


                                                                                                        if (mBluetoothLe != null) {
                                                                                                            try {
                                                                                                                byte[] BYTE_SEND_LOG_REQUEST = {
                                                                                                                        0x41, 0x54, 0x06, 0x01, 0x03, (byte) charArray[0], (byte) charArray[1], (byte) charArray[2],
                                                                                                                        (byte) charArray[3], (byte) charArray[4], (byte) charArray[5], (byte) charArray[6], (byte) charArray[7],
                                                                                                                        (byte) charArray[8], (byte) charArray[9], switchmobilethree, 0x74,
                                                                                                                        0x20, 0x0D, 0x0A};
                                                                                                                mBluetoothLe.writeCustomCharacteristic(BYTE_SEND_LOG_REQUEST);
                                                                                                                status = true;
                                                                                                                Log.v(TAG, "MBBB DEVICE COMMUNICATION Write 613...");
                                                                                                                Log.v(TAG, "MBBB DEVICE COMMUNICATION Write 613 mobile switch three state..." + mobile_Switch_three.isChecked());


                                                                                                                StringBuilder sb = new StringBuilder();

                                                                                                                for (byte b : BYTE_SEND_LOG_REQUEST) {
                                                                                                                    if (sb.length() > 0) {
                                                                                                                        sb.append(':');
                                                                                                                    }
                                                                                                                    sb.append(String.format("%02x", b));
                                                                                                                }


                                                                                                                Log.v(TAG, "MBBB DEVICE COMMUNICATION ***** 33 Received Value from sb : " + sb);



                                                                                                            } catch (Exception e) {
                                                                                                                e.printStackTrace();
                                                                                                                Toast.makeText(getApplicationContext(), "Bluetooth Connection problem", Toast.LENGTH_SHORT).show();
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        status = false;
                                                                                                        Log.v("MBBB", "mobile_Switch_three else length error" + etMobileNumber_three.getText().toString());
                                                                                                        Log.v(TAG, "MBBB DEVICE COMMUNICATION Write 613 mobile text three state..."+etMobileNumber_three.getText().toString());

                                                                                                    }

                                                                                                } else {
                                                                                                    status = false;
                                                                                                    Log.v("MBBB", "mobile_Switch_three else" + mobile_Switch_three.isChecked());
                                                                                                }
                                                                                            }
                                                                                        });

                                                                                    } catch (Exception e) {

                                                                                    }
                                                                                    return null;
                                                                                }

                                                                                @Override
                                                                                protected void onPostExecute(final Void aVoid) {
                                                                                    //progressDialog.dismiss();

                                                                                  //  if (status) {
                                                                                        runOnUiThread(new Runnable() {
                                                                                            @Override
                                                                                            public void run() {
                                                                                                try {
                                                                                                    //   Thread.sleep(2000);
                                                                                                   // Toast.makeText(getApplicationContext(), "Send Request Log File command Successfully ", Toast.LENGTH_SHORT).show();
                                                                                                    Toast.makeText(getApplicationContext(), "Mobile Config updated Successfully 4", Toast.LENGTH_SHORT).show();

                                                                                                    try {

                                                                                                        Log.v(TAG, "MBBB dd DEVICE COMMUNICATION Write 614... sleep start");
                                                                                                        Thread.sleep(2000);
                                                                                                        // Toast.makeText(getApplicationContext(), "Send Request Log File command Successfully (Write function 613...)", Toast.LENGTH_SHORT).show();
                                                                                                        Log.v(TAG, "MBBB dd DEVICE COMMUNICATION Write 614... sleep end");
                                                                                                        AsyncTask<Void, Void, Void> execute3 = new AsyncTask<Void, Void, Void>() {

                                                                                                            private boolean status = false;


                                                                                                            @Override
                                                                                                            protected void onPreExecute() {
                                                                                                                super.onPreExecute();
                                                                                                                //      progressDialog.setIndeterminate(true);
//                                                                                                              progressDialog.setMessage("Get Update details authentication...");
//                                                                                                              progressDialog.show();
                                                                                                            }

                                                                                                            @Override
                                                                                                            protected Void doInBackground(final Void... params) {
                                                                                                                System.out.println("mmm Found user doInBackground1 ");
                                                                                                                try {
                                                                                                                    runOnUiThread(new Runnable() {
                                                                                                                        @Override
                                                                                                                        public void run() {
                                                                                                                            if (mobile_Switch_four.isEnabled()) {
                                                                                                                                Log.v("MBBB", "mobile_Switch_four if" + mobile_Switch_four.isChecked());

                                                                                                                                if(etMobileNumber_four.getText().toString().length()==10) {

                                                                                                                                    String str = etMobileNumber_four.getText().toString();
                                                                                                                                    Log.v("MBBB", " String str = etMobileNumber_one.getText().toString() ; " + str);

                                                                                                                                    char[] charArray = str.toCharArray();
                                                                                                                                    Log.v("MBBB", " charArray[0] ; " + charArray[0] + " : Byte : " + (byte) charArray[0]);
                                                                                                                                    Log.v("MBBB", " charArray[1] ; " + charArray[1] + " : Byte : " + (byte) charArray[1]);
                                                                                                                                    Log.v("MBBB", " charArray[2] ; " + charArray[2] + " : Byte : " + (byte) charArray[2]);
                                                                                                                                    Log.v("MBBB", " charArray[3] ; " + charArray[3] + " : Byte : " + (byte) charArray[3]);
                                                                                                                                    Log.v("MBBB", " charArray[4] ; " + charArray[4] + " : Byte : " + (byte) charArray[4]);
                                                                                                                                    Log.v("MBBB", " charArray[5] ; " + charArray[5] + " : Byte : " + (byte) charArray[5]);
                                                                                                                                    Log.v("MBBB", " charArray[6] ; " + charArray[6] + " : Byte : " + (byte) charArray[6]);
                                                                                                                                    Log.v("MBBB", " charArray[7] ; " + charArray[7] + " : Byte : " + (byte) charArray[7]);
                                                                                                                                    Log.v("MBBB", " charArray[8] ; " + charArray[8] + " : Byte : " + (byte) charArray[8]);
                                                                                                                                    Log.v("MBBB", " charArray[9] ; " + charArray[9] + " : Byte : " + (byte) charArray[9]);

                                                                                                                                    byte switchmobilefour = 0x00;

                                                                                                                                    if(mobile_Switch_four.isChecked())
                                                                                                                                    {
                                                                                                                                        switchmobilefour = 0x01;
                                                                                                                                    }


                                                                                                                                    if (mBluetoothLe != null) {
                                                                                                                                        try {
                                                                                                                                            byte[] BYTE_SEND_LOG_REQUEST = {
                                                                                                                                                    0x41, 0x54, 0x06, 0x01, 0x04, (byte) charArray[0], (byte) charArray[1], (byte) charArray[2],
                                                                                                                                                    (byte) charArray[3], (byte) charArray[4], (byte) charArray[5], (byte) charArray[6], (byte) charArray[7],
                                                                                                                                                    (byte) charArray[8], (byte) charArray[9], switchmobilefour, 0x74,
                                                                                                                                                    0x20, 0x0D, 0x0A};
                                                                                                                                            mBluetoothLe.writeCustomCharacteristic(BYTE_SEND_LOG_REQUEST);
                                                                                                                                            status = true;
                                                                                                                                            Log.v(TAG, "DEVICE COMMUNICATION Write 614...");
                                                                                                                                            Log.v(TAG, "MBBB DEVICE COMMUNICATION Write 614 mobile switch four state..." + mobile_Switch_four.isChecked());


                                                                                                                                            StringBuilder sb = new StringBuilder();

                                                                                                                                            for (byte b : BYTE_SEND_LOG_REQUEST) {
                                                                                                                                                if (sb.length() > 0) {
                                                                                                                                                    sb.append(':');
                                                                                                                                                }
                                                                                                                                                sb.append(String.format("%02x", b));
                                                                                                                                            }


                                                                                                                                            Log.v(TAG, "MBBB DEVICE COMMUNICATION ***** 44 Received Value from sb : " + sb);


                                                                                                                                        } catch (Exception e) {
                                                                                                                                            e.printStackTrace();
                                                                                                                                            Toast.makeText(getApplicationContext(), "Bluetooth Connection problem", Toast.LENGTH_SHORT).show();
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }

                                                                                                                                else
                                                                                                                                {
                                                                                                                                    status = false;
                                                                                                                                    Log.v("MBBB", "mobile_Switch_four else length error" + etMobileNumber_four.getText().toString());
                                                                                                                                    Log.v(TAG, "MBBB DEVICE COMMUNICATION Write 614 mobile text four state..."+etMobileNumber_four.getText().toString());

                                                                                                                                }

                                                                                                                            } else {
                                                                                                                                status = false;
                                                                                                                                Log.v("MBBB", "mobile_Switch_four else" + mobile_Switch_four.isChecked());
                                                                                                                                Log.v(TAG, "MBBB DEVICE COMMUNICATION Write 614 mobile switch four state..."+mobile_Switch_four.isChecked());

                                                                                                                            }
                                                                                                                        }
                                                                                                                    });

                                                                                                                } catch (Exception e) {
                                                                                                                }
                                                                                                                return null;
                                                                                                            }

                                                                                                            @Override
                                                                                                            protected void onPostExecute(final Void aVoid) {
                                                                                                                //progressDialog.dismiss();
                                                                                                                if (status) {
                                                                                                                    runOnUiThread(new Runnable() {
                                                                                                                        @Override
                                                                                                                        public void run() {
                                                                                                                            try {
                                                                                                                                //Thread.sleep(2000);
                                                                                                                                //Toast.makeText(getApplicationContext(), "Send Request Log File command Successfully ", Toast.LENGTH_SHORT).show();
                                                                                                                                Toast.makeText(getApplicationContext(), "Mobile config updated Successfully 5", Toast.LENGTH_SHORT).show();
                                                                                                                                finish();

                                                                                                                            } catch (Exception e) {
                                                                                                                                e.printStackTrace();
                                                                                                                            }
                                                                                                                        }
                                                                                                                    });
                                                                                                                }
                                                                                                            }
                                                                                                        }.execute();
                                                                                                    } catch (InterruptedException e) {
                                                                                                        e.printStackTrace();
                                                                                                    }

                                                                                                } catch (Exception e) {
                                                                                                    e.printStackTrace();
                                                                                                }

                                                                                            }
                                                                                        });
                                                                                   // }
                                                                                }
                                                                            }.execute();


                                                                        } catch (InterruptedException e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }

                                                                }
                                                            });
                                                        //}
                                                    }

                                                }.execute();

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }
                                });

                        }

                    }.execute();
                }
                }

        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
