package com.mmm.rikon_lc108d.activity;

import android.bluetooth.BluetoothGattCharacteristic;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.mmm.rikon_lc108d.BLE.BluetoothLeService;
import com.mmm.rikon_lc108d.LC108DApplication;
import com.mmm.rikon_lc108d.R;
import com.mmm.rikon_lc108d.db.ModeTimeTable;
import com.mmm.rikon_lc108d.db.Switch_Status_Table;

import java.util.ArrayList;

import static com.mmm.rikon_lc108d.db.ModeTimeTable.RESET_WHEELS_PASSED;
import static com.mmm.rikon_lc108d.db.ModeTimeTable.TOTAL_WHEELS_PASSED_PER_DAY;
import static com.mmm.rikon_lc108d.db.ModeTimeTable.WHEELS_PER_DAY_MAX_LIMIT;
import static com.mmm.rikon_lc108d.db.ModeTimeTable.WHEELS_PER_DAY_MIN_LIMIT;
import static com.mmm.rikon_lc108d.db.ModeTimeTable.WHEEL_COUNT;


public class SwitchActivity extends AppCompatActivity {

    private final static String TAG = SwitchActivity.class.getSimpleName();
    private final static String TAG1 = "SwitchActivity";
    TextView text_state;

    TextView min_wheel_limit, max_wheel_limit, wheels_passed;
    TextView min_wheel_limit_txt, max_wheel_limit_txt, wheels_passed_txt;

    private Switch switch_daiy_wheel_report_on_off = null;

    TextView txtswitch_dailyWheelReport;

    private Switch switch_1 = null;
    private Switch switch_2 = null;
    private Switch switch_3 = null;
    private Switch switch_4 = null;
    private Switch switch_5 = null;
    private Switch switch_6 = null;
    private Switch switch_7 = null;
    private Switch switch_8 = null;
    private Switch switch_10 = null;
    private Switch switch_proximity = null;
    private Switch switch_pressure = null;
//    private Switch switch_wheel = null;
    private Button update_switch = null;
    private ExpandableListView mGattServicesList;
    private BluetoothLeService mBluetoothLeService;
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    private String mDeviceName;
    private String mDeviceAddress;
    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            try {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        mBluetoothLeService.connect(mDeviceAddress);
            } catch (Exception e) {
                e.printStackTrace();

            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };
    private boolean mConnected = false;
    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                updateConnectionState(R.string.connected);
                text_state.setText("State : Connected");

                invalidateOptionsMenu();
                Log.v("mmmma", " DEVICE CONNECTED : " + BluetoothLeService.ACTION_GATT_CONNECTED.equals(action));

                SharedPreferences sharedpreferences = getSharedPreferences(LC108DApplication.PREF_DEVICE_CONNECTION_STATUS, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor_seq_no = sharedpreferences.edit();
                editor_seq_no.clear();
                editor_seq_no.putString(LC108DApplication.PREF_DEVICE_CONNECTION_STATUS_KEY, LC108DApplication.STATUS_CONNECTED);
                editor_seq_no.commit();

            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                updateConnectionState(R.string.disconnected);
                invalidateOptionsMenu();
                text_state.setText("State : DisConnected");
                Log.v("mmmma", " DEVICE DISCONNECTED : " + BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action));
                SharedPreferences sharedpreferences = getSharedPreferences(LC108DApplication.PREF_DEVICE_CONNECTION_STATUS, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor_seq_no = sharedpreferences.edit();
                editor_seq_no.clear();
                editor_seq_no.putString(LC108DApplication.PREF_DEVICE_CONNECTION_STATUS_KEY, LC108DApplication.STATUS_DISCONNECTED);
                editor_seq_no.commit();
                // clearUI();
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
                //displayGattServices(mBluetoothLeService.getSupportedGattServices());
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
//                displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
                Log.v("mmmma", " RECEIVING BYTE Switch status : " + intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
            }
        }
    };
    private BluetoothGattCharacteristic mNotifyCharacteristic;
    // If a given GATT characteristic is selected, check for supported features.  This sample
    // demonstrates 'Read' and 'Notify' features.  See
    // http://d.android.com/reference/android/bluetooth/BluetoothGatt.html for the complete
    // list of supported characteristic features.
    private final ExpandableListView.OnChildClickListener servicesListClickListner =
            new ExpandableListView.OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                                            int childPosition, long id) {
                    if (mGattCharacteristics != null) {
                        final BluetoothGattCharacteristic characteristic =
                                mGattCharacteristics.get(groupPosition).get(childPosition);
                        final int charaProp = characteristic.getProperties();
                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
                            // If there is an active notification on a characteristic, clear
                            // it first so it doesn't update the data field on the user interface.
                            if (mNotifyCharacteristic != null) {
                                mBluetoothLeService.setCharacteristicNotification(
                                        mNotifyCharacteristic, false);
                                mNotifyCharacteristic = null;
                            }
                            mBluetoothLeService.readCharacteristic(characteristic);
                        }
                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                            mNotifyCharacteristic = characteristic;
                            mBluetoothLeService.setCharacteristicNotification(
                                    characteristic, true);
                        }
                        return true;
                    }
                    return false;
                }
            };

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BluetoothLeService.EXTRA_DATA);
        return intentFilter;
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (mBluetoothLeService != null) {
            final boolean result = mBluetoothLeService.connect(mDeviceAddress);
            Log.d(TAG, "Connect request result=" + result);

            if (result) {
                text_state.setText("State : Connected");
                Log.d(TAG, "Connect request result=" + result);

            } else {
                Log.d(TAG, "Connect request result=" + result);

                text_state.setText("State : Disconnected");
            }
        }




    }

    @Override
    protected void onPause() {
        super.onPause();



        unregisterReceiver(mGattUpdateReceiver);
        unbindService(mServiceConnection);
        mBluetoothLeService = null;

    }

    private void updateConnectionState(final int resourceId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                text_state.setText(resourceId);



            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        text_state = (TextView) toolbar.findViewById(R.id.txt_state_switch);

        setSupportActionBar(toolbar);

        //getActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);





        update_switch = (Button) findViewById(R.id.btn_update);
        TextView mode_name = (TextView) findViewById(R.id.mode_name);

        switch_1 = (Switch) findViewById(R.id.limit_switch1);
        switch_2 = (Switch) findViewById(R.id.limit_switch2);
        switch_3 = (Switch) findViewById(R.id.limit_switch3);
        switch_4 = (Switch) findViewById(R.id.limit_switch4);
        switch_5 = (Switch) findViewById(R.id.limit_switch5);
        switch_6 = (Switch) findViewById(R.id.limit_switch6);
        switch_7 = (Switch) findViewById(R.id.limit_switch7);
        switch_8 = (Switch) findViewById(R.id.limit_switch8);
        final Switch switch_9 = (Switch) findViewById(R.id.limit_switch9);
        switch_10 = (Switch) findViewById(R.id.limit_switch10);
        switch_proximity = (Switch) findViewById(R.id.proximity_switch);
        switch_pressure = (Switch) findViewById(R.id.pressure_switch);
//        switch_wheel = (Switch) findViewById(R.id.wheel_switch);

        min_wheel_limit = (TextView) findViewById(R.id.min_wheel_limit_value);

        min_wheel_limit_txt = (TextView) findViewById(R.id.min_wheel_limit_txt);
        max_wheel_limit = (TextView) findViewById(R.id.max_wheel_limit_value);
        max_wheel_limit_txt = (TextView) findViewById(R.id.max_wheel_limit_txt);

        switch_daiy_wheel_report_on_off = (Switch) findViewById(R.id.daily_wheel_report_status_switch);

        wheels_passed = (TextView) findViewById(R.id.wheels_passed_Value);
        wheels_passed_txt = (TextView) findViewById(R.id.wheelsPassed_txt);
        txtswitch_dailyWheelReport = (TextView) findViewById(R.id.daily_wheel_report_text) ;

        Cursor mCur = getContentResolver().query(ModeTimeTable.CONTENT_URI,
                null,
                null, null,
                null);

        String checkNameMode = "";

        if (mCur != null && mCur.moveToFirst()) {
            do {
                boolean getStatus = mCur.getInt(mCur.getColumnIndex(ModeTimeTable.STATUS)) > 0;
                Log.v("mmm", "Mode Name getStatus : " + getStatus);
                //TO get the
                if (getStatus) {
                    checkNameMode = mCur.getString(mCur.getColumnIndex(ModeTimeTable.MODE_NAME));
                    Log.v("mmm", "Mode Name if checkNameMode : " + checkNameMode);
                } else {
                    Log.v("mmm", "Mode Name else  checkNameMode : " + checkNameMode);
                }
            } while (mCur.moveToNext());
        }

        //set selected mode name
        mode_name.setText(""+ checkNameMode);

        Cursor getStatus = getContentResolver().query(Switch_Status_Table.CONTENT_URI,
                null,
                Switch_Status_Table.MODE_NAME + "=?", new String[]{checkNameMode},
                null);

        if (getStatus != null && getStatus.moveToFirst()) {

            String name = getStatus.getString(getStatus.getColumnIndex(Switch_Status_Table.MODE_NAME));
            Log.v("mmm", "Mode Name name.equals(\"DUAL_LINE_MODE\")) : " + name);

            if (name.equals("DUAL LINE MODE")) {
                Log.v("mmm", "Mode Name name.equals(\"DUAL_LINE_MODE\"))iff : " + name);
                switch_1.setChecked(true);
                switch_1.setEnabled(false);
                switch_2.setChecked(true);
                switch_2.setEnabled(false);

/*

                switch_3.setChecked(true);
                switch_3.setEnabled(false);
                switch_4.setChecked(true);
                switch_4.setEnabled(false);
                switch_5.setChecked(true);
                switch_5.setEnabled(false);
                switch_6.setChecked(true);
                switch_6.setEnabled(false);
                switch_7.setChecked(true);
                switch_7.setEnabled(false);
                switch_8.setChecked(true);
                switch_8.setEnabled(false);
*/


            } else {
                boolean switch1 = getStatus.getInt(getStatus.getColumnIndex(Switch_Status_Table.LIMIT_SWITCH1)) > 0;
                switch_1.setChecked(switch1);
                boolean switch2 = getStatus.getInt(getStatus.getColumnIndex(Switch_Status_Table.LIMIT_SWITCH2)) > 0;
                switch_2.setChecked(switch2);


            }

            boolean switch3 = getStatus.getInt(getStatus.getColumnIndex(Switch_Status_Table.LIMIT_SWITCH3)) > 0;
            switch_3.setChecked(switch3);

            boolean switch4 = getStatus.getInt(getStatus.getColumnIndex(Switch_Status_Table.LIMIT_SWITCH4)) > 0;
            switch_4.setChecked(switch4);

            boolean switch5 = getStatus.getInt(getStatus.getColumnIndex(Switch_Status_Table.LIMIT_SWITCH5)) > 0;
            switch_5.setChecked(switch5);

            boolean switch6 = getStatus.getInt(getStatus.getColumnIndex(Switch_Status_Table.LIMIT_SWITCH6)) > 0;
            switch_6.setChecked(switch6);

            boolean switch7 = getStatus.getInt(getStatus.getColumnIndex(Switch_Status_Table.LIMIT_SWITCH7)) > 0;
            switch_7.setChecked(switch7);

            boolean switch8 = getStatus.getInt(getStatus.getColumnIndex(Switch_Status_Table.LIMIT_SWITCH8)) > 0;
            switch_8.setChecked(switch8);




            boolean switch9 = getStatus.getInt(getStatus.getColumnIndex(Switch_Status_Table.LIMIT_SWITCH9)) > 0;
            switch_9.setChecked(switch9);

            boolean switch10 = getStatus.getInt(getStatus.getColumnIndex(Switch_Status_Table.LIMIT_SWITCH10)) > 0;
            switch_10.setChecked(switch10);

            boolean proximity = getStatus.getInt(getStatus.getColumnIndex(Switch_Status_Table.PROXIMITY_SWITCH)) > 0;
            switch_proximity.setChecked(proximity);


            if(name.equals("TRACK MODE"))

            {



                boolean daily_wheel_status_switch_value = getStatus.getInt(getStatus.getColumnIndex(Switch_Status_Table.DALIY_WHEEL_REPORT)) > 0;
                switch_daiy_wheel_report_on_off.setChecked(daily_wheel_status_switch_value);


                Log.v(TAG, " rvice : Read from Database" + daily_wheel_status_switch_value);


                Cursor checkExists = getContentResolver().
                        query(ModeTimeTable.CONTENT_URI,
                                null,
                                ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.TRACK_MODE},
                                null);
                String getWheelPassedMinValue="";
                String getWheelPassedMaxValue="";
                String gettotal_wheel_passed_per_day="";
                if (checkExists != null && checkExists.moveToFirst()) {

                    getWheelPassedMinValue = checkExists.getString(checkExists.getColumnIndex(WHEELS_PER_DAY_MIN_LIMIT));
                    getWheelPassedMaxValue = checkExists.getString(checkExists.getColumnIndex(WHEELS_PER_DAY_MAX_LIMIT));

                    gettotal_wheel_passed_per_day = checkExists.getString(checkExists.getColumnIndex(TOTAL_WHEELS_PASSED_PER_DAY));



                    min_wheel_limit.setText(String.valueOf(getWheelPassedMinValue));
                    max_wheel_limit.setText(String.valueOf(getWheelPassedMaxValue));


                    wheels_passed.setText(String.valueOf(gettotal_wheel_passed_per_day));


                } else {


                }



            }


            else
            {
                min_wheel_limit.setVisibility(View.GONE);
                max_wheel_limit.setVisibility(View.GONE);

                switch_daiy_wheel_report_on_off.setVisibility(View.GONE);

                wheels_passed.setVisibility(View.GONE);
                txtswitch_dailyWheelReport.setVisibility(View.GONE);
                min_wheel_limit_txt.setVisibility(View.GONE);
                max_wheel_limit_txt.setVisibility(View.GONE);
                wheels_passed_txt.setVisibility(View.GONE);

            }


//            boolean pressure = getStatus.getInt(getStatus.getColumnIndex(Switch_Status_Table.PRESSURE_SWITCH)) > 0;
//            switch_pressure.setChecked(pressure);

//            boolean wheel_sensor = getStatus.getInt(getStatus.getColumnIndex(Switch_Status_Table.WHEEL_SENSOR)) > 0;
//            switch_wheel.setChecked(wheel_sensor);
            mCur.close();

            SharedPreferences sharedpreferences1 = getSharedPreferences(LC108DApplication.PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
            mDeviceName = sharedpreferences1.getString(LC108DApplication.PREF_DEVICE_NAME, "");
            mDeviceAddress = sharedpreferences1.getString(LC108DApplication.PREF_DEVICE_ADDRESS, "");
            Log.v(TAG, "mDeviceName :" + mDeviceName + "mDeviceAddress : " + mDeviceAddress);


        }









        update_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //update switch status
                Log.v(TAG, "onstart");
                //get mode selection

                Cursor mCur = getContentResolver().query(ModeTimeTable.CONTENT_URI,
                        null,
                        null, null,
                        null);

                String nameMode = "";

                if (mCur != null && mCur.moveToFirst()) {
                    do {
                        boolean getStatus = mCur.getInt(mCur.getColumnIndex(ModeTimeTable.STATUS)) > 0;
                        Log.v("mmm", "Mode Name getStatus : " + getStatus);
                        //TO get the
                        if (getStatus) {
                            nameMode = mCur.getString(mCur.getColumnIndex(ModeTimeTable.MODE_NAME));
                            Log.v("mmm", "Mode Name if : " + nameMode);
                        } else {
                            Log.v("mmm", "Mode Name else : " + nameMode);
                        }
                    } while (mCur.moveToNext());
                }


                if (mBluetoothLeService != null) {
                    Log.v("mmm", "Mode Name : " + nameMode);
                    ContentValues values = new ContentValues();
                    values.put(Switch_Status_Table.LIMIT_SWITCH1, switch_1.isChecked());
                    values.put(Switch_Status_Table.LIMIT_SWITCH2, switch_2.isChecked());
                    values.put(Switch_Status_Table.LIMIT_SWITCH3, switch_3.isChecked());
                    values.put(Switch_Status_Table.LIMIT_SWITCH4, switch_4.isChecked());
                    values.put(Switch_Status_Table.LIMIT_SWITCH5, switch_5.isChecked());
                    values.put(Switch_Status_Table.LIMIT_SWITCH6, switch_6.isChecked());
                    values.put(Switch_Status_Table.LIMIT_SWITCH7, switch_7.isChecked());
                    values.put(Switch_Status_Table.LIMIT_SWITCH8, switch_8.isChecked());
                    values.put(Switch_Status_Table.LIMIT_SWITCH9, switch_9.isChecked());
                    values.put(Switch_Status_Table.LIMIT_SWITCH10, switch_10.isChecked());
                    values.put(Switch_Status_Table.PROXIMITY_SWITCH, switch_proximity.isChecked());
                    values.put(Switch_Status_Table.PRESSURE_SWITCH, switch_pressure.isChecked());
                    values.put(Switch_Status_Table.DALIY_WHEEL_REPORT, switch_daiy_wheel_report_on_off.isChecked());
//                values.put(Switch_Status_Table.WHEEL_SENSOR, switch_wheel.isChecked());

                    String where = Switch_Status_Table.MODE_NAME + " = '" + nameMode + "'";

                    getContentResolver().update(Switch_Status_Table.CONTENT_URI, values, where, null);

                    mCur.close();
                    Log.v(TAG, "onsave");

                    byte switch1 = 0x00;
                    if (switch_1.isChecked()) {
                        switch1 = 0x01;
                    }
                    byte switch2 = 0x00;
                    if (switch_2.isChecked()) {
                        switch2 = 0x01;
                    }
                    byte switch3 = 0x00;
                    if (switch_3.isChecked()) {
                        switch3 = 0x01;
                    }
                    byte switch4 = 0x00;
                    if (switch_4.isChecked()) {
                        switch4 = 0x01;
                    }
                    byte switch5 = 0x00;
                    if (switch_5.isChecked()) {
                        switch5 = 0x01;
                    }
                    byte switch6 = 0x00;
                    if (switch_6.isChecked()) {
                        switch6 = 0x01;
                    }
                    byte switch7 = 0x00;
                    if (switch_7.isChecked()) {
                        switch7 = 0x01;
                    }
                    byte switch8 = 0x00;
                    if (switch_8.isChecked()) {
                        switch8 = 0x01;
                    }
                    byte switch9 = 0x00;
                    if (switch_9.isChecked()) {
                        switch9 = 0x01;
                    }
                    byte switch10 = 0x00;
                    if (switch_10.isChecked()) {
                        switch10 = 0x01;
                    }
                    byte switch_proxi = 0x00;
                    if (switch_proximity.isChecked()) {
                        switch_proxi = 0x01;
                    }

                    byte switch_daily_wheel_status = 0x00;
                    if (switch_daiy_wheel_report_on_off.isChecked()) {
                        switch_daily_wheel_status = 0x01;
                    }
//                byte wheel_sensor = 0x00;
//                if (switch_wheel.isChecked()) {
//                    wheel_sensor = 0x01;
//                }
                    Log.v(TAG, "switch value byte : " + switch1 + switch2 + switch3 +
                            switch4 + switch5 + switch6 + switch7 + switch8 +
                            switch9 + switch10 + switch_proxi + switch_daily_wheel_status);
                    //send command to device
                    if (mBluetoothLeService != null) {
                        Log.v(TAG, "mBluetoothLeService != null : ");
                        byte[] BYTE_SEND_LOG_REQUEST = {0x41, 0x54, 0x01, 0x01, switch1, switch2, switch3,
                                switch4, switch5, switch6, switch7, switch8,
                                switch9, switch10, switch_proxi, switch_daily_wheel_status, 0x20,
                                0x20, 0x0D, 0x0A};
                        Log.v(TAG, "Write function 11...");



                        StringBuilder sb2=new StringBuilder();
                        for (byte bb : BYTE_SEND_LOG_REQUEST) {
                            if (sb2.length() > 0) {
                                sb2.append(':');
                            }
                            sb2.append(String.format("%02x", bb));
                        }

                        Log.v("LC108D-LOG", "UUUU Request service setSwitchStatus BYTE_SEND : "+ sb2);


                        try {
                            try {
                                mBluetoothLeService.writeCustomCharacteristic(BYTE_SEND_LOG_REQUEST);
                                Toast.makeText(getApplicationContext(), "Update Switch status", Toast.LENGTH_SHORT).show();
                                finish();

                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Bluetooth Connection problem", Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Error: Try to connect Device", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Log.v(TAG, "mBluetoothLeService = null : ");
                    }
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "Bluetooth Connection problem", Toast.LENGTH_SHORT).show();
                }
            }

        });

        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gatt_services, menu);
        if (mConnected) {
            menu.findItem(R.id.menu_connect).setVisible(true);
            menu.findItem(R.id.menu_disconnect).setVisible(false);
        } else {
            menu.findItem(R.id.menu_connect).setVisible(false);
            menu.findItem(R.id.menu_disconnect).setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        try {
            switch (item.getItemId()) {
                case R.id.menu_connect:
                    mBluetoothLeService.connect(mDeviceAddress);
                    text_state.setText("State : Connected");
                    return true;
                case R.id.menu_disconnect:
                    mBluetoothLeService.disconnect();
                    text_state.setText("State : DisConnected");
                    return true;
                case android.R.id.home:
                    onBackPressed();
                    return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        SharedPreferences sharedpreferences = getSharedPreferences(LC108DApplication.PREF_DEVICE_CONNECTION_STATUS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor_seq_no = sharedpreferences.edit();
        editor_seq_no.clear();
        editor_seq_no.putString(LC108DApplication.PREF_DEVICE_CONNECTION_STATUS_KEY, LC108DApplication.STATUS_DISCONNECTED);
        editor_seq_no.commit();
//        try {
//            unbindService(mServiceConnection);
//            mBluetoothLeService.disconnect();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        mBluetoothLeService = null;


       /* unbindService(mServiceConnection);
        mBluetoothLeService = null;*/

    }



}
