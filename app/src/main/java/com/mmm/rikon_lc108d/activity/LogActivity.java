package com.mmm.rikon_lc108d.activity;

import android.app.AlertDialog;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.mmm.rikon_lc108d.BLE.BluetoothLeService;
import com.mmm.rikon_lc108d.BLE.DeviceControlActivity;
import com.mmm.rikon_lc108d.BLE.SampleGattAttributes;
import com.mmm.rikon_lc108d.LC108DApplication;
import com.mmm.rikon_lc108d.R;
import com.mmm.rikon_lc108d.db.Login_Table;

import java.io.File;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class LogActivity extends AppCompatActivity {

    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";
    private final static String TAG = LogActivity.class.getSimpleName();
    public static final String BROADCAST_ACTION = "com.mmm.rikon_lc108d.BLE.broadcastreceiverlogvalue";
    public static TextView mReceiveByte;
    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";
    Timer mytime = new Timer();
    MyTask task = new MyTask();
    TextView text_state;
    private TextView mDataField;
    private Button bLogRequest = null;

    private Button bLogReset;
    private String mDeviceName;
    private String mDeviceAddress;
    private ExpandableListView mGattServicesList;
    private BluetoothLeService mBluetoothLeService;

    boolean mBlinking = false;

    MyBroadCastReceiver myBroadCastReceiver;


    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            mBluetoothLeService.connect(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    private boolean mConnected = false;
    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    // or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                updateConnectionState(R.string.connected);
                invalidateOptionsMenu();
                text_state.setText("State : Connected");

                Log.v("mmmma", " DEVICE CONNECTED : " + BluetoothLeService.ACTION_GATT_CONNECTED.equals(action));

            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                updateConnectionState(R.string.disconnected);
                invalidateOptionsMenu();
                text_state.setText("State : DisConnected");

                Log.v("mmmma", " DEVICE DISCONNECTED : " + BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action));
                SharedPreferences sharedpreferences = getSharedPreferences(LC108DApplication.PREF_DEVICE_CONNECTION_STATUS, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor_seq_no = sharedpreferences.edit();
                editor_seq_no.clear();
                editor_seq_no.putString(LC108DApplication.PREF_DEVICE_CONNECTION_STATUS_KEY, LC108DApplication.STATUS_CONNECTED);
                editor_seq_no.commit();
                clearUI();
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
                //displayGattServices(mBluetoothLeService.getSupportedGattServices());
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
                displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
                Log.v("mmmma", " RECEIVING BYTE : " + intent.getStringExtra(BluetoothLeService.EXTRA_DATA));

                String s = intent.getStringExtra(BluetoothLeService.EXTRA_DATA);
                Log.v(TAG, "Receiving display data : " + intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
                Log.v(TAG, "Receiving display data : " + s);

            }
        }
    };
    private BluetoothGattCharacteristic mNotifyCharacteristic;
    // If a given GATT characteristic is selected, check for supported features.  This sample
    // demonstrates 'Read' and 'Notify' features.  See
    // http://d.android.com/reference/android/bluetooth/BluetoothGatt.html for the complete
    // list of supported characteristic features.
    private final ExpandableListView.OnChildClickListener servicesListClickListner =
            new ExpandableListView.OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                                            int childPosition, long id) {
                    if (mGattCharacteristics != null) {
                        final BluetoothGattCharacteristic characteristic =
                                mGattCharacteristics.get(groupPosition).get(childPosition);
                        final int charaProp = characteristic.getProperties();
                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
                            // If there is an active notification on a characteristic, clear
                            // it first so it doesn't update the data field on the user interface.
                            if (mNotifyCharacteristic != null) {
//                                mBluetoothLeService.setCharacteristicNotification(
//                                        mNotifyCharacteristic, false);
                                mNotifyCharacteristic = null;
                            }
                            mBluetoothLeService.readCharacteristic(characteristic);
                        }
                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                            mNotifyCharacteristic = characteristic;
//                            mBluetoothLeService.setCharacteristicNotification(
//                                    characteristic, true);
                        }
                        return true;
                    }
                    return false;
                }
            };

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BluetoothLeService.EXTRA_DATA);
        return intentFilter;
    }

    public static void setDataText(final long receive_byte_count) {
        LogActivity.mReceiveByte.setText("Receive data" + receive_byte_count);
    }

    private void clearUI() {
        //mGattServicesList.setAdapter((SimpleExpandableListAdapter) null);
        mDataField.setText(R.string.no_data);
    }


    private void registerMyReceiver() {

        try
        {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(BROADCAST_ACTION);
            registerReceiver(myBroadCastReceiver, intentFilter);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }

    /**
     * MyBroadCastReceiver is responsible to receive broadCast from register action
     * */
    class MyBroadCastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            try {
                Log.d(TAG, "onReceive() called");
                Animation anim = new AlphaAnimation(0.0f, 1.0f);


                if (intent != null) {
                    String dataReceived = intent.getStringExtra("data");

                    Log.d(TAG, "BluetoothLeService Display data received set logtext" + dataReceived);

                    if (dataReceived.equals("completed")) {

                        mReceiveByte.clearAnimation();
                        mReceiveByte.setAlpha(1.0f);
                        mBlinking = false;
                        mReceiveByte.setText("Completed");

                    }

                    else if(dataReceived.equals("progress")||dataReceived.equals("moreinprogress"))
                    {

                        anim.setDuration(10000);
                        anim.setStartOffset(100);
                        anim.setRepeatMode(Animation.REVERSE);
                        anim.setRepeatCount(Animation.RELATIVE_TO_PARENT);

                        if(!mBlinking){
                            mReceiveByte.startAnimation(anim);
                            mBlinking = true;
                        } else{
                            mReceiveByte.clearAnimation();
                            mReceiveByte.setAlpha(1.0f);
                            mBlinking = false;
                        }

                        Log.d(TAG,"Display BluetoothLeService Display data received set logtext"+dataReceived);
                        mReceiveByte.setText("File Transfer in progress.Please wait...");

                    }

                }


            } catch (Exception e) {

            }
        }
    }



                @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_request);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        text_state = (TextView) toolbar.findViewById(R.id.txt_state_log);
        setSupportActionBar(toolbar);
        //getActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Intent intent = getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);
        mReceiveByte = (TextView) findViewById(R.id.txt_receive_byte);


        Button btnFileFolder = (Button) findViewById(R.id.btn_file_folder);
        btnFileFolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("MMMMMM", "Click File folder button");
                openFolder("LC108D");
            }
        });

        Button btnLastFile = (Button) findViewById(R.id.btn_open_lastfile);
        btnLastFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri selectedUri = Uri.parse(Environment.getExternalStorageDirectory() + "/LC108D/");
                File file = new File(selectedUri.getPath());
                Date lastModDate = new Date(file.lastModified());
                Log.i("MMM", "File last modified @ : " + lastModDate.toString());
            }
        });

        // Sets up UI references.
        /*((TextView) findViewById(R.id.device_address)).setText(mDeviceAddress);
        mGattServicesList = (ExpandableListView) findViewById(R.id.gatt_services_list);
        mGattServicesList.setOnChildClickListener(servicesListClickListner);
        mConnectionState = (TextView) findViewById(R.id.connection_state);
    */
        mDataField = (TextView) findViewById(R.id.receive_data);

        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

        bLogReset = (Button) findViewById(R.id.btn_reset_log);


        SharedPreferences sharedpreferences_login = getSharedPreferences(LC108DApplication.PREF_LOGIN_DETAILS, Context.MODE_PRIVATE);
        String login_status = sharedpreferences_login.getString(LC108DApplication.PREF_LOGIN_STATUS, "");
        String user_type = sharedpreferences_login.getString(LC108DApplication.PREF_LOGIN_USER_TYPE, "");
        String login_user = sharedpreferences_login.getString(LC108DApplication.PREF_LOGIN_USER, "");

        Log.d(TAG,"Display user type"+user_type);

        if(user_type.equals(Login_Table.USER)) {

            bLogReset.setVisibility(View.GONE);

        }

        else
        {
            bLogReset.setVisibility(View.VISIBLE);
        }

        bLogReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //send byte to device for request log file length




                AlertDialog.Builder alertDialog = new AlertDialog.Builder(LogActivity.this);
                alertDialog.setTitle("Password");
                alertDialog.setMessage("Enter Password");

                final EditText input = new EditText(LogActivity.this);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);
                alertDialog.setView(input);

                alertDialog.setPositiveButton("YES",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String password = input.getText().toString();

                                if ("rcs2017".equals(password)) {
                                    Toast.makeText(getApplicationContext(),
                                            "Password Matched", Toast.LENGTH_SHORT).show();

                                    if (mBluetoothLeService != null) {
                                        try {
                                            byte[] BYTE_SEND_LOG_RESET_REQUEST = {0x41, 0x54, 0x08, 0x01, 0x20,
                                                    0x20, 0x20, 0x20, 0x20, 0x20,
                                                    0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,
                                                    0x20, 0x0D, 0x0A};

                                            StringBuilder sb1 = new StringBuilder();
                                            for (byte bb : BYTE_SEND_LOG_RESET_REQUEST) {
                                                if (sb1.length() > 0) {
                                                    sb1.append(':');
                                                }
                                                sb1.append(String.format("%02x", bb));
                                            }
                                            mBluetoothLeService.writeCustomCharacteristic(BYTE_SEND_LOG_RESET_REQUEST);
                                            Log.v(TAG, "DEVICE COMMUNICATION Write 81..."+sb1);
                                            Toast.makeText(getApplicationContext(), "Reset Log Successfully..", Toast.LENGTH_SHORT).show();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            Toast.makeText(getApplicationContext(), "Bluetooth Connection problem", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                } else {
                                    Toast.makeText(getApplicationContext(),
                                            "Wrong Password!", Toast.LENGTH_SHORT).show();

                                }
                            }
                        });

                alertDialog.setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                alertDialog.show();





            }
        });

        bLogRequest = (Button) findViewById(R.id.btn_log_request);
        bLogRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //send byte to device for request log file length

                if (mBluetoothLeService != null) {
                    BluetoothLeService.RECEIVE_BYTE_COUNT = 0;
                    BluetoothLeService.countheader = 0;
                    try {
                        byte[] BYTE_SEND_LOG_REQUEST = {
                                0x41, 0x54, 0x02, 0x01, 0x03, 0x04, 0x05,
                                0x06, 0x07, 0x08, 0x09, 0x41,
                                0x56, 0x45, 0x66, 0x6d, 0x74,
                                0x20, 0x0D, 0x0A};


                        mBluetoothLeService.writeCustomCharacteristic(BYTE_SEND_LOG_REQUEST);
                        Log.v(TAG, "DEVICE COMMUNICATION Write 21...");
                        Toast.makeText(getApplicationContext(), "Send Request Log File command Successfully...)", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Bluetooth Connection problem", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gatt_services, menu);
        if (mConnected) {
            menu.findItem(R.id.menu_connect).setVisible(false);
            menu.findItem(R.id.menu_disconnect).setVisible(true);
        } else {
            menu.findItem(R.id.menu_connect).setVisible(true);
            menu.findItem(R.id.menu_disconnect).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        try {
            switch (item.getItemId()) {
                case R.id.menu_connect:
                    mBluetoothLeService.connect(mDeviceAddress);
                    text_state.setText("State : Connected");
                    return true;
                case R.id.menu_disconnect:
                    mBluetoothLeService.disconnect();
                    text_state.setText("State : DisConnected");
                    return true;
                case android.R.id.home:
                    onBackPressed();
                    return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (mBluetoothLeService != null) {
            final boolean result = mBluetoothLeService.connect(mDeviceAddress);
            Log.d(TAG, "Connect request result=" + result);


        }
        BluetoothLeService.countheader = 0;


        myBroadCastReceiver = new MyBroadCastReceiver();
        registerMyReceiver();

        /*try {
            mytime.scheduleAtFixedRate(task, 0, 100);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    @Override
    protected void onPause() {
        super.onPause();
        BluetoothLeService.countheader = 0;

        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharedPreferences sharedpreferences = getSharedPreferences(LC108DApplication.PREF_DEVICE_CONNECTION_STATUS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor_seq_no = sharedpreferences.edit();
        editor_seq_no.clear();
        editor_seq_no.putString(LC108DApplication.PREF_DEVICE_CONNECTION_STATUS_KEY, LC108DApplication.STATUS_DISCONNECTED);
        editor_seq_no.commit();
//        try {
//            unbindService(mServiceConnection);
//            mBluetoothLeService.disconnect();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        mBluetoothLeService = null;
        try {
            mytime.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private void sendMyBroadCast(String  serialno)
    {
        try
        {
            Log.d(TAG,"Serial No set data received if sendbroadcast called"+serialno);

            Intent broadCastIntent = new Intent();
            broadCastIntent.setAction(DeviceControlActivity.BROADCAST_ACTION);

            // uncomment this line if you want to send data
            broadCastIntent.putExtra("data", serialno);

            sendBroadcast(broadCastIntent);

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void updateConnectionState(final int resourceId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //mConnectionState.setText(resourceId);
            }
        });
    }

    public void openFolder(String mFoldername) {

        Uri path= Uri.parse(Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+mFoldername+"/");
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(path, "resource/folder");     // **//or "text/csv" for Other Types**
        startActivity(Intent.createChooser(intent, "Open folder"));
//        try {
//            Log.v("MMMMMM", "1. openFolder");
//            Uri selectedUri = Uri.parse(Environment.getExternalStorageDirectory() + "/DCIM/Camera");
//            Log.v("MMMMMM", "2. openFolder : "+selectedUri.getPath());
//            Intent intent = new Intent(Intent.ACTION_VIEW);
//            intent.setDataAndType(selectedUri, "resource/folder");
//            startActivity(intent);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
      }

    private void displayData(String data) {
        if (data != null) {
            mDataField.setText(data);
        }
    }

    // Demonstrates how to iterate through the supported GATT Services/Characteristics.
    // In this sample, we populate the data structure that is bound to the ExpandableListView
    // on the UI.
    private void displayGattServices(List<BluetoothGattService> gattServices) {
        if (gattServices == null) return;
        String uuid = null;
        String unknownServiceString = getResources().getString(R.string.unknown_service);
        String unknownCharaString = getResources().getString(R.string.unknown_characteristic);
        ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<HashMap<String, String>>();
        ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData
                = new ArrayList<ArrayList<HashMap<String, String>>>();
        mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {
            HashMap<String, String> currentServiceData = new HashMap<String, String>();
            uuid = gattService.getUuid().toString();
            currentServiceData.put(
                    LIST_NAME, SampleGattAttributes.lookup(uuid, unknownServiceString));
            currentServiceData.put(LIST_UUID, uuid);
            gattServiceData.add(currentServiceData);

            ArrayList<HashMap<String, String>> gattCharacteristicGroupData =
                    new ArrayList<HashMap<String, String>>();
            List<BluetoothGattCharacteristic> gattCharacteristics =
                    gattService.getCharacteristics();
            ArrayList<BluetoothGattCharacteristic> charas =
                    new ArrayList<BluetoothGattCharacteristic>();

            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                charas.add(gattCharacteristic);
                HashMap<String, String> currentCharaData = new HashMap<String, String>();
                uuid = gattCharacteristic.getUuid().toString();
                currentCharaData.put(
                        LIST_NAME, SampleGattAttributes.lookup(uuid, unknownCharaString));
                currentCharaData.put(LIST_UUID, uuid);
                gattCharacteristicGroupData.add(currentCharaData);
            }
            mGattCharacteristics.add(charas);
            gattCharacteristicData.add(gattCharacteristicGroupData);
        }

        SimpleExpandableListAdapter gattServiceAdapter = new SimpleExpandableListAdapter(
                this,
                gattServiceData,
                android.R.layout.simple_expandable_list_item_2,
                new String[]{LIST_NAME, LIST_UUID},
                new int[]{android.R.id.text1, android.R.id.text2},
                gattCharacteristicData,
                android.R.layout.simple_expandable_list_item_2,
                new String[]{LIST_NAME, LIST_UUID},
                new int[]{android.R.id.text1, android.R.id.text2}
        );
        mGattServicesList.setAdapter(gattServiceAdapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private class MyTask extends TimerTask {

        @Override
        public void run() {
            Log.v("mmml", "handler running");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (BluetoothLeService.RECEIVE_BYTE_COUNT_FINISH != 1)
                       // LogActivity.mReceiveByte.setText("Receive Byte : " + BluetoothLeService.RECEIVE_BYTE_COUNT);
                    LogActivity.mReceiveByte.setText("File transfer in progress.Please wait...");
                }
            });
        }
    }
}
