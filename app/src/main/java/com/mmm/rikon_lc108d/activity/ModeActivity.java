package com.mmm.rikon_lc108d.activity;

import android.app.Dialog;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.mmm.rikon_lc108d.BLE.BluetoothLeService;
import com.mmm.rikon_lc108d.LC108DApplication;
import com.mmm.rikon_lc108d.R;
import com.mmm.rikon_lc108d.db.ModeTimeTable;
import com.mmm.rikon_lc108d.db.Switch_Status_Table;

import java.util.ArrayList;

import static com.mmm.rikon_lc108d.R.id.daily_wheel_report_status_switch;
import static com.mmm.rikon_lc108d.R.id.dialog_title;
import static com.mmm.rikon_lc108d.R.id.min_wheel_limit_value;
import static com.mmm.rikon_lc108d.R.id.pressure_limit;
import static com.mmm.rikon_lc108d.R.id.txt_offtime;
import static com.mmm.rikon_lc108d.R.id.wheel_count;
import static com.mmm.rikon_lc108d.db.ModeTimeTable.PRESSURE_LIMIT;
import static com.mmm.rikon_lc108d.db.ModeTimeTable.RESET_WHEELS_PASSED;
import static com.mmm.rikon_lc108d.db.ModeTimeTable.SETTING_PASSED_PER_DAY;
import static com.mmm.rikon_lc108d.db.ModeTimeTable.TOTAL_WHEELS_PASSED_PER_DAY;
import static com.mmm.rikon_lc108d.db.ModeTimeTable.WHEELS_PER_DAY_MAX_LIMIT;
import static com.mmm.rikon_lc108d.db.ModeTimeTable.WHEELS_PER_DAY_MIN_LIMIT;
import static com.mmm.rikon_lc108d.db.ModeTimeTable.WHEEL_COUNT;
import static java.lang.Integer.parseInt;

public class ModeActivity extends AppCompatActivity {

    private final static String TAG = ModeActivity.class.getSimpleName();
    public static RecyclerView recyclerView = null;
    public static BluetoothLeService mBluetoothLe;
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    TextView text_state;

    private String mDeviceName;
    private String mDeviceAddress;
    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLe = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLe.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            try {
                mBluetoothLe.connect(mDeviceAddress);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName)  {
            mBluetoothLe = null;
        }
    };
    private boolean mConnected = false;
    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                text_state.setText("State : Connected");
                updateConnectionState(R.string.connected);
                invalidateOptionsMenu();
                Log.v(TAG, " DEVICE CONNECTED : " + BluetoothLeService.ACTION_GATT_CONNECTED.equals(action));
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                text_state.setText("State : DisConnected");
                updateConnectionState(R.string.disconnected);
                invalidateOptionsMenu();
                Log.v(TAG, " DEVICE DISCONNECTED : " + BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action));
                // clearUI();
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
                //displayGattServices(mBluetoothLe.getSupportedGattServices());
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
//                displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
                Log.v("mmmma", " RECEIVING BYTE Switch status : " + intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
            }
        }
    };

    private BluetoothGattCharacteristic mNotifyCharacteristic;
    // If a given GATT characteristic is selected, check for supported features.  This sample
    // demonstrates 'Read' and 'Notify' features.  See
    // http://d.android.com/reference/android/bluetooth/BluetoothGatt.html for the complete
    // list of supported characteristic features.
    private final ExpandableListView.OnChildClickListener servicesListClickListner =
            new ExpandableListView.OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                                            int childPosition, long id) {
                    if (mGattCharacteristics != null) {
                        final BluetoothGattCharacteristic characteristic =
                                mGattCharacteristics.get(groupPosition).get(childPosition);
                        final int charaProp = characteristic.getProperties();
                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
                            // If there is an active notification on a characteristic, clear
                            // it first so it doesn't update the data field on the user interface.
                            if (mNotifyCharacteristic != null) {
                                mBluetoothLe.setCharacteristicNotification(
                                        mNotifyCharacteristic, false);
                                mNotifyCharacteristic = null;
                            }
                            mBluetoothLe.readCharacteristic(characteristic);
                        }
                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                            mNotifyCharacteristic = characteristic;
                            mBluetoothLe.setCharacteristicNotification(
                                    characteristic, true);
                        }
                        return true;
                    }
                    return false;
                }
            };


    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BluetoothLeService.EXTRA_DATA);
        return intentFilter;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        try {
//            unbindService(mServiceConnection);
//            mBluetoothLe.disconnect();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        mBluetoothLe = null;

       /* unbindService(mServiceConnection);
        mBluetoothLe = null;*/

    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (mBluetoothLe != null) {
            final boolean result = mBluetoothLe.connect(mDeviceAddress);
            Log.d(TAG, "Connect request result=" + result);

            if(result)
                text_state.setText("State : Connected");
            else
                text_state.setText("State : Disconnected");
        }


    }

    private void updateConnectionState(final int resourceId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                text_state.setText(resourceId);
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gatt_services, menu);
        if (mConnected) {
            menu.findItem(R.id.menu_connect).setVisible(false);
            menu.findItem(R.id.menu_disconnect).setVisible(true);
        } else {
            menu.findItem(R.id.menu_connect).setVisible(true);
            menu.findItem(R.id.menu_disconnect).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        try {
            switch (item.getItemId()) {
                case R.id.menu_connect:
                    mBluetoothLe.connect(mDeviceAddress);
                    return true;
                case R.id.menu_disconnect:
                    mBluetoothLe.disconnect();
                    return true;
                case android.R.id.home:
                    onBackPressed();
                    return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mode_menu);

        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarmode);
        text_state = (TextView) toolbar.findViewById(R.id.txt_state_mode);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView = (RecyclerView) findViewById(R.id.mode_list);

        Log.d("ClassName","::"+TAG);

        ArrayList<String> modeName = new ArrayList<String>();
        ArrayList<Boolean> modeStatus = new ArrayList<Boolean>();
//        modeName.add("Progressive Mode");
        //modeStatus.add(0);
//        modeName.add("Track Mode");
        //  modeStatus.add(1);
//        modeName.add("Injector Mode");
        //  modeStatus.add(0);
//        modeName.add("Dual line Mode");
        // modeStatus.add(0);

        SharedPreferences sharedpreferences1 = getSharedPreferences(LC108DApplication.PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
        mDeviceName = sharedpreferences1.getString(LC108DApplication.PREF_DEVICE_NAME, "");
        mDeviceAddress = sharedpreferences1.getString(LC108DApplication.PREF_DEVICE_ADDRESS, "");

        String res=mDeviceName+"::"+mDeviceAddress;
        Log.d(TAG,":::"+res);

        Log.v(TAG, "mDeviceName :" + mDeviceName + "mDeviceAddress : " + mDeviceAddress);

        Cursor mCursor = getContentResolver().query(ModeTimeTable.CONTENT_URI,
                null,
                null, null,
                null);

        if (mCursor != null && mCursor.moveToFirst()) {
            do {
                boolean status = mCursor.getInt(mCursor.getColumnIndex(ModeTimeTable.STATUS)) > 0;
                String name = mCursor.getString(mCursor.getColumnIndex(ModeTimeTable.MODE_NAME));
                modeName.add(name);
                modeStatus.add(status);
            } while (mCursor.moveToNext());
        }

        ContentAdapter adapter = new ContentAdapter(getApplicationContext(), modeName, modeStatus);

        recyclerView.setAdapter(adapter);
        // recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    public static class ContentAdapter extends RecyclerView.Adapter<ViewHolder> {
        // Set numbers of List in RecyclerView.

        Context mCtx;
        ArrayList<String> modeName = new ArrayList<String>();
        ArrayList<Boolean> modeStatus = new ArrayList<Boolean>();

        public ContentAdapter(Context applicationContext,
                              ArrayList<String> modeName1,
                              ArrayList<Boolean> modeStatus1) {
            modeName = modeName1;
            modeStatus = modeStatus1;
            mCtx = applicationContext;
        }

        public static byte[] hexStringToByteArray(String s) {
            int len = s.length();
            byte[] data = new byte[len / 2];
            for (int i = 0; i < len; i += 2) {
                data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                        + Character.digit(s.charAt(i + 1), 16));
            }
            return data;
        }

        private static byte hexStringToByte(String data) {
            return (byte) ((Character.digit(data.charAt(0), 16) << 4)
                    + Character.digit(data.charAt(1), 16));
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            holder.main_Text.setText(modeName.get(position).toString());
             int modeEditablePosition = -1;

            //hidden code for update board status
            if (modeStatus.get(position)) {
                holder.radioButton.setChecked(true);
                modeEditablePosition = position;
                holder.radioButton.setEnabled(true);
                holder.main_Text.setEnabled(true);
            } else {
                holder.radioButton.setChecked(false);
                holder.radioButton.setEnabled(false);
                holder.main_Text.setEnabled(false);
            }

//            if (modeStatus.get(position)) {
//                holder.radioButton.setChecked(true);
//                modeEditablePosition = position;
//              //  holder.radioButton.setEnabled(true);
//               // holder.main_Text.setEnabled(true);
//            } else {
//                holder.radioButton.setChecked(false);
//              //  holder.radioButton.setEnabled(false);
//               // holder.main_Text.setEnabled(false);
//            }

            final int finalModeEditablePosition = modeEditablePosition;
            holder.radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    buttonView.setChecked(isChecked);
                    String mode_name = "";
                    if (position == 0) {
                        if(mBluetoothLe!=null) {
                            mode_name = ModeTimeTable.PROGRESSIVE_MODE;
                            ContentValues values = new ContentValues();
                            values.put(ModeTimeTable.STATUS, true);
                            mCtx.getContentResolver().update(ModeTimeTable.CONTENT_URI, values, ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.PROGRESSIVE_MODE});

                            ContentValues values2 = new ContentValues();
                            values2.put(ModeTimeTable.STATUS, false);
                            mCtx.getContentResolver().update(ModeTimeTable.CONTENT_URI, values2, ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.TRACK_MODE});

                            ContentValues values3 = new ContentValues();
                            values3.put(ModeTimeTable.STATUS, false);
                            mCtx.getContentResolver().update(ModeTimeTable.CONTENT_URI, values3, ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.INJECTOR_MODE});

                            ContentValues values4 = new ContentValues();
                            values4.put(ModeTimeTable.STATUS, false);
                            mCtx.getContentResolver().update(ModeTimeTable.CONTENT_URI, values4, ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.DUAL_LINE_MODE});
                        }
                        else
                        {
                            Toast.makeText(mCtx, "Bluetooth Connection Problem", Toast.LENGTH_SHORT).show();
                        }

                    }
                    if (position == 1) {
                        if(mBluetoothLe!=null) {
                            mode_name = ModeTimeTable.TRACK_MODE;
                            ContentValues values = new ContentValues();
                            values.put(ModeTimeTable.STATUS, false);
                            mCtx.getContentResolver().update(ModeTimeTable.CONTENT_URI, values, ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.PROGRESSIVE_MODE});

                            ContentValues values2 = new ContentValues();
                            values2.put(ModeTimeTable.STATUS, true);
                            mCtx.getContentResolver().update(ModeTimeTable.CONTENT_URI, values2, ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.TRACK_MODE});

                            ContentValues values3 = new ContentValues();
                            values3.put(ModeTimeTable.STATUS, false);
                            mCtx.getContentResolver().update(ModeTimeTable.CONTENT_URI, values3, ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.INJECTOR_MODE});

                            ContentValues values4 = new ContentValues();
                            values4.put(ModeTimeTable.STATUS, false);
                            mCtx.getContentResolver().update(ModeTimeTable.CONTENT_URI, values4, ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.DUAL_LINE_MODE});
                        }
                        else
                        {
                            Toast.makeText(mCtx, "Bluetooth Connection Problem", Toast.LENGTH_SHORT).show();
                        }
                    }
                    if (position == 2) {
                        if(mBluetoothLe!=null)
                        {
                        mode_name = ModeTimeTable.INJECTOR_MODE;
                        ContentValues values = new ContentValues();
                        values.put(ModeTimeTable.STATUS, false);
                        mCtx.getContentResolver().update(ModeTimeTable.CONTENT_URI, values, ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.PROGRESSIVE_MODE});

                        ContentValues values2 = new ContentValues();
                        values2.put(ModeTimeTable.STATUS, false);
                        mCtx.getContentResolver().update(ModeTimeTable.CONTENT_URI, values2, ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.TRACK_MODE});

                        ContentValues values3 = new ContentValues();
                        values3.put(ModeTimeTable.STATUS, true);
                        mCtx.getContentResolver().update(ModeTimeTable.CONTENT_URI, values3, ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.INJECTOR_MODE});

                        ContentValues values4 = new ContentValues();
                        values4.put(ModeTimeTable.STATUS, false);
                        mCtx.getContentResolver().update(ModeTimeTable.CONTENT_URI, values4, ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.DUAL_LINE_MODE});
                        }
                        else
                        {
                            Toast.makeText(mCtx, "Bluetooth Connection Problem", Toast.LENGTH_SHORT).show();
                        }
                    }
                    if (position == 3) {
                        if(mBluetoothLe!=null)
                        {
                        mode_name = ModeTimeTable.DUAL_LINE_MODE;
                        ContentValues values = new ContentValues();
                        values.put(ModeTimeTable.STATUS, false);
                        mCtx.getContentResolver().update(ModeTimeTable.CONTENT_URI, values, ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.PROGRESSIVE_MODE});

                        ContentValues values2 = new ContentValues();
                        values2.put(ModeTimeTable.STATUS, false);
                        mCtx.getContentResolver().update(ModeTimeTable.CONTENT_URI, values2, ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.TRACK_MODE});

                        ContentValues values3 = new ContentValues();
                        values3.put(ModeTimeTable.STATUS, false);
                        mCtx.getContentResolver().update(ModeTimeTable.CONTENT_URI, values3, ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.INJECTOR_MODE});

                        ContentValues values4 = new ContentValues();
                        values4.put(ModeTimeTable.STATUS, true);
                        mCtx.getContentResolver().update(ModeTimeTable.CONTENT_URI, values4, ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.DUAL_LINE_MODE});
                        }
                        else
                        {
                            Toast.makeText(mCtx, "Bluetooth Connection Problem", Toast.LENGTH_SHORT).show();
                        }
                    }

                    ArrayList<String> modeName = new ArrayList<String>();
                    ArrayList<Boolean> modeStatus = new ArrayList<Boolean>();

                    Cursor mCursor = mCtx.getContentResolver().query(ModeTimeTable.CONTENT_URI,
                            null,
                            null, null,
                            null);

                    if (mCursor != null && mCursor.moveToFirst()) {
                        do {
                            boolean status = mCursor.getInt(mCursor.getColumnIndex(ModeTimeTable.STATUS)) > 0;
                            String name = mCursor.getString(mCursor.getColumnIndex(ModeTimeTable.MODE_NAME));
                            modeName.add(name);
                            modeStatus.add(status);
                        } while (mCursor.moveToNext());
                    }

                    ModeActivity.ContentAdapter adapter = new ModeActivity.ContentAdapter(mCtx, modeName, modeStatus);

                    ModeActivity.recyclerView.setAdapter(adapter);
                    // recyclerView.setHasFixedSize(true);
                    ModeActivity.recyclerView.setLayoutManager(new LinearLayoutManager(mCtx));
                }
            });

            holder.main_Text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog addNoteDialog = new Dialog(holder.main_Text.getContext());
                    addNoteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    addNoteDialog.setContentView(R.layout.custom_dialog);
                    addNoteDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                    RelativeLayout onTime_layout = (RelativeLayout) addNoteDialog.findViewById(R.id.layout_ontime);
                    RelativeLayout offTime_layout = (RelativeLayout) addNoteDialog.findViewById(R.id.layout_offtime);
                    RelativeLayout wheelCount_layout = (RelativeLayout) addNoteDialog.findViewById(R.id.layout_wheel_count);
                    RelativeLayout pressure_layout = (RelativeLayout) addNoteDialog.findViewById(R.id.layout_pressure);

                    //pressure_limit_text
                    RelativeLayout pressure_limit_text = (RelativeLayout) addNoteDialog.findViewById(R.id.pressure_limit_text);

                    RelativeLayout pressure_switch_only = (RelativeLayout) addNoteDialog.findViewById(R.id.pressure_switch_only);
                    RelativeLayout pressure_limit_input = (RelativeLayout) addNoteDialog.findViewById(R.id.pressure_limit_input);


                    RelativeLayout wheels_per_day = (RelativeLayout) addNoteDialog.findViewById(R.id.layout_wheel_min_max_count);

                    TextView text = (TextView) addNoteDialog.findViewById(dialog_title);

                    //Edit text change listener
                    final EditText ontime_hours = (EditText) addNoteDialog.findViewById(R.id.on_hour);
                    final EditText ontime_min = (EditText) addNoteDialog.findViewById(R.id.on_min);
                    final EditText ontime_sec = (EditText) addNoteDialog.findViewById(R.id.on_sec);
                    final EditText offtime_hours = (EditText) addNoteDialog.findViewById(R.id.off_hour);
                    final EditText offtime_min = (EditText) addNoteDialog.findViewById(R.id.off_min);
                    final EditText offtime_sec = (EditText) addNoteDialog.findViewById(R.id.off_sec);
                    final EditText txt_wheel_count = (EditText) addNoteDialog.findViewById(R.id.wheel_count);
                    final EditText txt_pressure_limit = (EditText) addNoteDialog.findViewById(R.id.pressure_limit);
                    final Switch pressureLimit_switch = (Switch) addNoteDialog.findViewById(R.id.mode_pressure_limit_switch);
                   // wheels_passed_per_day_count

                    final EditText wheel_min_limit = (EditText) addNoteDialog.findViewById(R.id.wheel_min_limit);
                    final EditText wheel_max_limit = (EditText) addNoteDialog.findViewById(R.id.wheel_max_limit);
                    final CheckBox reset_wheel_limit = (CheckBox) addNoteDialog.findViewById(R.id.reset_wheel_limit);
                    final EditText wheels_passed_count = (EditText) addNoteDialog.findViewById(R.id.wheels_passed_count);

                    final Switch setting_wheel_pass_switch = (Switch) addNoteDialog.findViewById(R.id.setting_wheel_pass_switch);

                    setting_wheel_pass_switch.setChecked(false);


                    offtime_sec.setText("10");
                    txt_pressure_limit.setText("5");
                    txt_wheel_count.setText("5");
                    wheel_max_limit.setText("10000");
                    wheel_min_limit.setText("1");
                    reset_wheel_limit.setChecked(false);
                    wheels_passed_count.setText("0");




                    if(pressureLimit_switch.isChecked()) {
                        pressureLimit_switch.setEnabled(false);
                    } else {
                        pressureLimit_switch.setEnabled(true);
                    }

                    //position start with 0
                    if (position == 0) {
                        text.setText("PROGRESSIVE MODE");
                        wheelCount_layout.setVisibility(View.GONE);
                        pressure_layout.setVisibility(View.GONE);
                        wheels_per_day.setVisibility(View.GONE);
                        onTime_layout.setVisibility(View.VISIBLE);
                        offTime_layout.setVisibility(View.VISIBLE);
                        Cursor checkExists = holder.main_Text.getContext().getContentResolver().
                                query(ModeTimeTable.CONTENT_URI,
                                        null,
                                        ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.PROGRESSIVE_MODE},
                                        null);
                        String getOnTime = "000000000000";
                        String getOffTime = "000000000000";
                        if (checkExists != null && checkExists.moveToFirst()) {
                            getOnTime = checkExists.getString(checkExists.getColumnIndex(ModeTimeTable.TIME));
                            Log.v("LC108D-LOG", "getOnTime = " + getOnTime);
                            Log.v("LC108D-LOG", "getOnTime.charAt(0)+getOnTime.charAt(1) = " + getOnTime.charAt(0) + getOnTime.charAt(1));
                            Log.v("LC108D-LOG", "getOnTime.charAt(2)+getOnTime.charAt(3) = " + getOnTime.charAt(2) + getOnTime.charAt(3));
                            int getOnHour = getOnTime.charAt(2) + getOnTime.charAt(3);
                            ontime_hours.setText(String.valueOf(getOnTime.charAt(0) + String.valueOf(getOnTime.charAt(1))));
                            ontime_min.setText(String.valueOf(getOnTime.charAt(2) + String.valueOf(getOnTime.charAt(3))));
                            ontime_sec.setText(String.valueOf(getOnTime.charAt(4) + String.valueOf(getOnTime.charAt(5))));
                            offtime_hours.setText(String.valueOf(getOnTime.charAt(6) + String.valueOf(getOnTime.charAt(7))));
                            offtime_min.setText(String.valueOf(getOnTime.charAt(8) + String.valueOf(getOnTime.charAt(9))));
                            offtime_sec.setText(String.valueOf(getOnTime.charAt(10) + String.valueOf(getOnTime.charAt(11))));
                        } else {

                        }
                    } else if (position == 1) {
                        text.setText("TRACK MODE");
                        offTime_layout.setVisibility(View.GONE);
                        onTime_layout.setVisibility(View.VISIBLE);
                        wheelCount_layout.setVisibility(View.VISIBLE);
                        wheels_per_day.setVisibility(View.VISIBLE);
                        pressure_switch_only.setVisibility(View.GONE);

                        pressure_limit_text.setVisibility(View.VISIBLE);
                        pressure_limit_input.setVisibility(View.VISIBLE);


                        Cursor getStatus = holder.main_Text.getContext().getContentResolver().query(Switch_Status_Table.CONTENT_URI,
                                null,
                                Switch_Status_Table.MODE_NAME + "=?", new String[]{"TRACK MODE"},
                                null);

                        if (getStatus != null && getStatus.moveToFirst()) {

                            String name = getStatus.getString(getStatus.getColumnIndex(Switch_Status_Table.MODE_NAME));

                            if (name.equals("TRACK MODE")) {
                                boolean switch9 = getStatus.getInt(getStatus.getColumnIndex(Switch_Status_Table.DALIY_WHEEL_REPORT)) > 0;
                                setting_wheel_pass_switch.setChecked(switch9);

                                Log.d(TAG,"Enter into the ssss Update mode TRACK MODE"+switch9);

                                if(switch9) {

                                    Log.d(TAG,"Enter into the ssss Update mode TRACK MODE 111"+switch9);


                                    setting_wheel_pass_switch.setChecked(true);
                                    reset_wheel_limit.setEnabled(true);
                                    wheel_min_limit.setEnabled(true);
                                    wheel_max_limit.setEnabled(true);
                                   // txt_pressure_limit.setEnabled(true);
                                }
                                else
                                {

                                    Log.d(TAG,"Enter into the ssss Update mode TRACK MODE 222"+switch9);


                                    setting_wheel_pass_switch.setChecked(false);
                                    reset_wheel_limit.setEnabled(false);
                                    wheel_min_limit.setEnabled(false);
                                    wheel_max_limit.setEnabled(false);
                                   // txt_pressure_limit.setEnabled(false);

                                }
                            }
                        }
                        //set On time and wheel count

                        Cursor checkExists = holder.main_Text.getContext().getContentResolver().
                                query(ModeTimeTable.CONTENT_URI,
                                        null,
                                        ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.TRACK_MODE},
                                        null);
                        String getOnTime = "000000000000";
                        String getWheelCount = "";
                        String getWheelPassedMinValue="";
                        String getWheelPassedMaxValue="";
                        String  getreset_wheel_passed ="";
                        String get_passed_per_day_setting = "";
                        String gettotal_wheel_passed_per_day="";
                        String getpressure_limit = "";
                        if (checkExists != null && checkExists.moveToFirst()) {
                            getOnTime = checkExists.getString(checkExists.getColumnIndex(ModeTimeTable.TIME));
                            getWheelCount = checkExists.getString(checkExists.getColumnIndex(WHEEL_COUNT));
                            getWheelPassedMinValue = checkExists.getString(checkExists.getColumnIndex(WHEELS_PER_DAY_MIN_LIMIT));
                            getWheelPassedMaxValue = checkExists.getString(checkExists.getColumnIndex(WHEELS_PER_DAY_MAX_LIMIT));
                            getreset_wheel_passed = checkExists.getString(checkExists.getColumnIndex(RESET_WHEELS_PASSED));
                            gettotal_wheel_passed_per_day = checkExists.getString(checkExists.getColumnIndex(TOTAL_WHEELS_PASSED_PER_DAY));
                            getpressure_limit= checkExists.getString(checkExists.getColumnIndex(PRESSURE_LIMIT));
                          //  SETTING_PASSED_PER_DAY

                            /*get_passed_per_day_setting = checkExists.getString(checkExists.getColumnIndex(SETTING_PASSED_PER_DAY));
                            Log.v("LC108D-LOG", "get_passed_per_day_setting = " + get_passed_per_day_setting);

                            if(get_passed_per_day_setting.equals("0"))
                            {
                                setting_wheel_pass_switch.setChecked(false);
                                reset_wheel_limit.setEnabled(false);
                                wheel_min_limit.setEnabled(false);
                                wheel_max_limit.setEnabled(false);

                            }

                            else

                            {
                                setting_wheel_pass_switch.setChecked(true);
                                reset_wheel_limit.setEnabled(true);
                                wheel_min_limit.setEnabled(true);
                                wheel_max_limit.setEnabled(true);
                            }*/


                            Log.v("LC108D-LOG", "getOnTime = " + getOnTime);
                            Log.v("LC108D-LOG", "getOnTime.charAt(0)+getOnTime.charAt(1) = " + getOnTime.charAt(0) + getOnTime.charAt(1));
                            Log.v("LC108D-LOG", "getOnTime.charAt(2)+getOnTime.charAt(3) = " + getOnTime.charAt(2) + getOnTime.charAt(3));
                            int getOnHour = getOnTime.charAt(2) + getOnTime.charAt(3);
                            ontime_hours.setText(String.valueOf(getOnTime.charAt(0) + String.valueOf(getOnTime.charAt(1))));
                            ontime_min.setText(String.valueOf(getOnTime.charAt(2) + String.valueOf(getOnTime.charAt(3))));
                            ontime_sec.setText(String.valueOf(getOnTime.charAt(4) + String.valueOf(getOnTime.charAt(5))));
                            txt_wheel_count.setText(String.valueOf(getWheelCount));
                            wheel_min_limit.setText(String.valueOf(getWheelPassedMinValue));
                            wheel_max_limit.setText(String.valueOf(getWheelPassedMaxValue));
                            txt_pressure_limit.setText(String.valueOf(getpressure_limit));
/*
                            if(reset_wheel_limit.isChecked())
                            {
                                reset_wheel_limit.setChecked(true);
                            }

                            else
                            {
                                reset_wheel_limit.setChecked(false);

                            }*/

/*
                            if(getreset_wheel_passed.equals("true"))
                            {
                                reset_wheel_limit.setChecked(true);
                            }
                            else if(getreset_wheel_passed.equals("false"))
                            {
                                reset_wheel_limit.setChecked(false);
                            }*/

                            Log.d(TAG,"wheels per day"+String.valueOf(gettotal_wheel_passed_per_day));

                            wheels_passed_count.setText(String.valueOf(gettotal_wheel_passed_per_day));


                        } else {
                        }
                    } else if (position == 2) {

                        Log.d(TAG,"Enter into injector mode item click ");

                        text.setText("INJECTOR MODE");
                        wheelCount_layout.setVisibility(View.GONE);
                        wheels_per_day.setVisibility(View.GONE);
                        onTime_layout.setVisibility(View.VISIBLE);
                        offTime_layout.setVisibility(View.VISIBLE);
                        pressure_layout.setVisibility(View.VISIBLE);
                        Cursor checkExists = holder.main_Text.getContext().getContentResolver().
                                query(ModeTimeTable.CONTENT_URI,
                                        null,
                                        ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.INJECTOR_MODE},
                                        null);
                        String getOnTime = "000000000000";
                        String getPressureLimit = "";


                        Cursor getStatus1 = mCtx.getContentResolver().query(Switch_Status_Table.CONTENT_URI,
                                null,
                                Switch_Status_Table.MODE_NAME + "=?", new String[]{"INJECTOR MODE"},
                                null);

                        if (getStatus1 != null && getStatus1.moveToFirst()) {
                            Log.v("MMMM", "Enter into the Update mode INJECTOR MODE");
                            String name = getStatus1.getString(getStatus1.getColumnIndex(Switch_Status_Table.MODE_NAME));
                            boolean pressure = getStatus1.getInt(getStatus1.getColumnIndex(Switch_Status_Table.LIMIT_SWITCH9)) > 0;
                            pressureLimit_switch.setChecked(pressure);

                            if(pressure) {
                                Log.v("MMMM", "1.Enter into the Update mode INJECTOR MODE true");
                                txt_pressure_limit.setEnabled(false);
                            } else {
                                Log.v("MMMM", "2. Enter into the Update mode INJECTOR MODE false");
                                txt_pressure_limit.setEnabled(true);
                            }
                        }

                        Cursor getStatus = holder.main_Text.getContext().getContentResolver().query(Switch_Status_Table.CONTENT_URI,
                                null,
                                Switch_Status_Table.MODE_NAME + "=?", new String[]{"INJECTOR MODE"},
                                null);

                        if (getStatus != null && getStatus.moveToFirst()) {

                            String name = getStatus.getString(getStatus.getColumnIndex(Switch_Status_Table.MODE_NAME));

                            if (name.equals("INJECTOR MODE")) {
                                boolean switch9 = getStatus.getInt(getStatus.getColumnIndex(Switch_Status_Table.LIMIT_SWITCH9)) > 0;
                                pressureLimit_switch.setChecked(switch9);

                                if(switch9) {
                                    Log.v("MMMM", "1.Enter into the Update mode INJECTOR MODE true");
                                    txt_pressure_limit.setEnabled(false);
                                } else {
                                    Log.v("MMMM", "2. Enter into the Update mode INJECTOR MODE false");
                                    txt_pressure_limit.setEnabled(true);
                                }
                            }
                            }




                        if (checkExists != null && checkExists.moveToFirst()) {
                            getOnTime = checkExists.getString(checkExists.getColumnIndex(ModeTimeTable.TIME));
                            getPressureLimit = checkExists.getString(checkExists.getColumnIndex(PRESSURE_LIMIT));
                            Log.v("LC108D-LOG", "getOnTime = " + getOnTime);
                            Log.v("LC108D-LOG", "getOnTime.charAt(0)+getOnTime.charAt(1) = " + getOnTime.charAt(0) + getOnTime.charAt(1));
                            Log.v("LC108D-LOG", "getOnTime.charAt(2)+getOnTime.charAt(3) = " + getOnTime.charAt(2) + getOnTime.charAt(3));
                            int getOnHour = getOnTime.charAt(2) + getOnTime.charAt(3);
                            ontime_hours.setText(String.valueOf(getOnTime.charAt(0) + String.valueOf(getOnTime.charAt(1))));
                            ontime_min.setText(String.valueOf(getOnTime.charAt(2) + String.valueOf(getOnTime.charAt(3))));
                            ontime_sec.setText(String.valueOf(getOnTime.charAt(4) + String.valueOf(getOnTime.charAt(5))));
                            offtime_hours.setText(String.valueOf(getOnTime.charAt(6) + String.valueOf(getOnTime.charAt(7))));
                            offtime_min.setText(String.valueOf(getOnTime.charAt(8) + String.valueOf(getOnTime.charAt(9))));
                            offtime_sec.setText(String.valueOf(getOnTime.charAt(10) + String.valueOf(getOnTime.charAt(11))));
                            txt_pressure_limit.setText(String.valueOf(getPressureLimit));

                        } else {

                        }
                    } else if (position == 3) {
                        text.setText("DUAL LINE MODE");
                        wheelCount_layout.setVisibility(View.GONE);
                        pressure_layout.setVisibility(View.GONE);
                        wheels_per_day.setVisibility(View.GONE);
                        onTime_layout.setVisibility(View.VISIBLE);
                        offTime_layout.setVisibility(View.VISIBLE);
                        Cursor checkExists = holder.main_Text.getContext().getContentResolver().
                                query(ModeTimeTable.CONTENT_URI,
                                        null,
                                        ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.DUAL_LINE_MODE},
                                        null);
                        String getOnTime = "000000000000";
                        String getOffTime = "000000000000";
                        if (checkExists != null && checkExists.moveToFirst()) {
                            getOnTime = checkExists.getString(checkExists.getColumnIndex(ModeTimeTable.TIME));
                            Log.v("LC108D-LOG", "getOnTime = " + getOnTime);
                            Log.v("LC108D-LOG", "getOnTime.charAt(0)+getOnTime.charAt(1) = " + getOnTime.charAt(0) + getOnTime.charAt(1));
                            Log.v("LC108D-LOG", "getOnTime.charAt(2)+getOnTime.charAt(3) = " + getOnTime.charAt(2) + getOnTime.charAt(3));
                            int getOnHour = getOnTime.charAt(2) + getOnTime.charAt(3);
                            ontime_hours.setText(String.valueOf(getOnTime.charAt(0) + String.valueOf(getOnTime.charAt(1))));
                            ontime_min.setText(String.valueOf(getOnTime.charAt(2) + String.valueOf(getOnTime.charAt(3))));
                            ontime_sec.setText(String.valueOf(getOnTime.charAt(4) + String.valueOf(getOnTime.charAt(5))));
                            offtime_hours.setText(String.valueOf(getOnTime.charAt(6) + String.valueOf(getOnTime.charAt(7))));
                            offtime_min.setText(String.valueOf(getOnTime.charAt(8) + String.valueOf(getOnTime.charAt(9))));
                            offtime_sec.setText(String.valueOf(getOnTime.charAt(10) + String.valueOf(getOnTime.charAt(11))));

                        } else {
                        }
                    }

                    ImageButton save_mode = (ImageButton) addNoteDialog.findViewById(R.id.save_time);

                    ontime_min.addTextChangedListener(new TextWatcher() {
                        public void afterTextChanged(Editable s) {
//                            ontime_min.setText(String.valueOf(s.toString().length()));
                            if (!s.toString().equals("")) {
                                // Log.v("mmmasdf", "afterTextChanged" +String.valueOf(s.toString().length()));
                                int min_value = (parseInt(s.toString()));
                                if (min_value > 60) {
                                    ontime_min.setError("Enter minute value as <= 60");
                                }
                            } else {
                                // ontime_min.setText("00");
                            }
                        }

                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                        }
                    });

                    wheel_min_limit.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                            try
                            {
                                if (!s.toString().equals("")) {
                                    int min_value = (parseInt(s.toString()));
                                    if (min_value > (parseInt(wheel_max_limit.getText().toString()))) {
                                        wheel_min_limit.setError("Enter wheel min value as < wheel max limit ");
                                    }
                                } else {

                                }
                            }

                            catch (NumberFormatException NPE)
                            {
                                Log.d(TAG,"Number format excpetion");
                            }


                        }
                    });

                    wheel_max_limit.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                            try
                            {

                                if (!s.toString().equals("")) {
                                    int max_value = (parseInt(s.toString()));
                                    if (max_value >65000) {
                                        wheel_max_limit.setError("Enter wheel max value as <= 65000");
                                    }
                                } else {

                                }


                            }

                            catch (NumberFormatException NPE)
                            {

                            }

                        }
                    });


                    wheel_max_limit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                            } else {

                                if (wheel_max_limit.getText().toString().equals("")) {
                                    //wheel_max_limit.setText("10000");
                                    wheel_max_limit.setError("max value not empty");
                                }

                               // int max_value = (parseInt(wheel_max_limit.getText().toString()));

                                /*if (max_value > 65000) {
                                    wheel_max_limit.setText("10000");
                                }*/
                            }
                        }
                    });


                    wheel_min_limit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                            } else {

                                if (wheel_min_limit.getText().toString().equals("")) {
                                    //wheel_min_limit.setText("1");
                                    wheel_min_limit.setError("min value not empty");

                                }

                               // int min_value = (parseInt(wheel_min_limit.getText().toString()));

                                /*if (min_value > 65000 ||  min_value > (parseInt(wheel_max_limit.getText().toString()))) {
                                    wheel_max_limit.setText("1");
                                }*/

                            }
                        }
                    });




                    ontime_min.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                //Toast.makeText(getApplicationContext(), "got the focus", Toast.LENGTH_LONG).show();
                            } else {
                                if (ontime_min.getText().toString().equals("")) {
                                    ontime_min.setText("00");
                                }
                                //  Toast.makeText(getApplicationContext(), "lost the focus", Toast.LENGTH_LONG).show();
                                int max_value = (parseInt(ontime_min.getText().toString()));
                                if (max_value > 59) {
                                    ontime_min.setText("59");
                                }
                            }
                        }
                    });


                    ontime_sec.addTextChangedListener(new TextWatcher() {
                        public void afterTextChanged(Editable s) {
//                            ontime_min.setText(String.valueOf(s.toString().length()));
                            if (!s.toString().equals("")) {
                                // Log.v("mmmasdf", "afterTextChanged" +String.valueOf(s.toString().length()));
                                int min_value = (parseInt(s.toString()));
                                if (min_value > 59) {
                                    ontime_sec.setError("Enter minute value as <= 59");
                                }
                            }
                        }

                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                        }
                    });
                    ontime_sec.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                            } else {
                                if (ontime_sec.getText().toString().equals("")) {
                                    ontime_sec.setText("00");
                                }
                                int max_value = (parseInt(ontime_sec.getText().toString()));
                                if (max_value > 59) {
                                    ontime_sec.setText("59");
                                }
                            }
                        }
                    });
                    offtime_hours.addTextChangedListener(new TextWatcher() {
                        public void afterTextChanged(Editable s) {
                            if (!s.toString().equals("")) {
                                int min_value = (parseInt(s.toString()));
                                if (min_value > 99) {
                                    offtime_hours.setError("Enter minute value as <= 99");
                                }
                            }
                        }

                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                        }
                    });
                    offtime_hours.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                            } else {
                                if (offtime_hours.getText().toString().equals("")) {
                                    offtime_hours.setText("00");
                                }
                                int max_value = (parseInt(offtime_hours.getText().toString()));
                                if (max_value > 99) {
                                    offtime_hours.setText("99");
                                }
                            }
                        }
                    });
                    offtime_min.addTextChangedListener(new TextWatcher() {
                        public void afterTextChanged(Editable s) {
                            if (!s.toString().equals("")) {
                                int min_value = (parseInt(s.toString()));
                                if (min_value > 59) {
                                    offtime_min.setError("Enter minute value as <= 59");
                                }
                            }
                        }

                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                        }
                    });
                    offtime_min.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                            } else {
                                if (offtime_min.getText().toString().equals("")) {
                                    offtime_min.setText("00");
                                }
                                int max_value = (parseInt(offtime_min.getText().toString()));

                                if (max_value > 59) {
                                    offtime_min.setText("59");
                                }
                            }
                        }
                    });
                    offtime_sec.addTextChangedListener(new TextWatcher() {
                        public void afterTextChanged(Editable s) {
                            if (!s.toString().equals("")) {
                                int min_value = (parseInt(s.toString()));
                                if (min_value > 59) {

                                    offtime_sec.setError("Enter minute value as <= 59");
                                }
                            }
                        }

                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                        }
                    });


                    /////
                    offtime_sec.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                            } else {
                                if (offtime_sec.getText().toString().equals("")) {
                                    offtime_sec.setText("00");
                                }
                                int max_value = (parseInt(offtime_sec.getText().toString()));

                                if (max_value > 59) {
                                    offtime_sec.setText("59");
                                }
                            }
                        }
                    });





                   txt_wheel_count.addTextChangedListener(new TextWatcher() {
                        public void afterTextChanged(Editable s) {

                            try
                            {
                                if (!s.toString().equals("")) {
                                    int min_value = (parseInt(s.toString()));
                                    if (min_value > 59999) {
                                        txt_wheel_count.setError("Enter wheel count value as <= 59999");
                                    }
                                }
                            }

                            catch (NumberFormatException NPE)
                            {

                            }


                        }

                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                        }
                    });
                    txt_wheel_count.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                            } else {

                                if (txt_wheel_count.getText().toString().equals("")) {
                                    txt_wheel_count.setText("0");
                                }

                                int min_value = (parseInt(txt_wheel_count.getText().toString()));

                                if (min_value > 59999) {
                                    txt_wheel_count.setText("59999");
                                }
                            }
                        }
                    });

                    setting_wheel_pass_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            Log.v("Switch State=", ""+isChecked);
                            if(setting_wheel_pass_switch.isChecked())
                            {
                                wheel_min_limit.setEnabled(true);
                                wheel_max_limit.setEnabled(true);
                                reset_wheel_limit.setEnabled(true);
                              //  txt_pressure_limit.setEnabled(true);

                            }
                            else
                            {
                                wheel_min_limit.setEnabled(false);
                                wheel_max_limit.setEnabled(false);
                                reset_wheel_limit.setEnabled(false);
                               // txt_pressure_limit.setEnabled(false);
                            }
                        }
                    });

                    //set changes for Pressure Limit switch
//                    Cursor mCur = mCtx.getContentResolver().query(ModeTimeTable.CONTENT_URI,
//                            null,
//                            null, null,
//                            null);
//
//                    String checkNameMode = "";
//
//                    if (mCur != null && mCur.moveToFirst()) {
//                        do {
//                            boolean getStatus = mCur.getInt(mCur.getColumnIndex(ModeTimeTable.STATUS)) > 0;
//                            Log.v("mmm", "Mode Name getStatus : " + getStatus);
//                            //TO get the
//                            if (getStatus) {
//                                checkNameMode = mCur.getString(mCur.getColumnIndex(ModeTimeTable.MODE_NAME));
//                                Log.v("mmm", "Mode Name if checkNameMode : " + checkNameMode);
//
//                            } else {
//                                Log.v("mmm", "Mode Name else  checkNameMode : " + checkNameMode);
//
//                            }
//                        } while (mCur.moveToNext());
//                    }





                  /*  Cursor getStatusdailywheel = mCtx.getContentResolver().query(ModeTimeTable.CONTENT_URI,
                            null,
                            ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.TRACK_MODE},
                            null);

                    if (getStatusdailywheel != null && getStatusdailywheel.moveToFirst()) {
                        Log.v("MMMM", "Enter into the Update mode TRACK MODE");
                        boolean dailystatusonoff = getStatusdailywheel.getInt(getStatusdailywheel.getColumnIndex(ModeTimeTable.SETTING_PASSED_PER_DAY)) > 0;
                        Log.v("MMMM", "Enter into the Update mode TRACK MODE"+dailystatusonoff);

                        setting_wheel_pass_switch.setChecked(dailystatusonoff);

                    }


*/


                    Cursor getStatusswitchtrack = holder.main_Text.getContext().getContentResolver().query(Switch_Status_Table.CONTENT_URI,
                            null,
                            Switch_Status_Table.MODE_NAME + "=?", new String[]{"TRACK MODE"},
                            null);

                    if (getStatusswitchtrack != null && getStatusswitchtrack.moveToFirst()) {

                        String name = getStatusswitchtrack.getString(getStatusswitchtrack.getColumnIndex(Switch_Status_Table.MODE_NAME));

                        if (name.equals("TRACK MODE")) {
                            boolean switch9 = getStatusswitchtrack.getInt(getStatusswitchtrack.getColumnIndex(Switch_Status_Table.DALIY_WHEEL_REPORT)) > 0;
                            setting_wheel_pass_switch.setChecked(switch9);

                            Log.d(TAG,"Enter into the ssss Update mode TRACK MODE"+switch9);

                            if(switch9) {

                                setting_wheel_pass_switch.setChecked(true);
                                reset_wheel_limit.setEnabled(true);
                                wheel_min_limit.setEnabled(true);
                                wheel_max_limit.setEnabled(true);
                            }
                            else
                            {



                                setting_wheel_pass_switch.setChecked(false);
                                reset_wheel_limit.setEnabled(false);
                                wheel_min_limit.setEnabled(false);
                                wheel_max_limit.setEnabled(false);
                            }
                        }
                    }

                    pressureLimit_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if(isChecked) {
                                txt_pressure_limit.setEnabled(false);
                            } else {
                                txt_pressure_limit.setEnabled(true);
                            }
                            Log.v("MMMM", "3. Enter into the Update mode INJECTOR MODE ");
                            ContentValues values = new ContentValues();
                            values.put(Switch_Status_Table.PRESSURE_SWITCH, pressureLimit_switch.isChecked());
                            String where = Switch_Status_Table.MODE_NAME + " = '" + ModeTimeTable.INJECTOR_MODE + "'";
                            mCtx.getContentResolver().update(Switch_Status_Table.CONTENT_URI, values, where, null);
                            Log.v("MMMM", "4. Enter into the Update mode INJECTOR MODE  update successful ");
                        }
                    });

                    ////////////////////////////////////////

                    txt_pressure_limit.addTextChangedListener(new TextWatcher() {
                        public void afterTextChanged(Editable s) {
                            if (!s.toString().equals("")) {
                                int min_value = (parseInt(s.toString()));
                                /*if (min_value > 400) {
                                    txt_pressure_limit.setError("Enter pressure limit value as <= 400");
                                }*/
                            }
                        }

                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                        }
                    });


                    //////////
                    txt_pressure_limit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {

                            try {
                                if (hasFocus) {
                                    Log.v(TAG, "Focus changed in pressure limit switch");
                                } else {
                                    if (txt_pressure_limit.getText().toString().equals("")) {
                                        txt_pressure_limit.setText(00);
                                    }
                                    int min_value = (parseInt(txt_pressure_limit.getText().toString()));

                                   /* if (min_value > 400) {
                                        txt_pressure_limit.setText("400");
                                    }*/
                                }
                            }

                            catch (Exception e)
                            {
                                Log.d(TAG, "Pressure limit input text error");

                            }
                        }
                    });



                    save_mode.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            //if (obj != null && obj instanceof Integer) {

                            if (mBluetoothLe!=null) {

                                Log.d("Veera", "Connected" + String.valueOf(mBluetoothLe != null));

                                if (position == 0) {

                                    Log.d("Mode", "Progressive");

                                    //insert record into mode table
                                    String onHour = ontime_hours.getText().toString();
                                    String onMin = ontime_min.getText().toString();
                                    String onSec = ontime_sec.getText().toString();


                                    String offHour = offtime_hours.getText().toString();
                                    String offMin = offtime_min.getText().toString();
                                    String offSec = offtime_sec.getText().toString();

                                    if (onHour.equals("")) {
                                        onHour = "00";
                                    } else if (parseInt(String.valueOf(onHour)) > 99) {
                                        onHour = "99";
                                    }
                                    if (onMin.equals("")) {
                                        onMin = "00";
                                    } else if (parseInt(String.valueOf(onMin)) > 59) {
                                        onMin = "59";
                                    }
                                    if (onSec.equals("")) {
                                        onSec = "00";
                                    } else if (parseInt(String.valueOf(onSec)) > 59) {
                                        onSec = "59";
                                    }

                                    if (ontime_hours.getText().toString().length() < 2) {
                                        onHour = "0" + onHour;
                                    }
                                    if (ontime_min.getText().toString().length() < 2) {
                                        onMin = "0" + onMin;
                                    }
                                    if (ontime_sec.getText().toString().length() < 2) {
                                        onSec = "0" + onSec;
                                    }


                                    if (offHour.equals("")) {
                                        offHour = "00";
                                    } else if (parseInt(String.valueOf(offHour)) > 99) {
                                        offHour = "99";
                                    }

                                    if (offMin.equals("")) {
                                        offMin = "00";
                                    } else if (parseInt(String.valueOf(offMin)) > 59) {
                                        offMin = "59";
                                    }

                                    if (offSec.equals("")) {
                                        offSec = "00";
                                    } else if (parseInt(String.valueOf(offSec)) > 59) {
                                        offSec = "59";
                                    }


                                    if (offtime_hours.getText().toString().length() < 2) {
                                        offHour = "0" + offHour;
                                    }
                                    if (offtime_min.getText().toString().length() < 2) {
                                        offMin = "0" + offMin;
                                    }
                                    if (offtime_sec.getText().toString().length() < 2) {
                                        offSec = "0" + offSec;
                                    }

                                    String strTime = onHour + onMin + onSec + offHour + offMin + offSec;
                                    Log.v("MMMEE", " strOnOffTime { " + strTime);

                                    int ohon = Integer.parseInt(onHour);
                                    int omon = Integer.parseInt(onMin);
                                    int oson = Integer.parseInt(onSec);


                                    int oh = Integer.parseInt(offHour);
                                    int om = Integer.parseInt(offMin);
                                    int os = Integer.parseInt(offSec);
                                    Log.v("XXXXXXXXXXXX Off time", oh + ":" + om + ":" + os);
                                    int flag = 1;

                                    if (oh == 0) {
                                        if (om == 0) {
                                            if (os < 10) {
                                                if (omon == 0) {
                                                    if (oson < 10) {
                                                        flag = 0;
                                                        ontime_sec.setError("min 10 sec required");
                                                        offtime_sec.setError("min 10 sec required");

                                                    }

                                                if (os < 10) {
                                                    flag = 0;
                                                    offtime_sec.setError("min 10 sec required");

                                                }


                                                }
                                                else{

                                                    if (oh == 0) {
                                                        if (om == 0) {
                                                            if (os < 10) {
                                                                flag = 0;
                                                                offtime_sec.setError("min 10 sec required");

                                                            }
                                                        }
                                                    }


                                                }
                                            } else {
                                                if(omon==0) {
                                                    if (oson < 10) {
                                                        flag = 0;
                                                        ontime_sec.setError("min 10 sec required");

                                                    }
                                                }

                                            }
                                        }
                                        else {
                                            if(omon==0)
                                            {
                                            if (oson < 10)
                                            {
                                                flag = 0;
                                                ontime_sec.setError("min 10 sec required");


                                            }
                                            }

                                        }


                                    }

                                    else {

                                        flag = 1;
                                        if (omon == 0) {
                                            if (oson < 10) {
                                                    flag = 0;
                                                    ontime_sec.setError("min 10 sec required");
                                            }
                                        }




                                    }

                                    if (flag == 1) {
                                        //Insert into the table
                                        ContentValues values = new ContentValues();
                                        values.put(ModeTimeTable.MODE_NAME, ModeTimeTable.PROGRESSIVE_MODE);
                                        values.put(ModeTimeTable.TIME, strTime);
                                        //values.put(WHEEL_COUNT, "0");
                                        values.put(PRESSURE_LIMIT, "0");
                                        values.put(ModeTimeTable.STATUS, modeStatus.get(position));
                                        Cursor checkExists = holder.main_Text.getContext().getContentResolver().

                                                query(ModeTimeTable.CONTENT_URI,
                                                        null,
                                                        ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.PROGRESSIVE_MODE},
                                                        null);
                                        if (checkExists != null && checkExists.moveToFirst()) {
                                            try {
                                                holder.main_Text.getContext().getContentResolver().update(ModeTimeTable.CONTENT_URI, values, ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.PROGRESSIVE_MODE});
                                                Log.v("LC108D-LOG", "update");

                                                String strOnMin = strTime.substring(2, 4);

                                                String strOnSec = strTime.substring(4, 6);

                                                String stroffHr = strTime.substring(6, 8);

                                                String stroffMin = strTime.substring(8, 10);

                                                String stroffSec = strTime.substring(10, 12);


                                                byte[] BYTE_SEND_LOG_REQUEST = {0x41, 0x54, 0x05, 0x01, 0x01, Byte.parseByte(strOnMin), Byte.parseByte(strOnSec), Byte.parseByte(stroffHr),
                                                        Byte.parseByte(stroffMin), Byte.parseByte(stroffSec), 0x20, 0x20,
                                                        0x20, 0x20, 0x20, 0x20, 0x20,
                                                        0x20, 0x0D, 0x0A};
                                                try {
                                                    mBluetoothLe.writeCustomCharacteristic(BYTE_SEND_LOG_REQUEST);
                                                    Toast.makeText(mCtx, "Update Mode settings", Toast.LENGTH_SHORT).show();
                                                    addNoteDialog.dismiss();
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                    Toast.makeText(mCtx, "Bluetooth Connection problem", Toast.LENGTH_SHORT).show();
                                                }


                                                Log.v(TAG, "Write function 51...");
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        } else {
                                            Uri uri_insert;
                                            try {
                                                uri_insert = holder.main_Text.getContext().getContentResolver()
                                                        .insert(ModeTimeTable.CONTENT_URI, values);
                                                Log.v("LC108D-LOG", "Insert : " + uri_insert);

                                                //send mode time settings//
                                                String strOnMin = strTime.substring(2, 4);

                                                String strOnSec = strTime.substring(4, 6);

                                                String stroffHr = strTime.substring(6, 8);

                                                String stroffMin = strTime.substring(8, 10);

                                                String stroffSec = strTime.substring(10, 12);


                                                byte[] BYTE_SEND_LOG_REQUEST = {0x41, 0x54, 0x05, 0x01, 0x01, Byte.parseByte(strOnMin), Byte.parseByte(strOnSec), Byte.parseByte(stroffHr),
                                                        Byte.parseByte(stroffMin), Byte.parseByte(stroffSec), 0x20, 0x20,
                                                        0x20, 0x20, 0x20, 0x20, 0x20,
                                                        0x20, 0x0D, 0x0A};
                                                try {
                                                    mBluetoothLe.writeCustomCharacteristic(BYTE_SEND_LOG_REQUEST);
                                                    Log.v(TAG, "Write function 51... success");
                                                    Toast.makeText(mCtx, "Update Mode settings", Toast.LENGTH_SHORT).show();
                                                    addNoteDialog.dismiss();
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                    Log.v(TAG, "Write function 51... failure");
                                                    Toast.makeText(mCtx, "Bluetooth Connection problem", Toast.LENGTH_SHORT).show();
                                                }

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        if (checkExists != null) {
                                            checkExists.close();
                                        }
                                    } else {


                                        offtime_sec.setError("min 10 sec required");
                                    }

                                } else {
                                    if (position == 1) {

                                        Log.d("Mode", "TRACK_MODE");
                                        //insert record into mode table
                                        String onHour = ontime_hours.getText().toString();
                                        String onMin = ontime_min.getText().toString();
                                        String onSec = ontime_sec.getText().toString();

                                        boolean switch_setting_wheels_passed = setting_wheel_pass_switch.isChecked();


                                        String min_wheel_limit = wheel_min_limit.getText().toString();
                                        String  max_wheel_limit = wheel_max_limit.getText().toString();
                                        boolean reset_wheel_passed = reset_wheel_limit.isChecked();
                                        String total_wheels_passed_per_day = wheels_passed_count.getText().toString();

                                        wheel_max_limit.setClickable(false);
                                        wheel_min_limit.setClickable(false);



                                        //txt_wheel_count.setText("5");

                                        if (onHour.equals("")) {
                                            onHour = "00";
                                        } else if (parseInt(String.valueOf(onHour)) > 99) {
                                            onHour = "99";
                                        }
                                        if (onMin.equals("")) {
                                            onMin = "00";
                                        } else if (parseInt(String.valueOf(onMin)) > 59) {
                                            onMin = "59";
                                        }
                                        if (onSec.equals("")) {
                                            onSec = "00";
                                        } else if (parseInt(String.valueOf(onSec)) > 59) {
                                            onSec = "59";
                                        }

                                        if (ontime_hours.getText().toString().length() < 2) {
                                            onHour = "0" + onHour;
                                        }
                                        if (ontime_min.getText().toString().length() < 2) {
                                            onMin = "0" + onMin;
                                        }
                                        if (ontime_sec.getText().toString().length() < 2) {
                                            onSec = "0" + onSec;
                                        }

                                        String offHour = "00";
                                        String offMin = "00";
                                        String offSec = "00";

                                        int flag1 = 1;

                                        int ohon = Integer.parseInt(onHour);
                                        int omon = Integer.parseInt(onMin);
                                        int oson = Integer.parseInt(onSec);

                                        int oh = Integer.parseInt(offHour);
                                        int om = Integer.parseInt(offMin);
                                        int os = Integer.parseInt(offSec);
                                        Log.v("Track Mode on", ohon + ":" + omon + ":" + oson);
                                        String WHEEL_COUNT_VALUE = txt_wheel_count.getText().toString();


                                        String PRESSURE_LIMIT_VALUE;


                                        PRESSURE_LIMIT_VALUE = txt_pressure_limit.getText().toString();



                                         if(!(txt_pressure_limit.getText().toString()).equals(""))
                                          {


                                              Log.d(TAG,"Display pressure value"+PRESSURE_LIMIT_VALUE);
                                              int pressurevalue = parseInt(PRESSURE_LIMIT_VALUE);
                                              if(pressurevalue <5) {


                                                  if (!WHEEL_COUNT_VALUE.equals("")) {
                                                      int wheel = Integer.parseInt(WHEEL_COUNT_VALUE);
                                                      if (wheel < 5) {
                                                          if (ohon == 0) {
                                                              if (omon == 0) {
                                                                  if (oson < 10) {
                                                                      flag1 = 0;
                                                                      ontime_sec.setError("ontime second must be > 10");

                                                                  } else {
                                                                      flag1 = 1;
                                                                      if(pressurevalue < 5)
                                                                      {
                                                                          flag1 = 0;
                                                                          txt_pressure_limit.setError("min pressure limit is 5");

                                                                      }
                                                                  }
                                                              } else {
                                                                  flag1 = 1;
                                                              }


                                                          } else {
                                                              flag1 = 1;
                                                          }

                                                          txt_wheel_count.setError("min wheel count limit is 5");

                                                      } else {


                                                          if (ohon == 0) {
                                                              if (omon == 0) {
                                                                  if (oson < 10) {
                                                                      flag1 = 0;
                                                                      ontime_sec.setError("ontime second must be > 10");


                                                                  } else {
                                                                      flag1 = 1;
                                                                  }
                                                              } else {
                                                                  flag1 = 1;
                                                              }


                                                          } else {
                                                              flag1 = 1;
                                                          }
                                                      }

                                                      if (parseInt(String.valueOf(WHEEL_COUNT_VALUE)) > 59999) {
                                                          WHEEL_COUNT_VALUE = "59999";
                                                      }
                                                  }

                                                  flag1= 0;

                                                  txt_pressure_limit.setError("min pressure limit is 5");
                                              }


                                              else
                                              {

                                                  if (!WHEEL_COUNT_VALUE.equals("")) {
                                                      int wheel = Integer.parseInt(WHEEL_COUNT_VALUE);
                                                      if (wheel < 5) {
                                                          if (ohon == 0) {
                                                              if (omon == 0) {
                                                                  if (oson < 10) {
                                                                      flag1 = 0;
                                                                      ontime_sec.setError("ontime second must be > 10");

                                                                  } else {
                                                                      flag1 = 1;
                                                                  }
                                                              } else {
                                                                  flag1 = 1;
                                                              }


                                                          } else {
                                                              flag1 = 1;
                                                          }

                                                          txt_wheel_count.setError("min wheel count limit is 5");

                                                      } else {


                                                          if (ohon == 0) {
                                                              if (omon == 0) {
                                                                  if (oson < 10) {
                                                                      flag1 = 0;
                                                                      ontime_sec.setError("ontime second must be > 10");


                                                                  } else {
                                                                      flag1 = 1;
                                                                  }
                                                              } else {
                                                                  flag1 = 1;
                                                              }


                                                          } else {
                                                              flag1 = 1;
                                                          }
                                                      }

                                                      if (parseInt(String.valueOf(WHEEL_COUNT_VALUE)) > 59999) {
                                                          WHEEL_COUNT_VALUE = "59999";
                                                      }
                                                  }
                                              }

                                        }


                                        else
                                         {
                                             txt_pressure_limit.setError("Enter pressure value");
                                         }





                                      /*  if(parseInt(String.valueOf(min_wheel_limit)) > 65000 || parseInt(String.valueOf(min_wheel_limit))>parseInt(String.valueOf(max_wheel_limit)))
                                        {
                                            min_wheel_limit = "1";
                                        }

                                        if(parseInt(String.valueOf(max_wheel_limit))>65000)
                                        {
                                            max_wheel_limit ="10000";
                                        }
*/

                                        String strTime = onHour + onMin + onSec + offHour + offMin + offSec;


                                        //Insert into the table






                                        if(flag1==1) {

                                            Log.d(TAG, "MBMBLC108D-LOG WHEELS_PER_DAY_Min_LIMIT" + min_wheel_limit);

                                            Log.d(TAG, "MBMBLC108D-LOG WHEELS_PER_DAY_Max_LIMIT" + max_wheel_limit);

                                            Log.d(TAG, "MBMBLC108D-LOG WHEEL_COUNT_VALUE" + WHEEL_COUNT_VALUE);

                                            Log.d(TAG, "MBMBLC108D-LOG PRESSURE_VALUE" + PRESSURE_LIMIT_VALUE);


                                            if (!WHEEL_COUNT_VALUE.equals("")) {
                                                if ((Integer.parseInt(WHEEL_COUNT_VALUE)) >= 5 && (Integer.parseInt(WHEEL_COUNT_VALUE)) <=59999) {

                                                    if (!max_wheel_limit.equals("") && !min_wheel_limit.equals("")) {

                                                        if ((Integer.parseInt(min_wheel_limit)) >= 1 && (Integer.parseInt(max_wheel_limit)) >=5) {

                                                            if ((Integer.parseInt(min_wheel_limit)) < (Integer.parseInt(max_wheel_limit))) {


                                                                ContentValues values = new ContentValues();
                                                                values.put(ModeTimeTable.MODE_NAME, ModeTimeTable.TRACK_MODE);
                                                                values.put(ModeTimeTable.TIME, strTime);
                                                                values.put(WHEEL_COUNT, WHEEL_COUNT_VALUE);
                                                                values.put(PRESSURE_LIMIT, PRESSURE_LIMIT_VALUE);
                                                                values.put(WHEELS_PER_DAY_MAX_LIMIT, max_wheel_limit);
                                                                values.put(WHEELS_PER_DAY_MIN_LIMIT, min_wheel_limit);
                                                                values.put(RESET_WHEELS_PASSED, reset_wheel_limit.isChecked());
                                                                values.put(TOTAL_WHEELS_PASSED_PER_DAY, total_wheels_passed_per_day);
                                                                values.put(SETTING_PASSED_PER_DAY, setting_wheel_pass_switch.isChecked());

                                                                Log.d(TAG, "WHEELS_PER_DAY_MAX_LIMIT" + max_wheel_limit);
                                                                Log.d(TAG, "WHEELS_PER_DAY_MIN_LIMIT" + min_wheel_limit);
                                                                Log.d(TAG, "WHEELS_PASSED" + reset_wheel_limit.isChecked());
                                                                Log.d(TAG, "TOTAL WHEELS_PASSED" + total_wheels_passed_per_day);




                                                                Cursor getStatusnew = holder.main_Text.getContext().getContentResolver().query(Switch_Status_Table.CONTENT_URI,
                                                                        null,
                                                                        Switch_Status_Table.MODE_NAME + "=?", new String[]{"TRACK MODE"},
                                                                        null);

                                                                if (getStatusnew != null && getStatusnew.moveToFirst()) {

                                                                    String name = getStatusnew.getString(getStatusnew.getColumnIndex(Switch_Status_Table.MODE_NAME));

                                                                    if (name.equals("TRACK MODE")) {



                                                                        ContentValues valuesswitch = new ContentValues();

                                                                        valuesswitch.put(Switch_Status_Table.DALIY_WHEEL_REPORT, setting_wheel_pass_switch.isChecked());

                                                                        String where = Switch_Status_Table.MODE_NAME + " = '" + "TRACK MODE" + "'";

                                                                        holder.main_Text.getContext().getContentResolver().update(Switch_Status_Table.CONTENT_URI, valuesswitch, where, null);

                                                                    }
                                                                }



                                                                values.put(ModeTimeTable.STATUS, modeStatus.get(position));
                                                                Cursor checkExists = holder.main_Text.getContext().getContentResolver().
                                                                        query(ModeTimeTable.CONTENT_URI,
                                                                                null,
                                                                                ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.TRACK_MODE},
                                                                                null);
                                                                if (checkExists != null && checkExists.moveToFirst()) {
                                                                    try {
                                                                        holder.main_Text.getContext().getContentResolver().update(ModeTimeTable.CONTENT_URI, values, ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.TRACK_MODE});
                                                                        Log.v("MBMBLC108D-LOG", "update1");

                                                                        // send mode configuration to device
                                                                        String strOnMin = strTime.substring(2, 4);

                                                                        String strOnSec = strTime.substring(4, 6);
                                                                        Log.v("MBMBLC108D-LOG", "update2");

                                                                        //int value_test = 99999;
                                                                        int msb;
                                                                        int lsb;
                                                                        int value = parseInt(WHEEL_COUNT_VALUE);
                                                                        Log.v("MBMBLC108D-LOG", "position==22 value3 : " + value);
                                                                        String Hex_PressureCount = Integer.toHexString(value);
                                                                        Log.v("MBMBLC108D-LOG", "position==22 Hex_PressureCount4 : " + Hex_PressureCount);
                                                                        try {
                                                                            int firstValue = Integer.parseInt(Hex_PressureCount, 16);
                                                                            //  int firstValue = Integer.parseInt(String.valueOf(Hex_PressureCount));
                                                                            //Integer.parseInt("-FF", 16) ; // returns -255
                                                                            Log.v("MBMBLC108D-LOG", "position==22 firstValue5 : " + firstValue);
                                                                            msb = parseInt(String.valueOf(firstValue >> 8));
                                                                            Log.v("MBMBLC108D-LOG", "position==22 msb : " + msb);
                                                                            lsb = Integer.parseInt(String.valueOf(firstValue & 0xFF));
                                                                            Log.v("MBMBLC108D-LOG", "position==22 lsb6 : " + lsb);
                                                                        } catch (NumberFormatException e) {
                                                                            int firstValue = Integer.parseInt(Hex_PressureCount, 16);
                                                                            //  int firstValue = Integer.parseInt(String.valueOf(Hex_PressureCount));
                                                                            //Integer.parseInt("-FF", 16) ; // returns -255
                                                                            Log.v("MBMBLC108D-LOG", "position==22 firstValue5 : " + firstValue);
                                                                            Log.v("MBMBLC108D-LOG", "NumberFormatException position==22 firstValue5 : " + firstValue);
                                                                            msb = parseInt(String.valueOf(firstValue >> 8));
                                                                            Log.v("MBMBLC108D-LOG", "NumberFormatException position==22 msb : " + msb);
                                                                            lsb = Integer.parseInt(String.valueOf(firstValue & 0xFF));
                                                                            Log.v("MBMBLC108D-LOG", "NumberFormatException position==22 lsb6 : " + lsb);
                                                                        }


                                                                        /////////////////////////////////////////////////////


                                                                        int msbminwheel;
                                                                        int lsbminwheel;
                                                                        int valueminwheel = parseInt(min_wheel_limit);
                                                                        Log.v("MBMBLC108D-LOG", "position==22 value3 min : " + valueminwheel);
                                                                        String Hex_minwheel = Integer.toHexString(valueminwheel);
                                                                        Log.v("MBMBLC108D-LOG", "position==22 Hex_PressureCount4 : min" + Hex_minwheel);
                                                                        try {
                                                                            int firstValue = Integer.parseInt(Hex_minwheel, 16);
                                                                            //  int firstValue = Integer.parseInt(String.valueOf(Hex_PressureCount));
                                                                            //Integer.parseInt("-FF", 16) ; // returns -255
                                                                            Log.v("MBMBLC108D-LOG", "position==22 firstValue5 min: " + firstValue);
                                                                            msbminwheel = parseInt(String.valueOf(firstValue >> 8));
                                                                            Log.v("MBMBLC108D-LOG", "position==22 msb : " + msbminwheel);
                                                                            lsbminwheel = Integer.parseInt(String.valueOf(firstValue & 0xFF));
                                                                            Log.v("MBMBLC108D-LOG", "position==22 lsb6 : " + lsbminwheel);
                                                                        } catch (NumberFormatException e) {
                                                                            int firstValue = Integer.parseInt(Hex_minwheel, 16);
                                                                            //  int firstValue = Integer.parseInt(String.valueOf(Hex_PressureCount));
                                                                            //Integer.parseInt("-FF", 16) ; // returns -255
                                                                            Log.v("MBMBLC108D-LOG", "position==22 firstValue5 : " + firstValue);
                                                                            Log.v("MBMBLC108D-LOG", "NumberFormatException position==22 firstValue5 : " + firstValue);
                                                                            msbminwheel = parseInt(String.valueOf(firstValue >> 8));
                                                                            Log.v("MBMBLC108D-LOG", "NumberFormatException position==22 msb : " + msbminwheel);
                                                                            lsbminwheel = Integer.parseInt(String.valueOf(firstValue & 0xFF));
                                                                            Log.v("MBMBLC108D-LOG", "NumberFormatException position==22 lsb6 : " + lsbminwheel);
                                                                        }


                                                                        //////////////////////////////////////////////////


                                                                        int msbmaxwheel;
                                                                        int lsbmaxwheel;
                                                                        int valuemaxwheel = parseInt(max_wheel_limit);
                                                                        Log.v("MBMBLC108D-LOG", "position==22 value3 : " + valuemaxwheel);
                                                                        String Hex_maxwheel = Integer.toHexString(valuemaxwheel);
                                                                        Log.v("MBMBLC108D-LOG", "position==22 Hex_PressureCount4 : " + Hex_maxwheel);
                                                                        try {
                                                                            int firstValue = Integer.parseInt(Hex_maxwheel, 16);
                                                                            //  int firstValue = Integer.parseInt(String.valueOf(Hex_PressureCount));
                                                                            //Integer.parseInt("-FF", 16) ; // returns -255
                                                                            Log.v("MBMBLC108D-LOG", "position==22 firstValue5 : " + firstValue);
                                                                            msbmaxwheel = parseInt(String.valueOf(firstValue >> 8));
                                                                            Log.v("MBMBLC108D-LOG", "position==22 msb : " + msbmaxwheel);
                                                                            lsbmaxwheel = Integer.parseInt(String.valueOf(firstValue & 0xFF));
                                                                            Log.v("MBMBLC108D-LOG", "position==22 lsb6 : " + lsbmaxwheel);
                                                                        } catch (NumberFormatException e) {
                                                                            int firstValue = Integer.parseInt(Hex_maxwheel, 16);
                                                                            //  int firstValue = Integer.parseInt(String.valueOf(Hex_PressureCount));
                                                                            //Integer.parseInt("-FF", 16) ; // returns -255
                                                                            Log.v("MBMBLC108D-LOG", "position==22 firstValue5 : " + firstValue);
                                                                            Log.v("MBMBLC108D-LOG", "NumberFormatException position==22 firstValue5 : " + firstValue);
                                                                            msbmaxwheel = parseInt(String.valueOf(firstValue >> 8));
                                                                            Log.v("MBMBLC108D-LOG", "NumberFormatException position==22 msb : " + msbmaxwheel);
                                                                            lsbmaxwheel = Integer.parseInt(String.valueOf(firstValue & 0xFF));
                                                                            Log.v("MBMBLC108D-LOG", "NumberFormatException position==22 lsb6 : " + lsbmaxwheel);
                                                                        }

                                                                        byte reset_wheel_byte = 0x00;

                                                                        if (reset_wheel_limit.isChecked()) {
                                                                            reset_wheel_byte = 0x01;
                                                                        } else {
                                                                            reset_wheel_byte = 0x00;
                                                                        }


                                                                        ////////////////


                                                                        int pressurevalue = parseInt(PRESSURE_LIMIT_VALUE);

                                                                        Log.v("MMMMLC108D-LOG", "position==22 value : " + pressurevalue);
                                                                        String Hex_PressureCounttrack = Integer.toHexString(pressurevalue);
                                                                        Log.v("MMMMLC108D-LOG", "position==22 Hex_PressureCount : " + Hex_PressureCounttrack);
                                                                        int firstValuepressure = Integer.parseInt(Hex_PressureCounttrack, 16);
                                                                        //  int firstValue = Integer.parseInt(String.valueOf(Hex_PressureCount));
                                                                        //Integer.parseInt("-FF", 16) ; // returns -255
                                                                        Log.v("MBMBLC108D-LOG", "position==22 firstValue5 : " + firstValuepressure);
                                                                        int pressuremsb = Integer.parseInt(String.valueOf(firstValuepressure >> 8));
                                                                        Log.v("MMMMLC108D-LOG", "position==22 msb : " + pressuremsb);
                                                                        int pressurelsb = Integer.parseInt(String.valueOf(firstValuepressure & 0xFF));
                                                                        Log.v("MMMMLC108D-LOG", "position==22 lsb : " + pressurelsb);


                                                                        if ((parseInt(String.valueOf(min_wheel_limit)) > 65000) || (parseInt(String.valueOf(min_wheel_limit)) >= parseInt(String.valueOf(max_wheel_limit)))
                                                                                || (parseInt(String.valueOf(max_wheel_limit)) > 65000)) {
                                                                            //  min_wheel_limit = "1";

                                                                            Log.v("MBMBLC108D-LOG", " wheel passed min value if");


                                                                            if (parseInt(String.valueOf(min_wheel_limit)) > 65000 || parseInt(String.valueOf(min_wheel_limit)) >= parseInt(String.valueOf(max_wheel_limit))) {


                                                                                wheel_min_limit.setError("minimum  limit error");
                                                                            }

                                                                            if (parseInt(String.valueOf(max_wheel_limit)) > 65000) {
                                                                                wheel_max_limit.setError("maximum  limit error");


                                                                            }


                                                                        } else {
                                                                            Log.v("MBMBLC108D-LOG", " wheel passed min value else ");

                                                                            Log.v("MBMBLC108D-LOG", "Min value" + parseInt(String.valueOf(min_wheel_limit)));

                                                                            Log.v("MBMBLC108D-LOG", "MMAx value" + parseInt(String.valueOf(max_wheel_limit)));

                                                                            if (switch_setting_wheels_passed) {


                                                                                byte[] BYTE_SEND_LOG_REQUEST = {0x41, 0x54, 0x05, 0x01, 0x02, Byte.parseByte(strOnMin), Byte.parseByte(strOnSec),
                                                                                        (byte) msb, (byte) lsb, 0x01, (byte) msbmaxwheel, (byte) lsbmaxwheel,
                                                                                        (byte) msbminwheel, (byte) lsbminwheel, reset_wheel_byte, 0x01, (byte) pressuremsb,
                                                                                        (byte) pressurelsb, 0x0D, 0x0A};


                                                                                StringBuilder sb1 = new StringBuilder();
                                                                                for (byte bb : BYTE_SEND_LOG_REQUEST) {
                                                                                    if (sb1.length() > 0) {
                                                                                        sb1.append(':');
                                                                                    }
                                                                                    sb1.append(String.format("%02x", bb));
                                                                                }

                                                                                Log.v("MBMBLC108D-LOG", "update8 wheel passed" + String.valueOf(sb1));

                                                                                try {
                                                                                    Log.v("MBMBLC108D-LOG", "update9");
                                                                                    mBluetoothLe.writeCustomCharacteristic(BYTE_SEND_LOG_REQUEST);
                                                                                    Log.v(TAG, "Write function 51...");
                                                                                    Toast.makeText(mCtx, "Update Mode settings", Toast.LENGTH_SHORT).show();
                                                                                    addNoteDialog.dismiss();
                                                                                } catch (Exception e) {
                                                                                    e.printStackTrace();
                                                                                    Toast.makeText(mCtx, "Bluetooth Connection problem", Toast.LENGTH_SHORT).show();
                                                                                }

                                                                            } else

                                                                            {
                                                                                byte[] BYTE_SEND_LOG_REQUEST = {0x41, 0x54, 0x05, 0x01, 0x02, Byte.parseByte(strOnMin), Byte.parseByte(strOnSec),
                                                                                        (byte) msb, (byte) lsb, 0x01, (byte) msbmaxwheel, (byte) lsbmaxwheel,
                                                                                        (byte) msbminwheel, (byte) lsbminwheel, reset_wheel_byte, 0x00, (byte) pressuremsb,
                                                                                        (byte) pressurelsb, 0x0D, 0x0A};


                                                                                StringBuilder sb1 = new StringBuilder();
                                                                                for (byte bb : BYTE_SEND_LOG_REQUEST) {
                                                                                    if (sb1.length() > 0) {
                                                                                        sb1.append(':');
                                                                                    }
                                                                                    sb1.append(String.format("%02x", bb));
                                                                                }

                                                                                Log.v("MBMBLC108D-LOG", "update8 wheel passed" + String.valueOf(sb1));

                                                                                try {
                                                                                    Log.v("MBMBLC108D-LOG", "update9");
                                                                                    mBluetoothLe.writeCustomCharacteristic(BYTE_SEND_LOG_REQUEST);
                                                                                    Log.v(TAG, "Write function 51...");
                                                                                    Toast.makeText(mCtx, "Update Mode settings", Toast.LENGTH_SHORT).show();
                                                                                    addNoteDialog.dismiss();
                                                                                } catch (Exception e) {
                                                                                    e.printStackTrace();
                                                                                    Toast.makeText(mCtx, "Bluetooth Connection problem", Toast.LENGTH_SHORT).show();
                                                                                }

                                                                            }


                                                                        }


                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                } else {
                                                                    Uri uri_insert;
                                                                    try {
                                                                        uri_insert = holder.main_Text.getContext().getContentResolver()
                                                                                .insert(ModeTimeTable.CONTENT_URI, values);
                                                                        Log.v("LC108D-LOG", "Insert : " + uri_insert);

                                                                        // send mode configuration to device
                                                                        String strOnMin = strTime.substring(2, 4);
                                                                        String strOnSec = strTime.substring(4, 6);

                                                                        //int value_test = 99999;
                                                                        int value = parseInt(WHEEL_COUNT_VALUE);
                                                                        Log.v("MMMMLC108D-LOG", "position==22 value : " + value);
                                                                        String Hex_PressureCount = Integer.toHexString(value);
                                                                        Log.v("MMMMLC108D-LOG", "position==22 Hex_PressureCount : " + Hex_PressureCount);
                                                                        int firstValue = Integer.parseInt(Hex_PressureCount, 16);
                                                                        //  int firstValue = Integer.parseInt(String.valueOf(Hex_PressureCount));
                                                                        //Integer.parseInt("-FF", 16) ; // returns -255
                                                                        Log.v("MBMBLC108D-LOG", "position==22 firstValue5 : " + firstValue);
                                                                        int msb = Integer.parseInt(String.valueOf(firstValue >> 8));
                                                                        Log.v("MMMMLC108D-LOG", "position==22 msb : " + msb);
                                                                        int lsb = Integer.parseInt(String.valueOf(firstValue & 0xFF));
                                                                        Log.v("MMMMLC108D-LOG", "position==22 lsb : " + lsb);

                                                                        byte[] BYTE_SEND_LOG_REQUEST = {0x41, 0x54, 0x05, 0x01, 0x02, Byte.parseByte(strOnMin), Byte.parseByte(strOnSec),
                                                                                (byte) msb, (byte) lsb, 0x01, 0x20, 0x20,
                                                                                0x20, 0x20, 0x20, 0x20, 0x20,
                                                                                0x20, 0x0D, 0x0A};
                                                                        try {
                                                                            mBluetoothLe.writeCustomCharacteristic(BYTE_SEND_LOG_REQUEST);
                                                                            Log.v(TAG, "Write function 51...");
                                                                            Toast.makeText(mCtx, "Update Mode settings", Toast.LENGTH_SHORT).show();
                                                                            addNoteDialog.dismiss();
                                                                        } catch (Exception e) {
                                                                            e.printStackTrace();
                                                                            Toast.makeText(mCtx, "Bluetooth Connection problem", Toast.LENGTH_SHORT).show();
                                                                        }
                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }


                                                                if (checkExists != null) {
                                                                    checkExists.close();
                                                                }
                                                            } else {
                                                                if ((Integer.parseInt(min_wheel_limit) == Integer.parseInt(max_wheel_limit))) {
                                                                    wheel_min_limit.setError("Min limit < Max limit");

                                                                }
                                                            }
                                                        } else {
                                                            if (Integer.parseInt(min_wheel_limit) < 1) {
                                                                wheel_min_limit.setError("Min limit > 1");
                                                            }
                                                            if (Integer.parseInt(max_wheel_limit) < 5) {
                                                                wheel_max_limit.setError("Max limit > 5");

                                                            }

                                                        }
                                                    } else {
                                                        Log.v(TAG, "MBMBLC108D-LOG empty check");

                                                        if (min_wheel_limit.equals("")) {
                                                            wheel_min_limit.setError("Enter value");
                                                        }
                                                        if (max_wheel_limit.equals(""))
                                                            wheel_max_limit.setError("Enter value");

                                                        if(Integer.parseInt(WHEEL_COUNT_VALUE)<5)
                                                        {
                                                            txt_wheel_count.setError("wheel count limit is 5");

                                                        }

                                                    }
                                                } else {

                                                    txt_wheel_count.setError("minimum wheel count limit is 5");
                                                }

                                            } else {
                                                if (WHEEL_COUNT_VALUE.equals(""))
                                                    txt_wheel_count.setError("Enter value");
                                            }


                                        }




                                    } else {
                                        if (position == 2) {

//                                            offtime_sec.setText("10");
//                                            txt_pressure_limit.setText("5");

                                            Log.v("MMMMLC108D-LOG", "position==2");
                                            Log.d("Mode", "INJECTOR_MODE");
                                            //insert record into mode table
                                            String onHour = ontime_hours.getText().toString();
                                            String onMin = ontime_min.getText().toString();
                                            String onSec = ontime_sec.getText().toString();

                                            if (onHour.equals("")) {
                                                onHour = "00";
                                            } else if (parseInt(String.valueOf(onHour)) > 99) {
                                                onHour = "99";
                                            }
                                            if (onMin.equals("")) {
                                                onMin = "00";
                                            } else if (parseInt(String.valueOf(onMin)) > 59) {
                                                onMin = "59";
                                            }
                                            if (onSec.equals("")) {
                                                onSec = "00";
                                            } else if (parseInt(String.valueOf(onSec)) > 59) {
                                                onSec = "59";
                                            }
                                            if (ontime_hours.getText().toString().length() < 2) {
                                                onHour = "0" + onHour;
                                            }
                                            if (ontime_min.getText().toString().length() < 2) {
                                                onMin = "0" + onMin;
                                            }
                                            if (ontime_sec.getText().toString().length() < 2) {
                                                onSec = "0" + onSec;
                                            }

                                            String offHour = offtime_hours.getText().toString();
                                            String offMin = offtime_min.getText().toString();
                                            String offSec = offtime_sec.getText().toString();

                                            if (offHour.equals("")) {
                                                offHour = "00";
                                            } else if (parseInt(String.valueOf(offHour)) > 99) {
                                                offHour = "99";
                                            }
                                            if (offMin.equals("")) {
                                                offMin = "00";
                                            } else if (parseInt(String.valueOf(offMin)) > 59) {
                                                offMin = "59";
                                            }
                                            if (offSec.equals("")) {
                                                offSec = "00";
                                            } else if (parseInt(String.valueOf(offSec)) > 59) {
                                                offSec = "59";
                                            }

                                            if (offtime_hours.getText().toString().length() < 2) {
                                                offHour = "0" + offHour;
                                            }
                                            if (offtime_min.getText().toString().length() < 2) {
                                                offMin = "0" + offMin;
                                            }
                                            if (offtime_sec.getText().toString().length() < 2) {
                                                offSec = "0" + offSec;
                                            }


                                            int ohon = Integer.parseInt(onHour);
                                            int omon = Integer.parseInt(onMin);
                                            int oson = Integer.parseInt(onSec);

                                            int oh = Integer.parseInt(offHour);
                                            int om = Integer.parseInt(offMin);
                                            int os = Integer.parseInt(offSec);
                                            Log.v("XXXXXXXXXXXX INJECTOR_MODE Off time", oh + ":" + om + ":" + os);
                                            int flag = 1;
                                            int flag1 = 1;

                                            if (oh == 0) {
                                                if (om == 0) {
                                                    if (os < 10) {
                                                        flag = 0;
                                                    } else {
                                                        flag = 1;
                                                    }
                                                }
                                            }


                                            Log.v("MMMMLC108D-LOG", "position==21");
                                            String PRESSURE_LIMIT_VALUE = "";


                                            if (pressureLimit_switch.isChecked()) {
                                                PRESSURE_LIMIT_VALUE = "0";


                                                //////////////////////////



/*

                                                if (oh == 0) {
                                                    if (om == 0) {
                                                        if (os < 10) {
                                                            flag = 0;
                                                            if(oson <10) {
                                                                flag = 0;
                                                                ontime_sec.setError("min 10 sec required");
                                                                offtime_sec.setError("min 10 sec required");


                                                            }
                                                        } else {
                                                            if(oson<10) {
                                                                flag = 0;
                                                                ontime_sec.setError("min 10 sec required");

                                                            }
                                                            else
                                                                flag = 1;
                                                        }
                                                    }
                                                    else {
                                                        if (oson < 10)
                                                        {
                                                            flag = 0;
                                                            ontime_sec.setError("min 10 sec required");


                                                        }
                                                        else
                                                            flag = 1;
                                                    }


                                                }

                                                else
                                                {
                                                    if(oson<10) {
                                                        flag = 0;
                                                        ontime_sec.setError("min 10 sec required");

                                                    }
                                                    else
                                                        flag = 1;
                                                }


*/






                                                if (oh == 0) {
                                                    if (om == 0) {
                                                        if (os < 10) {
                                                            if (omon == 0) {
                                                                if (oson < 10) {
                                                                    flag = 0;
                                                                    ontime_sec.setError("min 10 sec required");
                                                                    offtime_sec.setError("min 10 sec required");

                                                                }

                                                                if (os < 10) {
                                                                    flag = 0;
                                                                    offtime_sec.setError("min 10 sec required");

                                                                }


                                                            }
                                                            else{

                                                                if (oh == 0) {
                                                                    if (om == 0) {
                                                                        if (os < 10) {
                                                                            flag = 0;
                                                                            offtime_sec.setError("min 10 sec required");

                                                                        }
                                                                    }
                                                                }


                                                            }
                                                        } else {
                                                            if(omon==0) {
                                                                if (oson < 10) {
                                                                    flag = 0;
                                                                    ontime_sec.setError("min 10 sec required");

                                                                }
                                                            }

                                                        }
                                                    }
                                                    else {
                                                        if(omon==0)
                                                        {
                                                            if (oson < 10)
                                                            {
                                                                flag = 0;
                                                                ontime_sec.setError("min 10 sec required");


                                                            }
                                                        }

                                                    }


                                                }

                                                else {

                                                    flag = 1;
                                                    if (omon == 0) {
                                                        if (oson < 10) {
                                                            flag = 0;
                                                            ontime_sec.setError("min 10 sec required");
                                                        }
                                                    }




                                                }





                                            } else {

                                                if(!(txt_pressure_limit.getText().toString()).equals("")) {

                                                    PRESSURE_LIMIT_VALUE = txt_pressure_limit.getText().toString();
                                                    int presvalue = Integer.parseInt(PRESSURE_LIMIT_VALUE);

                                                    Log.d(TAG, "Pressure value" + presvalue);
                                                /*if (presvalue < 5) {
                                                    flag1 = 0;
                                                } else {
                                                    flag1 = 1;
                                                }*/


                                                    if (presvalue < 5) {

                                                       if (oh == 0) {
                                                            if (om == 0) {
                                                                if (os < 10) {
                                                                    if (ohon == 0) {
                                                                        if (omon == 0) {
                                                                            if (oson < 10) {

                                                                                    flag1 = 0;
                                                                                    ontime_sec.setError("ontime second must be > 10");
                                                                                    offtime_sec.setError("offtime second must be > 10");
                                                                                    txt_pressure_limit.setError("min pressure limit is 5");


                                                                                Log.d(TAG, "Enter loop 1");



                                                                            } else {


                                                                                flag1 = 1;
                                                                                Log.d(TAG,"Enter loop 2");


                                                                                if (presvalue < 5) {
                                                                                    flag1 = 0;
                                                                                    Log.d(TAG,"Enter loop 3");

                                                                                    txt_pressure_limit.setError("min pressure limit is 5");

                                                                                }
                                                                            }
                                                                        } else {
                                                                            flag1 = 1;
                                                                        }


                                                                    } else {
                                                                        flag1 = 1;
                                                                        if (presvalue < 5) {
                                                                            flag1 = 0;
                                                                            Log.d(TAG,"Enter loop 4");

                                                                            txt_pressure_limit.setError("min pressure limit is 5");

                                                                        }
                                                                        if (os < 10) {
                                                                            flag1 = 0;
                                                                            offtime_sec.setError("offtime must be >10");
                                                                        }
                                                                    }

                                                                    if (os < 10) {
                                                                        flag1 = 0;
                                                                        offtime_sec.setError("offtime must be >10");
                                                                    }


                                                                } else {


                                                                    if (ohon == 0) {
                                                                        if (omon == 0) {
                                                                            if (oson < 10) {
                                                                                flag1 = 0;
                                                                                ontime_sec.setError("ontime second must be > 10");


                                                                            } else {
                                                                                flag1 = 1;


                                                                                if (presvalue < 5) {
                                                                                    flag1 = 0;
                                                                                    Log.d(TAG,"Enter loop 5");

                                                                                    txt_pressure_limit.setError("min pressure limit is 5");

                                                                                }
                                                                            }
                                                                        } else {

                                                                            flag1 = 1;

                                                                            if (presvalue < 5) {
                                                                                flag1 = 0;
                                                                                Log.d(TAG,"Enter loop 6");

                                                                                txt_pressure_limit.setError("min pressure limit is 5");

                                                                            }
                                                                        }


                                                                    } else {

                                                                        flag1 = 1;

                                                                        if (presvalue < 5) {
                                                                            flag1 = 0;
                                                                            Log.d(TAG,"Enter loop 7");

                                                                            txt_pressure_limit.setError("min pressure limit is 5");

                                                                        }
                                                                    }
                                                                }

                                                            }
                                                           flag1 = 1;

                                                           if (presvalue < 5) {
                                                               flag1 = 0;
                                                               Log.d(TAG,"Enter loop 7");

                                                               txt_pressure_limit.setError("min pressure limit is 5");

                                                           }

                                                        }else {
                                                                flag1 = 0;
                                                                txt_pressure_limit.setError("Enter Pressure value");


                                                        }
                                                    }



                                                    else
                                                    {
                                                        if (oh == 0) {
                                                            if (om == 0) {
                                                                if (os < 10) {
                                                                    if (ohon == 0) {
                                                                        if (omon == 0) {
                                                                            if (oson < 10) {

                                                                                    flag1 = 0;
                                                                                ontime_sec.setError("ontime second must be > 10");
                                                                                offtime_sec.setError("offtime second must be > 10");


                                                                            } else {


                                                                                flag1 = 1;

                                                                                if (presvalue < 5) {
                                                                                    flag1 = 0;
                                                                                    Log.d(TAG,"Enter loop 8");

                                                                                    txt_pressure_limit.setError("min pressure limit is 5");

                                                                                }
                                                                            }
                                                                        } else {
                                                                            flag1 = 1;
                                                                        }


                                                                    } else {
                                                                        flag1 = 1;
                                                                        if (presvalue < 5) {
                                                                            flag1 = 0;
                                                                            Log.d(TAG,"Enter loop 9");

                                                                            txt_pressure_limit.setError("min pressure limit is 5");

                                                                        }
                                                                        if (os < 10) {
                                                                            flag1 = 0;
                                                                            offtime_sec.setError("offtime must be >10");
                                                                        }
                                                                    }

                                                                    if (os < 10) {
                                                                        flag1 = 0;
                                                                        offtime_sec.setError("offtime must be >10");
                                                                    }


                                                                } else {


                                                                    if (ohon == 0) {
                                                                        if (omon == 0) {
                                                                            if (oson < 10) {
                                                                                flag1 = 0;
                                                                                ontime_sec.setError("ontime second must be > 10");


                                                                            } else {
                                                                                flag1 = 1;


                                                                                if (presvalue < 5) {
                                                                                    flag1 = 0;
                                                                                    Log.d(TAG,"Enter loop 10");

                                                                                    txt_pressure_limit.setError("min pressure limit is 5");

                                                                                }
                                                                            }
                                                                        } else {

                                                                            flag1 = 1;

                                                                            if (presvalue < 5) {
                                                                                flag1 = 0;
                                                                                Log.d(TAG,"Enter loop 11");

                                                                                txt_pressure_limit.setError("min pressure limit is 5");

                                                                            }
                                                                        }


                                                                    } else {

                                                                        flag1 = 1;

                                                                        if (presvalue < 5) {
                                                                            flag1 = 0;
                                                                            Log.d(TAG,"Enter loop 12");

                                                                            txt_pressure_limit.setError("min pressure limit is 5");

                                                                        }
                                                                    }
                                                                }

                                                            }
                                                        }else {
                                                            flag1 = 1;
                                                            Log.d(TAG,"Enter loop 13");
                                                            if(presvalue<5) {
                                                                flag1 = 0;
                                                                txt_pressure_limit.setError("Enter Pressure value");
                                                            }


                                                        }

                                                    }




                                                }

                                                else
                                                {
                                                    flag1 = 0;
                                                    txt_pressure_limit.setError("Enter Pressure value");

                                                }

                                           }

                                            String strTime = onHour + onMin + onSec + offHour + offMin + offSec + pressureLimit_switch.isEnabled();

                                            Log.d("Veera", "rajj" + strTime);
                                            Log.d("MMMM", "Enter loop flag val " + flag+ "val flag1"+flag1);


                                            if (flag == 1 && flag1 == 1) {
                                                //Insert into the table


                                                /*Cursor getStatus = mCtx.getContentResolver().query(Switch_Status_Table.CONTENT_URI,
                                                        null,
                                                        Switch_Status_Table.MODE_NAME + "=?", new String[]{ModeTimeTable.INJECTOR_MODE},
                                                        null);

                                                if (getStatus != null && getStatus.moveToFirst()) {
                                                    Log.v("MMMM", "Enter into the ssss Update mode INJECTOR MODE");
                                                    String name = getStatus.getString(getStatus.getColumnIndex(Switch_Status_Table.MODE_NAME));
                                                    boolean pressure = getStatus.getInt(getStatus.getColumnIndex(Switch_Status_Table.PRESSURE_SWITCH)) > 0;
                                                    pressureLimit_switch.setChecked(pressure);

                                                }*/

                                                ContentValues values1 = new ContentValues();
                                                values1.put(Switch_Status_Table.LIMIT_SWITCH9, pressureLimit_switch.isChecked());
                                                String where = Switch_Status_Table.MODE_NAME + " = '" + ModeTimeTable.INJECTOR_MODE + "'";
                                                mCtx.getContentResolver().update(Switch_Status_Table.CONTENT_URI, values1, where, null);
                                                Log.v("MMMM", "4. Enter into the ssss Update mode INJECTOR MODE  update successful "+pressureLimit_switch.isChecked());


                                                ContentValues values = new ContentValues();
                                                values.put(ModeTimeTable.MODE_NAME, ModeTimeTable.INJECTOR_MODE);
                                                values.put(ModeTimeTable.TIME, strTime);
                                                //values.put(WHEEL_COUNT, "0");
                                                values.put(PRESSURE_LIMIT, PRESSURE_LIMIT_VALUE);
                                                values.put(ModeTimeTable.STATUS, modeStatus.get(position));
                                                Cursor checkExists = holder.main_Text.getContext().getContentResolver().
                                                        query(ModeTimeTable.CONTENT_URI,
                                                                null,
                                                                ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.INJECTOR_MODE},
                                                                null);
                                                if (checkExists != null && checkExists.moveToFirst()) {
                                                    try {
                                                        holder.main_Text.getContext().getContentResolver().update(ModeTimeTable.CONTENT_URI, values, ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.INJECTOR_MODE});
                                                        Log.v("LC108D-LOG", "update");

                                                        String strOnMin = strTime.substring(2, 4);

                                                        String strOnSec = strTime.substring(4, 6);

                                                        String stroffHr = strTime.substring(6, 8);

                                                        String stroffMin = strTime.substring(8, 10);

                                                        String stroffSec = strTime.substring(10, 12);

                                                        Log.v("MMMMLC108D-LOG", "position==22");
                                                        //int value_test = 99999;

                                                        //int value_test = 99999;
                                                        int value = parseInt(PRESSURE_LIMIT_VALUE);

                                                        Log.v("MMMMLC108D-LOG", "position==22 value : " + value);
                                                        String Hex_PressureCount = Integer.toHexString(value);
                                                        Log.v("MMMMLC108D-LOG", "position==22 Hex_PressureCount : " + Hex_PressureCount);
                                                        int firstValue = Integer.parseInt(Hex_PressureCount, 16);
                                                        //  int firstValue = Integer.parseInt(String.valueOf(Hex_PressureCount));
                                                        //Integer.parseInt("-FF", 16) ; // returns -255
                                                        Log.v("MBMBLC108D-LOG", "position==22 firstValue5 : " + firstValue);
                                                        int msb = Integer.parseInt(String.valueOf(firstValue >> 8));
                                                        Log.v("MMMMLC108D-LOG", "position==22 msb : " + msb);
                                                        int lsb = Integer.parseInt(String.valueOf(firstValue & 0xFF));
                                                        Log.v("MMMMLC108D-LOG", "position==22 lsb : " + lsb);
//
//                                              String Hex_WheelCount = Integer.toHexString(Integer.parseInt(PRESSURE_LIMIT_VALUE));
//                                              int firstval = Integer.parseInt(Hex_WheelCount);
//                                              int b = Integer.parseInt(String.valueOf(firstval >> 8));
//                                              int bb = Integer.parseInt(String.valueOf(firstval & 0xFF));
                                                        Log.v("MMMMLC108D-LOG", "position==23");

                                                        byte pressureSwitch = 0x00;
                                                        if (pressureLimit_switch.isChecked()) {
                                                            pressureSwitch = 0x01;
                                                        }
                                                        try {
                                                            Log.v("MMMMLC108D-LOG", "position==24 pressureSwitch : " + pressureSwitch);
                                                            byte[] BYTE_SEND_LOG_REQUEST = {0x41, 0x54, 0x05, 0x01, 0x03, Byte.parseByte(strOnMin), Byte.parseByte(strOnSec), Byte.parseByte(stroffHr),
                                                                    Byte.parseByte(stroffMin), Byte.parseByte(stroffSec), pressureSwitch, (byte) msb, (byte) lsb,
                                                                    0x20, 0x20, 0x20, 0x20, 0x20, 0x0D, 0x0A};
                                                            Log.v("MMMMLC108D-LOG", "position==25");
                                                            mBluetoothLe.writeCustomCharacteristic(BYTE_SEND_LOG_REQUEST);
                                                            Log.v(TAG, "SSSS Write function 51..." + BYTE_SEND_LOG_REQUEST.toString());

                                                            StringBuilder sb1 = new StringBuilder();
                                                            for (byte bb : BYTE_SEND_LOG_REQUEST) {
                                                                if (sb1.length() > 0) {
                                                                    sb1.append(':');
                                                                }
                                                                sb1.append(String.format("%02x", bb));
                                                            }
                                                            Log.v(TAG, "SSSS Write function 51..." + sb1.toString());

                                                            Log.v("MMMMLC108D-LOG", "position==26");
                                                            Toast.makeText(mCtx, "Update Mode settings ..", Toast.LENGTH_SHORT).show();
                                                            addNoteDialog.dismiss();

                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                            Toast.makeText(mCtx, "Bluetooth Connection problem", Toast.LENGTH_SHORT).show();
                                                        }
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                } else {
                                                    Uri uri_insert;
                                                    try {
                                                        uri_insert = holder.main_Text.getContext().getContentResolver()
                                                                .insert(ModeTimeTable.CONTENT_URI, values);
                                                        Log.v("LC108D-LOG", "Insert : " + uri_insert);

                                                        String strOnMin = strTime.substring(2, 4);

                                                        String strOnSec = strTime.substring(4, 6);

                                                        String stroffHr = strTime.substring(6, 8);

                                                        String stroffMin = strTime.substring(8, 10);

                                                        String stroffSec = strTime.substring(10, 12);
//
//                                                byte pressureSwitch_ = 0x00;
//                                                if (pressureLimit_switch.isChecked()) {
//                                                    pressureSwitch_ = 0x01;
//                                                }

                                                        int value = parseInt(PRESSURE_LIMIT_VALUE);
                                                        Log.v("MMMMLC108D-LOG", "position==22 value : " + value);
                                                        String Hex_PressureCount = Integer.toHexString(value);
                                                        Log.v("MMMMLC108D-LOG", "position==22 Hex_PressureCount : " + Hex_PressureCount);
                                                        int firstValue = Integer.parseInt(Hex_PressureCount, 16);
                                                        //  int firstValue = Integer.parseInt(String.valueOf(Hex_PressureCount));
                                                        //Integer.parseInt("-FF", 16) ; // returns -255
                                                        Log.v("MBMBLC108D-LOG", "position==22 firstValue5 : " + firstValue);
                                                        int msb = Integer.parseInt(String.valueOf(firstValue >> 8));
                                                        Log.v("MMMMLC108D-LOG", "position==22 msb : " + msb);
                                                        int lsb = Integer.parseInt(String.valueOf(firstValue & 0xFF));
                                                        Log.v("MMMMLC108D-LOG", "position==22 lsb : " + lsb);
                                                        byte pressureSwitch1 = 0x00;
                                                        if (pressureLimit_switch.isChecked()) {
                                                            pressureSwitch1 = 0x01;
                                                        }
                                                        Log.v("MMMMLC108D-LOG", "position==22 pressureSwitch1 : " + pressureSwitch1);
                                                        byte[] BYTE_SEND_LOG_REQUEST = {0x41, 0x54, 0x05, 0x01, 0x03, Byte.parseByte(strOnMin), Byte.parseByte(strOnSec), Byte.parseByte(stroffHr),
                                                                Byte.parseByte(stroffMin), Byte.parseByte(stroffSec), pressureSwitch1, (byte) msb, (byte) lsb,
                                                                0x20, 0x20, 0x20, 0x20,
                                                                0x20, 0x0D, 0x0A};
                                                        try {
                                                            mBluetoothLe.writeCustomCharacteristic(BYTE_SEND_LOG_REQUEST);
                                                            Log.v(TAG, "Write function 51...");
                                                            Toast.makeText(mCtx, "Update Mode settings ...", Toast.LENGTH_SHORT).show();
                                                            addNoteDialog.dismiss();
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                            Toast.makeText(mCtx, "Bluetooth Connection problem", Toast.LENGTH_SHORT).show();
                                                        }
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                                if (checkExists != null) {
                                                    checkExists.close();
                                                }
                                            } else {
                                                if (flag == 1) {

                                                    Log.d(TAG,"Enter loop else");
                                                    txt_pressure_limit.setError("minimum pressure limit is 5");


                                                }
                                                else {
                                                    offtime_sec.setError("min 10 seconds");
                                                }

                                            }
                                        } else if (position == 3) {

                                            //offtime_sec.setText("10");

                                            Log.d("Mode", "DUAL_LINE_MODE");
                                            Log.v("LC108D-LOG", "position==0");
                                            //insert record into mode table
                                            String onHour = ontime_hours.getText().toString();
                                            String onMin = ontime_min.getText().toString();
                                            String onSec = ontime_sec.getText().toString();

                                            if (onHour.equals("")) {
                                                onHour = "00";
                                            } else if (parseInt(String.valueOf(onHour)) > 99) {
                                                onHour = "99";
                                            }
                                            if (onMin.equals("")) {
                                                onMin = "00";
                                            } else if (parseInt(String.valueOf(onMin)) > 59) {
                                                onMin = "59";
                                            }
                                            if (onSec.equals("")) {
                                                onSec = "00";
                                            } else if (parseInt(String.valueOf(onSec)) > 59) {
                                                onSec = "59";
                                            }

                                            if (ontime_hours.getText().toString().length() < 2) {
                                                onHour = "0" + onHour;
                                            }
                                            if (ontime_min.getText().toString().length() < 2) {
                                                onMin = "0" + onMin;
                                            }
                                            if (ontime_sec.getText().toString().length() < 2) {
                                                onSec = "0" + onSec;
                                            }
                                            String offHour = offtime_hours.getText().toString();
                                            String offMin = offtime_min.getText().toString();
                                            String offSec = offtime_sec.getText().toString();

                                            if (offHour.equals("")) {
                                                offHour = "00";
                                            } else if (parseInt(String.valueOf(offHour)) > 99) {
                                                offHour = "99";
                                            }
                                            if (offMin.equals("")) {
                                                offMin = "00";
                                            } else if (parseInt(String.valueOf(offMin)) > 59) {
                                                offMin = "59";
                                            }
                                            if (offSec.equals("")) {
                                                offSec = "00";
                                            } else if (parseInt(String.valueOf(offSec)) > 59) {
                                                offSec = "59";
                                            }

                                            if (offtime_hours.getText().toString().length() < 2) {
                                                offHour = "0" + offHour;
                                            }
                                            if (offtime_min.getText().toString().length() < 2) {
                                                offMin = "0" + offMin;
                                            }
                                            if (offtime_sec.getText().toString().length() < 2) {
                                                offSec = "0" + offSec;
                                            }

                                            /*int oh = Integer.parseInt(offHour);
                                            int om = Integer.parseInt(offMin);
                                            int os = Integer.parseInt(offSec);
                                            Log.v("XXXXXXXXXXXX Off time", oh + ":" + om + ":" + os);
                                            int flag = 1;

                                            if (oh == 0) {
                                                if (om == 0) {
                                                    if (os < 10) {
                                                        flag = 0;
                                                    } else {
                                                        flag = 1;
                                                    }
                                                }
                                            }*/



                                            int ohon = Integer.parseInt(onHour);
                                            int omon = Integer.parseInt(onMin);
                                            int oson = Integer.parseInt(onSec);


                                            int oh = Integer.parseInt(offHour);
                                            int om = Integer.parseInt(offMin);
                                            int os = Integer.parseInt(offSec);
                                            Log.v("XXXXXXXXXXXX Off time", oh + ":" + om + ":" + os);
                                            int flag = 1;


/*
                                            if (oh == 0) {
                                                if (om == 0) {
                                                    if (os < 10) {
                                                        if(oson <10 ) {
                                                            flag = 0;
                                                        }
                                                    } else {
                                                        if(oson<10) {
                                                            flag = 0;
                                                            ontime_sec.setError("min 10 sec required");

                                                        }
                                                        else
                                                            flag = 1;
                                                    }
                                                }
                                                else {
                                                    if (oson < 10)
                                                    {
                                                        flag = 0;
                                                        ontime_sec.setError("min 10 sec required");


                                                    }
                                                    else
                                                        flag = 1;
                                                }


                                            }

                                            else
                                            {
                                                if(oson<10) {
                                                    flag = 0;
                                                    ontime_sec.setError("min 10 sec required");

                                                }
                                                else {
                                                    flag = 1;
                                                }
                                            }*/




                                            if (oh == 0) {
                                                if (om == 0) {
                                                    if (os < 10) {
                                                        if (omon == 0) {
                                                            if (oson < 10) {
                                                                flag = 0;
                                                                ontime_sec.setError("min 10 sec required");
                                                                offtime_sec.setError("min 10 sec required");

                                                            }

                                                            if (os < 10) {
                                                                flag = 0;
                                                                offtime_sec.setError("min 10 sec required");

                                                            }


                                                        }
                                                        else{

                                                            if (oh == 0) {
                                                                if (om == 0) {
                                                                    if (os < 10) {
                                                                        flag = 0;
                                                                        offtime_sec.setError("min 10 sec required");

                                                                    }
                                                                }
                                                            }


                                                        }
                                                    } else {
                                                        if(omon==0) {
                                                            if (oson < 10) {
                                                                flag = 0;
                                                                ontime_sec.setError("min 10 sec required");

                                                            }
                                                        }

                                                    }
                                                }
                                                else {
                                                    if(omon==0)
                                                    {
                                                        if (oson < 10)
                                                        {
                                                            flag = 0;
                                                            ontime_sec.setError("min 10 sec required");


                                                        }
                                                    }

                                                }


                                            }

                                            else {

                                                flag = 1;
                                                if (omon == 0) {
                                                    if (oson < 10) {
                                                        flag = 0;
                                                        ontime_sec.setError("min 10 sec required");
                                                    }
                                                }




                                            }


                                            String strTime = onHour + onMin + onSec + offHour + offMin + offSec;
                                            Log.d(TAG,"Display dual line mode strtime"+strTime);
                                            if (flag == 1) {
                                                //Insert into the table
                                                ContentValues values = new ContentValues();
                                                values.put(ModeTimeTable.MODE_NAME, ModeTimeTable.DUAL_LINE_MODE);
                                                values.put(ModeTimeTable.TIME, strTime);
                                                //values.put(WHEEL_COUNT, "0");
                                                values.put(PRESSURE_LIMIT, "0");
                                                values.put(ModeTimeTable.STATUS, modeStatus.get(position));
                                                Cursor checkExists = holder.main_Text.getContext().getContentResolver().
                                                        query(ModeTimeTable.CONTENT_URI,
                                                                null,
                                                                ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.DUAL_LINE_MODE},
                                                                null);
                                                if (checkExists != null && checkExists.moveToFirst()) {
                                                    try {

                                                        holder.main_Text.getContext().getContentResolver().update(ModeTimeTable.CONTENT_URI, values, ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.DUAL_LINE_MODE});
                                                        Log.v("LC108D-LOG", "update");

                                                        String strOnMin = strTime.substring(2, 4);

                                                        String strOnSec = strTime.substring(4, 6);

                                                        String stroffHr = strTime.substring(6, 8);

                                                        String stroffMin = strTime.substring(8, 10);

                                                        String stroffSec = strTime.substring(10, 12);

                                                        byte[] BYTE_SEND_LOG_REQUEST = {0x41, 0x54, 0x05, 0x01, 0x04, Byte.parseByte(strOnMin), Byte.parseByte(strOnSec), Byte.parseByte(stroffHr),
                                                                Byte.parseByte(stroffMin), Byte.parseByte(stroffSec), 0x20, 0x20,
                                                                0x20, 0x20, 0x20, 0x20, 0x20,
                                                                0x20, 0x0D, 0x0A};
                                                        try {
                                                            mBluetoothLe.writeCustomCharacteristic(BYTE_SEND_LOG_REQUEST);
                                                            Log.v(TAG, "Write function 51...");
                                                            addNoteDialog.dismiss();
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                            Toast.makeText(mCtx, "Bluetooth Connection problem", Toast.LENGTH_SHORT).show();
                                                        }
                                                        Toast.makeText(mCtx, "Update Mode settings.", Toast.LENGTH_SHORT).show();
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                } else {
                                                    Uri uri_insert;
                                                    try {
                                                        uri_insert = holder.main_Text.getContext().getContentResolver()
                                                                .insert(ModeTimeTable.CONTENT_URI, values);
                                                        Log.v("LC108D-LOG", "Insert : " + uri_insert);

                                                        String strOnMin = strTime.substring(2, 4);

                                                        String strOnSec = strTime.substring(4, 6);

                                                        String stroffHr = strTime.substring(6, 8);

                                                        String stroffMin = strTime.substring(8, 10);

                                                        String stroffSec = strTime.substring(10, 12);

                                                        byte[] BYTE_SEND_LOG_REQUEST = {0x41, 0x54, 0x05, 0x01, 0x04, Byte.parseByte(strOnMin), Byte.parseByte(strOnSec), Byte.parseByte(stroffHr),
                                                                Byte.parseByte(stroffMin), Byte.parseByte(stroffSec), 0x20, 0x20,
                                                                0x20, 0x20, 0x20, 0x20, 0x20,
                                                                0x20, 0x0D, 0x0A};
                                                        try {
                                                            mBluetoothLe.writeCustomCharacteristic(BYTE_SEND_LOG_REQUEST);
                                                            Log.v(TAG, "Write function 51...");
                                                            addNoteDialog.dismiss();
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                            //Toast.makeText(mCtx, "Bluetooth Connection problem", Toast.LENGTH_SHORT).show();
                                                        }
                                                        Toast.makeText(mCtx, "Update Mode settings", Toast.LENGTH_SHORT).show();
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                                if (checkExists != null) {
                                                    checkExists.close();
                                                }
                                            } else {
                                                offtime_sec.setError("min 10 seconds");
                                            }
                                        }
                                    }
                                }

                            }
                            else {
                                Toast.makeText(mCtx, "Bluetooth Connection problem", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    addNoteDialog.show();
                }
            });
        }


        @Override
        public int getItemCount() {
            return modeName.size();
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView main_Text;
        private RadioButton radioButton;

        private ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.list_single_check, parent, false));

            main_Text = (TextView) itemView.findViewById(R.id.tv_MainText);
            radioButton = (RadioButton) itemView.findViewById(R.id.mode_option);
        }
    }
}
