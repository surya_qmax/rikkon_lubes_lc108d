package com.mmm.rikon_lc108d.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mmm.rikon_lc108d.MainActivity;
import com.mmm.rikon_lc108d.R;
import com.mmm.rikon_lc108d.db.Login_Table;

/**
 * A login screen that offers login via email/password.
 */
public class RegisterActivity extends AppCompatActivity {

    private static final String TAG = "RegisterActivity";
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;
    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPhoneNumberView;
    private EditText mUserNameView;
    private View mProgressView;
    private View mLoginFormView;
    private EditText mPassword;

    private String USER_TYPE = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_register);

        Bundle b = new Bundle();
        b = getIntent().getExtras();
        if(b != null) {
            USER_TYPE = b.getString("user_type");
        } else {
            Log.v(TAG, "Bundle Null");
        }


        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        mPassword = (EditText) findViewById(R.id.password);

        mUserNameView = (EditText) findViewById(R.id.user_name);

        mPhoneNumberView = (EditText) findViewById(R.id.phone_number);

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPhoneNumberView.setError(null);
        mUserNameView.setError(null);
        mPassword.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String phoneNumber = mPhoneNumberView.getText().toString();
        String userName = mUserNameView.getText().toString();
        String password = mPassword.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(phoneNumber) && !isPasswordValid(phoneNumber)) {
            mPhoneNumberView.setError(getString(R.string.error_invalid_password));
            focusView = mPhoneNumberView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new UserLoginTask(getApplicationContext(), email, phoneNumber, userName, password, USER_TYPE);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPhoneNumber;
        private final String mUserName;
        private final String mPassword;
        private Context mCtx;
        private String User_type;
        private int flag = 0;

        UserLoginTask(Context context, String email, String phoneNumber, String name,
                       String password, String user_type) {
            mEmail = email;
            mPhoneNumber = phoneNumber;
            mUserName = name;
            mPassword = password;
            User_type = user_type;
            mCtx = context;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                flag = 0;
                Cursor countCursor = getContentResolver().query(Login_Table.CONTENT_URI,
                        new String[] {"count(*) AS count"},
                        Login_Table.USER_TYPE+"=?",new String[] {Login_Table.ADMIN},
                        null);

                countCursor.moveToFirst();
                int count = countCursor.getInt(0);

                if(count<3) {
                    ContentValues values = new ContentValues();
                    values.put(Login_Table.USER_NAME, mUserName);
                    values.put(Login_Table.USER_TYPE, User_type);
                    values.put(Login_Table.EMAIL_ID, mEmail);
                    values.put(Login_Table.PASSWORD, mPassword);
                    values.put(Login_Table.PHONE_NUMBER, mPhoneNumber);
                    Uri uri_insert = null;
                    try {
                        uri_insert = getContentResolver().insert(Login_Table.CONTENT_URI, values);
                        Log.v(TAG, "One row inserted");
                        return true;
                    } catch (Exception e) {
                        return false;
                    }
                } else {
                    flag = 1;
                    return false;
                }

            } catch (Exception e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                if(User_type.equals(Login_Table.ADMIN)) {
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                } else if (User_type.equals(Login_Table.USER)){
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                }
                Toast.makeText(getApplicationContext(), "Register User Success", Toast.LENGTH_SHORT).show();
            } else {
                if(flag == 1) {
                    Toast.makeText(getApplicationContext(), "3 ADMIN Only Allowed / Registeration failed", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "User name already exist / Registeration failed", Toast.LENGTH_SHORT).show();

                }
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}

