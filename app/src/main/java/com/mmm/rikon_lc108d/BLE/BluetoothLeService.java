/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mmm.rikon_lc108d.BLE;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Binder;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Selection;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.mmm.rikon_lc108d.LC108DApplication;
import com.mmm.rikon_lc108d.activity.LogActivity;
import com.mmm.rikon_lc108d.activity.UserMsgTransferService;
import com.mmm.rikon_lc108d.db.ModeTimeTable;
import com.mmm.rikon_lc108d.service.RequestService;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;
import java.util.UUID;

/**
 * Service for managing connection and data communication with a GATT server hosted on a
 * given Bluetooth LE device.
 */
public class BluetoothLeService extends Service {
    public final static String ACTION_GATT_CONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE =
            "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA =
            "com.example.bluetooth.le.EXTRA_DATA";
    public final static UUID UUID_HEART_RATE_MEASUREMENT =
            UUID.fromString(SampleGattAttributes.HEART_RATE_MEASUREMENT);
    private final static String TAG = BluetoothLeService.class.getSimpleName();
    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;
    private static final UUID GET_SERVICE_UUID = UUID.fromString("0000bbb0-0000-1000-8000-00805f9b34fb");
    private static final UUID GET_DATA_CHAR = UUID.fromString("0000bbb2-0000-1000-8000-00805f9b34fb");
    private static final UUID CONFIG_DESCRIPTOR = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    public static long RECEIVE_BYTE_COUNT = 0;
    public static long RECEIVE_LINE_COUNT = 0;
    public static long countheader = 0;
    public static long countlineno = 0;


    public static long RECEIVE_BYTE_COUNT_FINISH = 0;
    private static UserMsgTransferService userService = null;


    private final IBinder mBinder = new LocalBinder();
    BluetoothGattCharacteristic get_characteristic;
    File receiveFile = new File(Environment.getExternalStorageDirectory() + File.separator + "receive.txt");
    FileOutputStream mReceiveFOS = null;
    FileInputStream mReceiveFIS = null;
    byte[] bytes = null;
    private File file;
    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    private BluetoothGatt mBluetoothGatt;
    private int mConnectionState = STATE_DISCONNECTED;
    // Implements callback methods for GATT events that the app cares about.  For example,
    // connection change and services discovered.
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            String intentAction;
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.v(TAG, "UUUUU onConnectionStateChange STATE_CONNECTED...");
                intentAction = ACTION_GATT_CONNECTED;
                mConnectionState = STATE_CONNECTED;
                broadcastUpdate(intentAction);
                userService = new UserMsgTransferService();

                SharedPreferences sharedpreferences_ = getSharedPreferences(LC108DApplication.PREF_DEVICE_CONNECTION_STATUS, Context.MODE_PRIVATE);

                SharedPreferences.Editor editor = sharedpreferences_.edit();
                editor.clear();
                editor.putString(LC108DApplication.PREF_DEVICE_NAME, gatt.getDevice().getName());
                editor.putString(LC108DApplication.PREF_DEVICE_ADDRESS, gatt.getDevice().getAddress());
                editor.commit();

                updateMessageStatus(LC108DApplication.STATUS_CONNECTED);

                SharedPreferences sharedpreferences = getSharedPreferences(LC108DApplication.PREF_DEVICE_CONNECTION_STATUS, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor_seq_no = sharedpreferences.edit();
                editor_seq_no.clear();
                editor_seq_no.putString(LC108DApplication.PREF_DEVICE_CONNECTION_STATUS_KEY, LC108DApplication.STATUS_CONNECTED);
                editor_seq_no.commit();
                //Display Toast after Bluetooth Connection
                //Toast.makeText(getApplicationContext(), "BluetoothProfile STATE_CONNECTED", Toast.LENGTH_SHORT).show();

                mBluetoothGatt.discoverServices();

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                intentAction = ACTION_GATT_DISCONNECTED;
                mConnectionState = STATE_DISCONNECTED;

                Log.v(TAG, "UUUUU onConnectionStateChange STATE_DISCONNECTED...");


                broadcastUpdate(intentAction);
                SharedPreferences sharedpreferences_ = getSharedPreferences(LC108DApplication.PREF_DEVICE_CONNECTION_STATUS, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences_.edit();
                editor.clear();
                editor.putString(LC108DApplication.PREF_DEVICE_NAME, gatt.getDevice().getName());
                editor.putString(LC108DApplication.PREF_DEVICE_ADDRESS, gatt.getDevice().getAddress());
                editor.commit();

                updateMessageStatus(LC108DApplication.STATUS_DISCONNECTED);

                SharedPreferences sharedpreferences = getSharedPreferences(LC108DApplication.PREF_DEVICE_CONNECTION_STATUS, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor_seq_no = sharedpreferences.edit();
                editor_seq_no.putString(LC108DApplication.PREF_DEVICE_CONNECTION_STATUS_KEY, LC108DApplication.STATUS_DISCONNECTED);

                editor_seq_no.clear();
                editor_seq_no.commit();

                Intent serviceIntent = new Intent(getApplicationContext(), RequestService.class);
                stopService(serviceIntent);
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);
                if (mBluetoothGatt != null) {
                    Log.v(TAG, "mConnectedGatt not null");
                    /*check if the service is available on the device*/
                    BluetoothGattService mCustomService = mBluetoothGatt.getService(GET_SERVICE_UUID);
                    if (mCustomService == null) {
                        Log.w(TAG, "Custom BLE Service not found");
                        return;
                    }
        /*get the read characteristic from the service*/
                    BluetoothGattCharacteristic mWriteCharacteristic = mCustomService.getCharacteristic(GET_DATA_CHAR);
                    //Enable local notifications
                    gatt.setCharacteristicNotification(mWriteCharacteristic, true);
                    //Enabled remote notifications
                    BluetoothGattDescriptor desc = mWriteCharacteristic.getDescriptor(CONFIG_DESCRIPTOR);
                    desc.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                    gatt.writeDescriptor(desc);

//        // This is specific to Heart Rate Measurement.
                    BluetoothGattDescriptor descriptor = mWriteCharacteristic.getDescriptor(
                            UUID.fromString(SampleGattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
                    descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                    mBluetoothGatt.writeDescriptor(descriptor);
                }
            } else {
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }

//            if(LC108DApplication.FLAG_REQUEST_DEVICE_STATUS == 1) {
//                //send Request for getting the device status
//                Log.v(TAG, "UUUUU LC108DApplication.FLAG_REQUEST_DEVICE_STATUS == 1 if ");
//                byte[] BYTE_SEND_LOG_REQUEST = {0x41, 0x54, 0x03, 0x01, 0x03, 0x04, 0x05,
//                        0x06, 0x07, 0x08, 0x09, 0x41,
//                        0x56, 0x45, 0x66, 0x6d, 0x74,
//                        0x20, 0x0D, 0x0A};
//                try {
//                    writeCustomCharacteristic(BYTE_SEND_LOG_REQUEST);
//                    Log.v(TAG, " UUUUU DEVICE COMMUNICATION Write 31... success");
//                    LC108DApplication.FLAG_REQUEST_DEVICE_STATUS = 0;
//                    // Toast.makeText(getApplicationContext(), "Bluetooth Connection problem", Toast.LENGTH_SHORT).show();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    Log.v(TAG, " UUUUU DEVICE COMMUNICATION Write 31... failed");
//                }
//            }

        }

        private void updateMessageStatus(String mMessage) {
            Log.v("QQQQM", " String Message for update : " + mMessage);
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic,
                                         int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
            }
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {
            Log.v(TAG, "KKKKKa  DEVICE COMMUNICATION ***** onCharacteristicChanged");
            //  broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);//
            bytes = characteristic.getValue();

            StringBuilder sb = new StringBuilder();

            for (byte b : bytes) {
                if (sb.length() > 0) {
                    sb.append(':');
                }
                sb.append(String.format("%02x", b));
            }

            Log.v(TAG, "KKKKKa DEVICE COMMUNICATION ***** Received Value from sb1 : "+ sb);

            if (bytes[0] == 0x02 && bytes[1] == 0x02) {
                mReConvLength(bytes);
                Log.v(TAG, "KKKKK DEVICE COMMUNICATION onCharacteristicChanged 22 Received ");
                Log.v(TAG, "KKKKK DEVICE COMMUNICATION onCharacteristicChanged 22 Received "+getApplicationContext());

//
                Log.v("MMMEEEE", "KKKKB DEVICE COMMUNICATION Mode selection selectedModeName2 : ");
                countheader++;
                Log.v("MMMEEEE", "KKKKB DEVICE COMMUNICATION Count Header : "+countheader);

                if(countheader==1)
                userService.mCreateReceiveFile(getApplicationContext());
                //send start command
                byte[] BYTE_SEND_LOG_REQUEST = {0x41, 0x54, 0x02, 0x03, 0x03, 0x04, 0x05,
                        0x06, 0x07, 0x08, 0x09, 0x41,
                        0x56, 0x45, 0x66, 0x6d, 0x74,
                        0x20, 0x0D, 0x0A};
                try {
                    writeCustomCharacteristic(BYTE_SEND_LOG_REQUEST);
                    StringBuilder sb1 = new StringBuilder();
                    for (byte bb : BYTE_SEND_LOG_REQUEST) {
                        if (sb1.length() > 0) {
                            sb1.append(':');
                        }
                        sb1.append(String.format("%02x", bb));
                    }

                    Log.v(TAG, " DEVICE COMMUNICATION Receive Length Write 23..."+sb1);
                   // Toast.makeText(getApplicationContext(), "Bluetooth Connection problem", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Bluetooth Connection problem", Toast.LENGTH_SHORT).show();
                }

                RECEIVE_BYTE_COUNT = 0;
                RECEIVE_BYTE_COUNT_FINISH = 0;
                Log.v(TAG, "********RECEIVE_BYTE_COUNT RECEIVE 22.... = " + RECEIVE_BYTE_COUNT + " *********" + RECEIVE_BYTE_COUNT);

            }

            if (bytes[0] == 0x02 && bytes[1] == 0x04) {
                //write bytes
                Log.v(TAG, " DEVICE COMMUNICATION Data Receive 24...");
                RECEIVE_BYTE_COUNT += 14;
                RECEIVE_LINE_COUNT ++;
                Log.v(TAG, "********RECEIVE_BYTE_COUNT Data RECEIVE 24.... = " + RECEIVE_BYTE_COUNT + " *********" + RECEIVE_BYTE_COUNT);
                //       LogActivity.mReceiveByte.setText(""+RECEIVE_BYTE_COUNT);
                try {
                    StringBuilder sb1 = new StringBuilder();

                    for (byte bb : bytes) {
                        if (sb1.length() > 0) {
                            sb1.append(':');
                        }
                        sb1.append(String.format("%02x", bb));
                    }

                    Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204; : "+ sb1);
                    Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes)0204;RECEIVE_LINE_COUNT : "+ RECEIVE_LINE_COUNT);

                    userService.getByte(bytes);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.v(TAG, "********RECEIVE_BYTE_COUNT = " + RECEIVE_BYTE_COUNT + " *********");
                sendMyBroadCastLogStatus("progress");

            }

            if (bytes[0] == 0x02 && bytes[1] == 0x06) {
                //finished
                Log.v(TAG, " DEVICE COMMUNICATION Read more logggs Receive 26...");
                //  RECEIVE_BYTE_COUNT = 0;
//                    userService.outputFile();

                //send start command
                try {
                    byte[] BYTE_SEND_LOG_REQUEST = {0x41, 0x54, 0x02, 0x07, 0x03, 0x04, 0x05,
                            0x06, 0x07, 0x08, 0x09, 0x41,
                            0x56, 0x45, 0x66, 0x6d, 0x74,
                            0x20, 0x0D, 0x0A};
                    try {
                        writeCustomCharacteristic(BYTE_SEND_LOG_REQUEST);

                        StringBuilder sb1 = new StringBuilder();

                        for (byte bb : BYTE_SEND_LOG_REQUEST) {
                            if (sb1.length() > 0) {
                                sb1.append(':');
                            }
                            sb1.append(String.format("%02x", bb));
                        }

                        Log.v(TAG, "KKKKKa Android Data receive userService.getByte(bytes);0206 : "+ sb1);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Bluetooth Connection problem", Toast.LENGTH_SHORT).show();
                    }
                    Log.v(TAG, " DEVICE COMMUNICATION Write 27...");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                sendMyBroadCastLogStatus("moreinprogress");

            }

            if (bytes[0] == 0x02 && bytes[1] == 0x05) {
                //finished
                Log.v(TAG, " DEVICE COMMUNICATION Receive 25...Completed");
                RECEIVE_BYTE_COUNT = 0;
                RECEIVE_BYTE_COUNT_FINISH = 1;
                /////////////////// File transfer completed

                countheader =0;

                sendMyBroadCastLogStatus("completed");



//                    userService.outputFile();
            }
            if (bytes[0] == 0x05 && bytes[1] == 0x02) {
                Log.v(TAG, "RECEIVE 52 :: ");
                //GET LAST BYTE [] FOR GETTING MODE SELECTION POINT
                //SEND SPECIFIC MODE ON/OFF TIME
                int modePoint = bytes[19];

                if (modePoint == 1) {
                    //Get Mode time
                    Cursor modeTimeCursor = getContentResolver().query(ModeTimeTable.CONTENT_URI,
                            null,
                            ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.PROGRESSIVE_MODE},
                            null);

                    if (modeTimeCursor != null && modeTimeCursor.moveToFirst()) {
                        String getTime = modeTimeCursor.getString(modeTimeCursor.getColumnIndex(ModeTimeTable.TIME));
                    } else {
                    }
                    modeTimeCursor.close();

                } else if (modePoint == 2) {
                    //Get Mode time
                    Cursor modeTimeCursor = getContentResolver().query(ModeTimeTable.CONTENT_URI,
                            null,
                            ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.TRACK_MODE},
                            null);

                    if (modeTimeCursor != null && modeTimeCursor.moveToFirst()) {
                        String getTime = modeTimeCursor.getString(modeTimeCursor.getColumnIndex(ModeTimeTable.TIME));
                    } else {
                    }
                    modeTimeCursor.close();

                } else if (modePoint == 3) {
                    //Get Mode time
                    Cursor modeTimeCursor = getContentResolver().query(ModeTimeTable.CONTENT_URI,
                            null,
                            ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.INJECTOR_MODE},
                            null);

                    if (modeTimeCursor != null && modeTimeCursor.moveToFirst()) {
                        String getTime = modeTimeCursor.getString(modeTimeCursor.getColumnIndex(ModeTimeTable.TIME));
                    } else {
                    }
                    modeTimeCursor.close();
                } else if (modePoint == 4) {
                    //Get Mode time
                    Cursor modeTimeCursor = getContentResolver().query(ModeTimeTable.CONTENT_URI,
                            null,
                            ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.DUAL_LINE_MODE},
                            null);

                    if (modeTimeCursor != null && modeTimeCursor.moveToFirst()) {
                        String getTime = modeTimeCursor.getString(modeTimeCursor.getColumnIndex(ModeTimeTable.TIME));
                    } else {
                    }
                    modeTimeCursor.close();
                }
            }

            //Receive Device status
            if (bytes[0] == 0x03 && bytes[1] == 0x02) {
                Log.v(TAG, "UUUUU DEVICE COMMUNICATION onCharacteristicChanged 32 Received ");
                RequestService deviceStatusService = new RequestService();
                deviceStatusService.setModeStatus(bytes, getApplicationContext());

            } if (bytes[0] == 0x03 && bytes[1] == 0x04) {
                Log.v(TAG, "UUUUU DEVICE COMMUNICATION onCharacteristicChanged 34 Received ");
                RequestService deviceStatusService = new RequestService();
                deviceStatusService.setSwitchStatus(bytes, getApplicationContext());
            }

            if (bytes[0] == 0x0A && bytes[1] == 0x02) {
                Log.v(TAG, "UUUUU DEVICE COMMUNICATION onCharacteristicChanged 111 Received ");
                RequestService deviceStatusService = new RequestService();
                deviceStatusService.setWheelsPerDayPassed(bytes, getApplicationContext());

            }



            if (bytes[0] == 0x09 && bytes[1] == 0x02) {
                Log.v(TAG, "UUUUU DEVICE COMMUNICATION onCharacteristicChanged 111 Received ");

                sendMyBroadCast("disable");

            }

            if(bytes[0] == 0x03 && bytes[1] ==0x06)

            {


                Log.d(TAG,"Serial No set data received");
                if(bytes[8]==0x00 && bytes[9]==0x00 && bytes[10]==0x00 &&
                        bytes[11]==0x00 && bytes[12]==0x00 && bytes[13]==0x00)
                {

                    Log.d(TAG,"Serial No set data received if");

                    sendMyBroadCast("0");


                }

                else

                {
                    int temp =0;
                    int serialnoval=0;
                    int serialnostartpos= 8;
                    StringBuilder serialnostring = new StringBuilder();

                    for(int i=0;i<6;i++)
                    {
                        serialnoval = serialnoval * 10;

                        serialnoval += bytes[serialnostartpos];

                        Log.d(TAG,"Serial No set data received else check bytes received"+serialnoval);
                        /*if(bytes[serialnostartpos]<0)
                        {
                            temp = temp +256;
                        }*/
                        serialnostartpos++;
                        //temp =  temp<<8;
                        //serialnoval = serialnoval + temp;

                       // Log.d(TAG,"Serial No set data received else check bytes received 1 $$$"+temp);
                        Log.d(TAG,"Serial No set data received else check bytes received 2 $$$"+serialnoval);

                        serialnostring.append(String.valueOf(serialnoval));

                        serialnoval = 0;



                    }

                    Log.d(TAG,"Serial No set data received else check bytes received ***"+serialnostring);

                    sendMyBroadCast(String.valueOf(serialnostring));

                    Log.d(TAG,"Serial No set data received else");

                }
            }
        }
    };



    private void sendMyBroadCastLogStatus(String  logstatus)
    {
        try
        {
            Log.d(TAG,"logstatusset data received if sendbroadcast called");

            Intent broadCastIntent = new Intent();
            broadCastIntent.setAction(LogActivity.BROADCAST_ACTION);

            // uncomment this line if you want to send data
            broadCastIntent.putExtra("data", logstatus);

            sendBroadcast(broadCastIntent);

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }



    private void sendMyBroadCast(String  serialno)
    {
        try
        {
            Log.d(TAG,"Serial No set data received if sendbroadcast called"+serialno);

            Intent broadCastIntent = new Intent();
            broadCastIntent.setAction(DeviceControlActivity.BROADCAST_ACTION);

            // uncomment this line if you want to send data
            broadCastIntent.putExtra("data", serialno);

            sendBroadcast(broadCastIntent);

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public long mReConvLength(byte[] reqLength) {
        //  reqLength[5] = (byte) 0xf4;
        long value = 0;
        for (int i = 2; i < 6; i++) {
//                Log.v(TAG, "Receiving display data arrary : "+ reqLength[i]);
            value = (value << 8) + (reqLength[i] & 0xff);
            long ii = reqLength[i] & 0xff;
        }
        Log.v(TAG, "Receivinglength = length = " + value);

        return value;
    }

    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        sendBroadcast(intent);
    }

    private void broadcastUpdate(final String action,
                                 final BluetoothGattCharacteristic characteristic) {
        final Intent intent = new Intent(action);

        // This is special handling for the Heart Rate Measurement profile.  Data parsing is
        // carried out as per profile specifications:
        // http://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.heart_rate_measurement.xml
        if (UUID_HEART_RATE_MEASUREMENT.equals(characteristic.getUuid())) {
            int flag = characteristic.getProperties();
            int format = -1;
            if ((flag & 0x01) != 0) {
                format = BluetoothGattCharacteristic.FORMAT_UINT16;
                Log.d(TAG, "Heart rate format UINT16.");
            } else {
                format = BluetoothGattCharacteristic.FORMAT_UINT8;
                Log.d(TAG, "Heart rate format UINT8.");
            }
            final int heartRate = characteristic.getIntValue(format, 1);
            Log.d(TAG, String.format("Received heart rate: %d", heartRate));
            intent.putExtra(EXTRA_DATA, String.valueOf(heartRate));
        } else {
            // For all other profiles, writes the data formatted in HEX.
            final byte[] data = characteristic.getValue();
            if (data != null && data.length > 0) {
                final StringBuilder stringBuilder = new StringBuilder(data.length);
                for (byte byteChar : data)
                    stringBuilder.append(String.format("%02X ", byteChar));
                intent.putExtra(EXTRA_DATA, new String(data) + "\n" + stringBuilder.toString());
            }
        }
        sendBroadcast(intent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        // After using a given device, you should make sure that BluetoothGatt.close() is called
        // such that resources are cleaned up properly.  In this particular example, close() is
        // invoked when the UI is disconnected from the Service.
        close();
        return super.onUnbind(intent);
    }

    /**
     * Initializes a reference to the local Bluetooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize() {
        // For API level 18 and above, get a reference to BluetoothAdapter through
        // BluetoothManager.
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }
        return true;
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @param address The device address of the destination device.
     * @return Return true if the connection is initiated successfully. The connection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public boolean connect(final String address) {
        if (mBluetoothAdapter == null || address == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        // Previously connected device.  Try to reconnect.
        if (mBluetoothDeviceAddress != null && address.equals(mBluetoothDeviceAddress)
                && mBluetoothGatt != null) {
            Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection.");
            if (mBluetoothGatt.connect()) {
                mConnectionState = STATE_CONNECTING;
                return true;
            } else {
                return false;
            }
        }

        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) {
            Log.w(TAG, "Device not found.  Unable to connect.");
            return false;
        }
        // We want to directly connect to the device, so we are setting the autoConnect
        // parameter to false.
        mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
        Log.d(TAG, "Trying to create a new connection.");
        mBluetoothDeviceAddress = address;
        mConnectionState = STATE_CONNECTING;
        return true;
    }

    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public void disconnect() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.disconnect();
    }

    /**
     * After using a given BLE device, the app must call this method to ensure resources are
     * released properly.
     */
    public void close() {
        if (mBluetoothGatt == null) {
            return;
        }
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    /**
     * Request a read on a given {@code BluetoothGattCharacteristic}. The read result is reported
     * asynchronously through the {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
     * callback.
     *
     * @param characteristic The characteristic to read from.
     */
    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.readCharacteristic(characteristic);
    }

    /**
     * Enables or disables notification on a give characteristic.
     *
     * @param characteristic Characteristic to act on.
     * @param enabled        If true, enable notification.  False otherwise.
     */
    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic,
                                              boolean enabled) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }

        BluetoothGattService mCustomService = mBluetoothGatt.getService(GET_SERVICE_UUID);
        if (mCustomService == null) {
            Log.w(TAG, "Custom BLE Service not found");
            return;
        }
        /*get the read characteristic from the service*/
        BluetoothGattCharacteristic mReadCharacteristic = mCustomService.getCharacteristic(GET_DATA_CHAR);
        mBluetoothGatt.setCharacteristicNotification(mReadCharacteristic, enabled);

//        // This is specific to Heart Rate Measurement.
//        if (UUID_HEART_RATE_MEASUREMENT.equals(characteristic.getUuid())) {
//            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(
//                    UUID.fromString(SampleGattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
//            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
//            mBluetoothGatt.writeDescriptor(descriptor);
//        }
    }

    /**
     * Retrieves a list of supported GATT services on the connected device. This should be
     * invoked only after {@code BluetoothGatt#discoverServices()} completes successfully.
     *
     * @return A {@code List} of supported services.
     */
    public List<BluetoothGattService> getSupportedGattServices() {
        if (mBluetoothGatt == null) return null;

        return mBluetoothGatt.getServices();
    }

    public void readCustomCharacteristic() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        /*check if the service is available on the device*/
        // UUID=0000bbb2-0000-1000-8000-00805f9b34fb
        BluetoothGattService mCustomService = mBluetoothGatt.getService(GET_SERVICE_UUID);
        if (mCustomService == null) {
            Log.w(TAG, "Custom BLE Service not found");
            return;
        }
        /*get the read characteristic from the service*/
        BluetoothGattCharacteristic mReadCharacteristic = mCustomService.getCharacteristic(GET_DATA_CHAR);
        if (mBluetoothGatt.readCharacteristic(mReadCharacteristic) == false) {
            Log.w(TAG, "Failed to read characteristic");
        }
    }

    public void writeCustomCharacteristic(int value) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        /*check if the service is available on the device*/
        BluetoothGattService mCustomService = mBluetoothGatt.getService(GET_SERVICE_UUID);
        if (mCustomService == null) {
            Log.w(TAG, "Custom BLE Service not found");
            return;
        }
        /*get the read characteristic from the service*/
        BluetoothGattCharacteristic mWriteCharacteristic = mCustomService.getCharacteristic(GET_DATA_CHAR);
        mWriteCharacteristic.setValue(value, BluetoothGattCharacteristic.FORMAT_UINT8, 0);
        if (mBluetoothGatt.writeCharacteristic(mWriteCharacteristic) == false) {
            Log.w(TAG, "Failed to write characteristic");
        }
    }

    public void writeCustomCharacteristic(byte[] byte_send_log_request) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        /*check if the service is available on the device*/

        BluetoothGattCharacteristic mWriteCharacteristic = mBluetoothGatt.getService(GET_SERVICE_UUID).getCharacteristic(GET_DATA_CHAR);

        mWriteCharacteristic.setValue(byte_send_log_request);
        // get_characteristic.setValue(getUserText);
        mBluetoothGatt.writeCharacteristic(mWriteCharacteristic);
    }

    public class LocalBinder extends Binder {
        public BluetoothLeService getService() {
            return BluetoothLeService.this;
        }
    }
}
