/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mmm.rikon_lc108d.BLE;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Selection;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.mmm.rikon_lc108d.LC108DApplication;
import com.mmm.rikon_lc108d.R;
import com.mmm.rikon_lc108d.activity.LogActivity;
import com.mmm.rikon_lc108d.activity.MobileDetails;
import com.mmm.rikon_lc108d.activity.ModeActivity;
import com.mmm.rikon_lc108d.activity.SwitchActivity;
import com.mmm.rikon_lc108d.db.Login_Table;
import com.mmm.rikon_lc108d.db.ModeTimeTable;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * For a given BLE device, this Activity provides the user interface to connect, display data,
 * and display GATT services and characteristics supported by the device.  The Activity
 * communicates with {@code BluetoothLeService}, which in turn interacts with the
 * Bluetooth LE API.
 */
public class DeviceControlActivity extends AppCompatActivity {
    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";
    private final static String TAG = DeviceControlActivity.class.getSimpleName();
    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";
    private TextView mConnectionState;
    private TextView mDataField;
    private String mDeviceName;
    private String mDeviceAddress;
    private ExpandableListView mGattServicesList;
    public static BluetoothLeService mBluetoothLeService;
    TextView text_state;
    public static  ProgressDialog progressDialog;
    public  EditText etserialno;
    public  Button saveserialno;
    public  Button mBtnsetserial;
    public  TextView tvserialno;
    MyBroadCastReceiver myBroadCastReceiver;
    Button mSwitchControl;
    String dataReceived="";

    public static final String BROADCAST_ACTION = "com.mmm.rikon_lc108d.BLE.broadcastreceiverdemo";
    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            mBluetoothLeService.connect(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    private boolean mConnected = false;
    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    public final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                text_state.setText("State : Connected");
                updateConnectionState(R.string.connected);
                invalidateOptionsMenu();
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(LC108DApplication.FLAG_REQUEST_DEVICE_STATUS == 1) {
                    Log.v(TAG, "UUUUU LC108DApplication.FLAG_REQUEST_DEVICE_STATUS if ");

                    if (mBluetoothLeService != null) {
                        byte[] BYTE_SEND_RTC_TIME = {0x41, 0x54, 0x03, 0x01, 0x20,
                                0x20, 0x20, 0x20, 0x20, 0x20,
                                0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,
                                0x20, 0x0D, 0x0A};
                        try {
                            mBluetoothLeService.writeCustomCharacteristic(BYTE_SEND_RTC_TIME);

                            StringBuilder sb1 = new StringBuilder();
                            for (byte bb : BYTE_SEND_RTC_TIME) {
                                if (sb1.length() > 0) {
                                    sb1.append(':');
                                }
                                sb1.append(String.format("%02x", bb));
                            }

                            Log.v(TAG, "UUUUU Write function 31... success"+sb1);
                            Toast.makeText(getApplicationContext(), "Read Board Status Success", Toast.LENGTH_SHORT).show();
                            LC108DApplication.FLAG_REQUEST_DEVICE_STATUS = 0;
                            StringBuilder sb = new StringBuilder();
                            for (byte b : BYTE_SEND_RTC_TIME) {
                                if (sb.length() > 0) {
                                    sb.append(':');
                                }
                                sb.append(String.format("%02x", b));
                            }
                        } catch (Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(context, "Write function 31... failed", Toast.LENGTH_SHORT).show();
                            Log.v(TAG, "UUUUU Write function 31... failed"+e.toString());
                        }
                    }
                }
                Log.v("mmmma", " DEVICE CONNECTED : " + BluetoothLeService.ACTION_GATT_CONNECTED.equals(action));
            }

            else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                updateConnectionState(R.string.disconnected);
                text_state.setText("State : DisConnected");
                invalidateOptionsMenu();
                Log.v("mmmma", " DEVICE DISCONNECTED : " + BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action));
               // clearUI();
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
                //displayGattServices(mBluetoothLeService.getSupportedGattServices());
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
                displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
                Log.v("mmmma", " RECEIVING BYTE : " + intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
            }
        }
    };
    private BluetoothGattCharacteristic mNotifyCharacteristic;
    // If a given GATT characteristic is selected, check for supported features.  This sample
    // demonstrates 'Read' and 'Notify' features.  See
    // http://d.android.com/reference/android/bluetooth/BluetoothGatt.html for the complete
    // list of supported characteristic features.
    private final ExpandableListView.OnChildClickListener servicesListClickListner =
            new ExpandableListView.OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                                            int childPosition, long id) {
                    if (mGattCharacteristics != null) {
                        final BluetoothGattCharacteristic characteristic =
                                mGattCharacteristics.get(groupPosition).get(childPosition);
                        final int charaProp = characteristic.getProperties();
                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
                            // If there is an active notification on a characteristic, clear
                            // it first so it doesn't update the data field on the user interface.
                            if (mNotifyCharacteristic != null) {
                                mBluetoothLeService.setCharacteristicNotification(
                                        mNotifyCharacteristic, true);
                                mNotifyCharacteristic = null;
                            }
                            mBluetoothLeService.readCharacteristic(characteristic);
                        }
                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                            mNotifyCharacteristic = characteristic;
                            mBluetoothLeService.setCharacteristicNotification(
                                    characteristic, true);
                        }
                        return true;
                    }
                    return false;
                }
            };

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BluetoothLeService.EXTRA_DATA);
        return intentFilter;
    }

//    private void clearUI() {
//        //mGattServicesList.setAdapter((SimpleExpandableListAdapter) null);
//        mDataField.setText(R.string.no_data);
//    }

    /**
     * Send RTC Time
     * @param ctx
     */


    public void sendRTCtime (Context ctx) {

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Log.v(TAG, "UUUU Update RTC Time Event");
        if (DeviceControlActivity.mBluetoothLeService!=null) {
            Log.v(TAG, "UUUU Update RTC Time Event step 2");
            Calendar c = Calendar.getInstance();
            //c.add(Calendar.MONTH, 1);
            int seconds = c.get(Calendar.SECOND);
            int date = c.get(Calendar.DATE);
            int hours = c.get(Calendar.HOUR_OF_DAY);
            int min = c.get(Calendar.MINUTE);
            int month = c.get(Calendar.MONTH)+1;
//                    int year = c.get(Calendar.YEAR);

            int dayofmonth=c.get((Calendar.DAY_OF_WEEK));
            Log.d("Dayofmonth","::"+dayofmonth);

            String thisYear = new SimpleDateFormat("yy").format(new Date());
            int year = Integer.parseInt(thisYear);
            Log.v(TAG, "UUUU Update RTC Time Event step date : "+ date);
            Log.v(TAG, "UUUU Update RTC Time Event step : month : "+ month);
            Log.v(TAG, "UUUU Update RTC Time Event step : year : "+ year);
            Log.v(TAG, "UUUU Update RTC Time Event step : thisYear : "+ Integer.parseInt(thisYear));
            Log.v(TAG, "UUUU Update RTC Time Event step : hours : "+ hours);
            Log.v(TAG, "UUUU Update RTC Time Event step : min : "+ min);
            Log.v(TAG, "UUUU Update RTC Time Event step : se"+ seconds);
            Log.v(TAG, "UUUU Update RTC Time Event step : seconds : "+ seconds);

            byte[] BYTE_SEND_RTC_TIME = {0x41, 0x54, 0x07, 0x01, (byte) year,
                    (byte) month, (byte) date,(byte) hours, (byte) min, (byte) seconds,
                    0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,
                    0x20, 0x0D, 0x0A};
            try {
                DeviceControlActivity.mBluetoothLeService.writeCustomCharacteristic(BYTE_SEND_RTC_TIME);
                Log.v(TAG, "UUUU Write function 71...");
                StringBuilder sb = new StringBuilder();
                for (byte b : BYTE_SEND_RTC_TIME) {
                    if (sb.length() > 0) {
                        sb.append(':');
                    }
                    sb.append(String.format("%02x", b));
                }

                Log.v(TAG, "UUUU DEVICE COMMUNICATION ***** Received Value from sb : "+ sb);
                //Toast.makeText(ctx, "RTC Time Command Send Successfully (Write function 71.)\n"+sb, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(ctx, "Bluetooth Connection problem", Toast.LENGTH_SHORT).show();
            }
        }
        DeviceControlActivity.progressDialog.dismiss();



    }

    /**
     * Send Request for Switch Status
     * @param ctx
     * @return
     */

    public boolean requestSwitchStatus (Context ctx) {
        try {
            Log.v("UUUUU", "Before 2 sec : ");
            Thread.sleep(2000);
            Log.v("UUUUU", "after 2 sec : ");

            if(DeviceControlActivity.mBluetoothLeService != null) {
                byte[] BYTE_SEND_RTC_TIME = {0x41, 0x54, 0x03, 0x03, 0x20,
                        0x20, 0x20, 0x20, 0x20, 0x20,
                        0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,
                        0x20, 0x0D, 0x0A};
                try {
                    mBluetoothLeService.writeCustomCharacteristic(BYTE_SEND_RTC_TIME);
                    Log.v(TAG, "UUUUU Write function 33...");
                    StringBuilder sb = new StringBuilder();
                    for (byte b : BYTE_SEND_RTC_TIME) {
                        if (sb.length() > 0) {
                            sb.append(':');
                        }
                        sb.append(String.format("%02x", b));
                    }
                } catch (Exception e) {

                }
            }
            return true;
        } catch (InterruptedException e) {
            e.printStackTrace();
            Log.v("UUUUU", "Exception in write 33 : "+ e);
            return false;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.button_control);



        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_deviceControl);
        text_state = (TextView) toolbar.findViewById(R.id.txt_connection_stat);
        setSupportActionBar(toolbar);
        //getActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressDialog = new ProgressDialog(DeviceControlActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Fetching Board Status..");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        // converting character to ASCII value in Java
         char A = 'A';
        int ascii = A;
        System.out.println("ASCII value of 'A' is : " + ascii);


        final Intent intent = getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);

        // Sets up UI references.
        mDataField = (TextView) findViewById(R.id.data_value);

//        getActionBar().setTitle(mDeviceName);

        mSwitchControl= (Button) findViewById(R.id.btn_switch_activity);
        Button mLogRequest = (Button) findViewById(R.id.btn_log_activity);
        Button mMobileConfiguration = (Button) findViewById(R.id.btn_mobile_activity);
        Button mModeSetting = (Button) findViewById(R.id.btn_mode_activity);
        Button mRtcTime = (Button) findViewById(R.id.btn_rtc_time);

        SharedPreferences sharedpreferences_login = getSharedPreferences(LC108DApplication.PREF_LOGIN_DETAILS, Context.MODE_PRIVATE);
        String login_status = sharedpreferences_login.getString(LC108DApplication.PREF_LOGIN_STATUS, "");
        String user_type = sharedpreferences_login.getString(LC108DApplication.PREF_LOGIN_USER_TYPE, "");
        String login_user = sharedpreferences_login.getString(LC108DApplication.PREF_LOGIN_USER, "");
        ImageButton eepromHiddenButton = (ImageButton) toolbar.findViewById(R.id.btn_eeprom_hidden);


        mBtnsetserial = (Button) findViewById(R.id.btn_serialno);

        tvserialno = (TextView) findViewById(R.id.tvserialno);

        etserialno = (EditText)findViewById(R.id.edtserialno);
        String code = "LC108D";
        etserialno.setText(code);
        etserialno.setInputType(InputType.TYPE_NULL);
        Selection.setSelection(etserialno.getText(), etserialno.getText().length());



        saveserialno = (Button) findViewById(R.id.btnsaveserial);

        if(user_type.equals(Login_Table.USER)) {
            mMobileConfiguration.setVisibility(View.GONE);
            mModeSetting.setVisibility(View.GONE);
            mRtcTime.setVisibility(View.GONE);
            eepromHiddenButton.setVisibility(View.GONE);
            mBtnsetserial.setVisibility(View.GONE);
            tvserialno.setVisibility(View.GONE);
            etserialno.setVisibility(View.GONE);
        }

        Button btn_request_board_status = (Button) findViewById(R.id.btn_request_board_status);
        btn_request_board_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v(TAG, "UUUUU LC108DApplication.FLAG_REQUEST_DEVICE_STATUS onResume : "+ LC108DApplication.FLAG_REQUEST_DEVICE_STATUS);
                if(LC108DApplication.FLAG_REQUEST_DEVICE_STATUS == 1) {
                    Log.v(TAG, "UUUUU LC108DApplication.FLAG_REQUEST_DEVICE_STATUS if ");

                    if (mBluetoothLeService != null) {
                        byte[] BYTE_SEND_RTC_TIME = {0x41, 0x54, 0x03, 0x01, 0x20,
                                0x20, 0x20, 0x20, 0x20, 0x20,
                                0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,
                                0x20, 0x0D, 0x0A};
                        try {
                            mBluetoothLeService.writeCustomCharacteristic(BYTE_SEND_RTC_TIME);
                            Log.v(TAG, "UUUUU Write function 31... success");
                            Toast.makeText(getApplicationContext(), "Write function 31... success", Toast.LENGTH_SHORT).show();
                            LC108DApplication.FLAG_REQUEST_DEVICE_STATUS = 0;
                            StringBuilder sb = new StringBuilder();
                            for (byte b : BYTE_SEND_RTC_TIME) {
                                if (sb.length() > 0) {
                                    sb.append(':');
                                }
                                sb.append(String.format("%02x", b));
                            }


                        } catch (Exception e) {
                            Log.v(TAG, "UUUUU Write function 31... failed");
                        }
                    }
                }
            }
        });

        eepromHiddenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog addNoteDialog = new Dialog(DeviceControlActivity.this);
                addNoteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                addNoteDialog.setContentView(R.layout.activity_getting_password);
                addNoteDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

                ImageButton saveConfiguration = (ImageButton) addNoteDialog.findViewById(R.id.save_configuration);

                final EditText EditPassword = (EditText) addNoteDialog.findViewById(R.id.eeprom_password_edit);
                saveConfiguration.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //save configuration click event
                        if(!EditPassword.getText().toString().isEmpty() &&
                                EditPassword.getText().toString().equals("qmax@12345")) {
                            if (mBluetoothLeService!=null) {
                                byte[] BYTE_SEND_RTC_TIME = {0x41, 0x54, 0x08, 0x01, 0x20,
                                        0x20, 0x20, 0x20, 0x20, 0x20,
                                        0x20, 0x20, 0x20,0x20, 0x20, 0x20, 0x20,
                                        0x20, 0x0D, 0x0A};
                                try {
                                    mBluetoothLeService.writeCustomCharacteristic(BYTE_SEND_RTC_TIME);
                                    Log.v(TAG, "Write function 81...");
                                    Toast.makeText(getApplicationContext(), "Update Mode settings Command Send Successfully (Write function 81.", Toast.LENGTH_SHORT).show();
                                    addNoteDialog.dismiss();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Bluetooth Problem to send command", Toast.LENGTH_SHORT).show();
                                    addNoteDialog.dismiss();
                                }
                            }
                        } else {
//                            if (EditPassword.getText().toString().isEmpty()) {
//                                EditPassword.setError("password field should not empty");
//                            } else {
                                Toast.makeText(getApplicationContext(), "Unauthorized Access ", Toast.LENGTH_SHORT).show();
                                addNoteDialog.dismiss();
                             }
                    }
                });
                addNoteDialog.show();
            }
        });


        mRtcTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v(TAG, "NNNNN Update RTC Time Event");
                if (mBluetoothLeService!=null) {
                    Log.v(TAG, "NNNNNN Update RTC Time Event step 2");
                    Calendar c = Calendar.getInstance();

                    DateFormatSymbols dfs=new DateFormatSymbols(Locale.getDefault());
                    String week[]=dfs.getWeekdays();

                    c.add(Calendar.MONTH, 1);
                    int seconds = c.get(Calendar.SECOND);

                    int date = c.get(Calendar.DATE);
                    int dayofmonth=c.get((Calendar.DAY_OF_WEEK));
                    Log.d("Dayofmonth","::"+dayofmonth);
                    String day=week[dayofmonth];
                    Log.d("Nameofday","::"+day);

                    int hours = c.get(Calendar.HOUR);
                    int min = c.get(Calendar.MINUTE);
                    int month = c.get(Calendar.MONTH);
//                    int year = c.get(Calendar.YEAR);
                    String thisYear = new SimpleDateFormat("yy").format(new Date());
                    int year = Integer.parseInt(thisYear);
                    Log.v(TAG, "MBBB Update RTC Time Event step date : "+ date);
                    Log.v(TAG, "MBBB Update RTC Time Event step : month : "+ month);
                    Log.v(TAG, "MBBB Update RTC Time Event step : year : "+ year);
                    Log.v(TAG, "MBBB Update RTC Time Event step : thisYear : "+ Integer.parseInt(thisYear));
                    Log.v(TAG, "MBBB Update RTC Time Event step : hours : "+ hours);
                    Log.v(TAG, "MBBB Update RTC Time Event step : min : "+ min);
                    Log.v(TAG, "MBBB Update RTC Time Event step : se"+ seconds);
                    Log.v(TAG, "MBBB Update RTC Time Event step : seconds : "+ seconds);

                    byte[] BYTE_SEND_RTC_TIME = {0x41, 0x54, 0x07, 0x01, (byte) year,
                            (byte) month, (byte) date, (byte)dayofmonth,(byte) hours, (byte) min, (byte) seconds,
                            0x20, 0x20, 0x20,0x20, 0x20, 0x20,
                            0x20, 0x0D, 0x0A};
                    try {
                        mBluetoothLeService.writeCustomCharacteristic(BYTE_SEND_RTC_TIME);
                        Log.v(TAG, "Write function 51...");
                        StringBuilder sb = new StringBuilder();
                        for (byte b : BYTE_SEND_RTC_TIME) {
                            if (sb.length() > 0) {
                                sb.append(':');
                            }
                            sb.append(String.format("%02x", b));
                        }

                        Log.v(TAG, "MBBB DEVICE COMMUNICATION ***** Received Value from sb : "+ sb);
                        Toast.makeText(getApplicationContext(), "Update Mode settings Command Send Successfully (Write function 71.)\n"+sb, Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Bluetooth Connection problem", Toast.LENGTH_SHORT).show();
                    }
                }

                ////////////////////////////////
//                Log.v(TAG, "UUUUU LC108DApplication.FLAG_REQUEST_DEVICE_STATUS onResume : "+ LC108DApplication.FLAG_REQUEST_DEVICE_STATUS);
//                if(LC108DApplication.FLAG_REQUEST_DEVICE_STATUS == 1) {
//                    Log.v(TAG, "UUUUU LC108DApplication.FLAG_REQUEST_DEVICE_STATUS if ");
//
//                    if (mBluetoothLeService != null) {
//                        byte[] BYTE_SEND_RTC_TIME = {0x41, 0x54, 0x03, 0x01, 0x20,
//                                0x20, 0x20, 0x20, 0x20, 0x20,
//                                0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,
//                                0x20, 0x0D, 0x0A};
//                        try {
//                            mBluetoothLeService.writeCustomCharacteristic(BYTE_SEND_RTC_TIME);
//                            Log.v(TAG, "UUUUU Write function 31... success");
//                            StringBuilder sb = new StringBuilder();
//                            for (byte b : BYTE_SEND_RTC_TIME) {
//                                if (sb.length() > 0) {
//                                    sb.append(':');
//                                }
//                                sb.append(String.format("%02x", b));
//                            }
//                        } catch (Exception e) {
//                            Log.v(TAG, "UUUUU Write function 31... failed");
//                        }
//                    }
//                }

                //////////////////////////////////////////////////////////////////////////////////////////
            }
        });



        ////////////////////








        mSwitchControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SwitchActivity.class);
                SharedPreferences sharedpreferences = getSharedPreferences(LC108DApplication.PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor_seq_no = sharedpreferences.edit();
                editor_seq_no.clear();
                editor_seq_no.putString(LC108DApplication.PREF_DEVICE_NAME, mDeviceName);
                editor_seq_no.putString(LC108DApplication.PREF_DEVICE_ADDRESS, mDeviceAddress);
                editor_seq_no.commit();
                SharedPreferences sharedpreferences1 = getSharedPreferences(LC108DApplication.PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
                String mDeviceName = sharedpreferences1.getString(LC108DApplication.PREF_DEVICE_NAME, "");
                String mDeviceAddress = sharedpreferences1.getString(LC108DApplication.PREF_DEVICE_ADDRESS, "");
                Log.v(TAG, "sssss mDeviceName :" + mDeviceName + "mDeviceAddress : " + mDeviceAddress);
                intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_NAME, mDeviceName);
                intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_ADDRESS, mDeviceAddress);
                startActivity(intent);
            }
        });

        mLogRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LogActivity.class);
                SharedPreferences sharedpreferences = getSharedPreferences(LC108DApplication.PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor_seq_no = sharedpreferences.edit();
                editor_seq_no.clear();
                editor_seq_no.putString(LC108DApplication.PREF_DEVICE_NAME, mDeviceName);
                editor_seq_no.putString(LC108DApplication.PREF_DEVICE_ADDRESS, mDeviceAddress);
                editor_seq_no.commit();
                SharedPreferences sharedpreferences1 = getSharedPreferences(LC108DApplication.PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
                String mDeviceName = sharedpreferences1.getString(LC108DApplication.PREF_DEVICE_NAME, "");
                String mDeviceAddress = sharedpreferences1.getString(LC108DApplication.PREF_DEVICE_ADDRESS, "");
                Log.v(TAG, "sssss mDeviceName :" + mDeviceName + "mDeviceAddress : " + mDeviceAddress);
                intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_NAME, mDeviceName);
                intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_ADDRESS, mDeviceAddress);
                startActivity(intent);
            }
        });

        mMobileConfiguration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MobileDetails.class);
                SharedPreferences sharedpreferences = getSharedPreferences(LC108DApplication.PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor_seq_no = sharedpreferences.edit();
                editor_seq_no.clear();
                editor_seq_no.putString(LC108DApplication.PREF_DEVICE_NAME, mDeviceName);
                editor_seq_no.putString(LC108DApplication.PREF_DEVICE_ADDRESS, mDeviceAddress);
                editor_seq_no.commit();
                SharedPreferences sharedpreferences1 = getSharedPreferences(LC108DApplication.PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
                String mDeviceName = sharedpreferences1.getString(LC108DApplication.PREF_DEVICE_NAME, "");
                String mDeviceAddress = sharedpreferences1.getString(LC108DApplication.PREF_DEVICE_ADDRESS, "");
                Log.v(TAG, "sssss mDeviceName :" + mDeviceName + "mDeviceAddress : " + mDeviceAddress);
                intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_NAME, mDeviceName);
                intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_ADDRESS, mDeviceAddress);
                startActivity(intent);
            }
        });

        mModeSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ModeActivity.class);
                SharedPreferences sharedpreferences = getSharedPreferences(LC108DApplication.PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor_seq_no = sharedpreferences.edit();
                editor_seq_no.clear();
                editor_seq_no.putString(LC108DApplication.PREF_DEVICE_NAME, mDeviceName);
                editor_seq_no.putString(LC108DApplication.PREF_DEVICE_ADDRESS, mDeviceAddress);
                editor_seq_no.commit();
                SharedPreferences sharedpreferences1 = getSharedPreferences(LC108DApplication.PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
                String mDeviceName = sharedpreferences1.getString(LC108DApplication.PREF_DEVICE_NAME, "");
                String mDeviceAddress = sharedpreferences1.getString(LC108DApplication.PREF_DEVICE_ADDRESS, "");
                Log.v(TAG, "sssss mDeviceName :" + mDeviceName + "mDeviceAddress : " + mDeviceAddress);
                intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_NAME, mDeviceName);
                intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_ADDRESS, mDeviceAddress);
                startActivity(intent);
            }
        });




        mBtnsetserial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(DeviceControlActivity.this);
                    alertDialog.setTitle("Password");
                    alertDialog.setMessage("Enter Password");

                    final EditText input = new EditText(DeviceControlActivity.this);
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT);
                    input.setLayoutParams(lp);
                    alertDialog.setView(input);

                    alertDialog.setPositiveButton("YES",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    String password = input.getText().toString();

                                        if ("rcs2017".equals(password)) {
                                            Toast.makeText(getApplicationContext(),
                                                    "Password Matched", Toast.LENGTH_SHORT).show();



                                            etserialno.setClickable(true);
                                            etserialno.requestFocus();
                                            etserialno.setEnabled(true);
                                            etserialno.setInputType(InputType.TYPE_CLASS_NUMBER);


                                            etserialno.setFocusable(true);

                                        } else {
                                            Toast.makeText(getApplicationContext(),
                                                    "Wrong Password!", Toast.LENGTH_SHORT).show();

                                    }
                                }
                            });

                    alertDialog.setNegativeButton("NO",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });

                    alertDialog.show();

            }


        });
        etserialno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().startsWith("LC108D")) {
                    etserialno.setText("LC108D");
                    Selection.setSelection(etserialno.getText(), etserialno
                            .getText().length());
                }


                if(s.toString().length()==12)
                {
                    saveserialno.setEnabled(true);
                }
                else
                {
                    saveserialno.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().startsWith("LC108D")) {
                    etserialno.setText("LC108D");
                    Selection.setSelection(etserialno.getText(), etserialno
                            .getText().length());
                }

                if(s.toString().length()>12)
                {
                    etserialno.setError("Serial no accepts 6 digits only");
                    etserialno.setText(s.toString().substring(0, s.toString().length() - 1));
                }
                if(s.toString().length()==12)
                {
                    saveserialno.setEnabled(true);
                }
                else
                {
                    saveserialno.setEnabled(false);
                }
            }
        });

        saveserialno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ////////////////////////

                if(etserialno.getText().toString().length()==12) {


                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(DeviceControlActivity.this);


                    alertDialog.setTitle("Confirm set serial no : "+etserialno.getText().toString());


                    alertDialog.setPositiveButton("YES",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {


                                    String serialno = etserialno.getText().toString().substring(6, 12);

                                    int serialint = Integer.parseInt(serialno);

                                    Log.d(TAG, "Write function serial serialint.." + serialint);

                                    Log.d(TAG, "Write function serial serialno.." + serialno);


                                    int arrayint[] = new int[20];

                                    arrayint[5] = (serialint % 1000000) / 100000;

                                    arrayint[6] = (serialint % 100000) / 10000;

                                    arrayint[7] = (serialint % 10000) / 1000;

                                    arrayint[8] = (serialint % 1000) / 100;

                                    arrayint[9] = (serialint % 100) / 10;

                                    arrayint[10] = (serialint % 10);

                                    Log.d(TAG, "Write function serial number.." + arrayint[5]);

                                    Log.d(TAG, "Write function serial number.." + arrayint[6]);

                                    Log.d(TAG, "Write function serial number.." + arrayint[7]);

                                    Log.d(TAG, "Write function serial number.." + arrayint[8]);

                                    Log.d(TAG, "Write function serial number.." + arrayint[9]);

                                    Log.d(TAG, "Write function serial number.." + arrayint[10]);

                                    byte[] bytes = new byte[15];

                                    for (int i = 5; i <= 10; i++) {
                                        if (arrayint[i] == 0) {
                                            bytes[i] = 0x00;
                                        }
                                        if (arrayint[i] == 1) {
                                            bytes[i] = 0x01;
                                        }
                                        if (arrayint[i] == 2) {
                                            bytes[i] = 0x02;
                                        }
                                        if (arrayint[i] == 3) {
                                            bytes[i] = 0x03;
                                        }
                                        if (arrayint[i] == 4) {
                                            bytes[i] = 0x04;
                                        }
                                        if (arrayint[i] == 5) {
                                            bytes[i] = 0x05;
                                        }
                                        if (arrayint[i] == 6) {
                                            bytes[i] = 0x06;
                                        }
                                        if (arrayint[i] == 7) {
                                            bytes[i] = 0x07;
                                        }
                                        if (arrayint[i] == 8) {
                                            bytes[i] = 0x08;
                                        }
                                        if (arrayint[i] == 9) {
                                            bytes[i] = 0x09;
                                        }

                                    }


                                    if (mBluetoothLeService != null) {
                   /* byte[] BYTE_SEND_SERIAL_NO = {0x09, 0x01, 0x4C, 0x43, 0x31,
                            0x30, 0x38, 0x44, bytes[5], bytes[6],
                            bytes[7], bytes[8], bytes[9],bytes[10], 0x00, 0x00, 0x00,
                            0x00, 0x0D, 0x0A};*/

                                        byte[] BYTE_SEND_SERIAL_NO = {0x41, 0x54, 0x09, 0x01, 0x4C, 0x43, 0x31,
                                                0x30, 0x38, 0x44, bytes[5], bytes[6],
                                                bytes[7], bytes[8], bytes[9], bytes[10], 0x20,
                                                0x20, 0x0D, 0x0A};

                                        StringBuilder sb1 = new StringBuilder();
                                        for (byte bb : BYTE_SEND_SERIAL_NO) {
                                            if (sb1.length() > 0) {
                                                sb1.append(':');
                                            }
                                            sb1.append(String.format("%02x", bb));
                                        }
                                        try {
                                            mBluetoothLeService.writeCustomCharacteristic(BYTE_SEND_SERIAL_NO);
                                            Log.v(TAG, "Write function 101 Write function serial number...." + sb1);
                                            Toast.makeText(getApplicationContext(), "Update Serial no settings Command Send Successfully (Write function).", Toast.LENGTH_SHORT).show();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            Toast.makeText(getApplicationContext(), "Bluetooth Problem to send command", Toast.LENGTH_SHORT).show();
                                        }
                                    }



                                    }

                            });

                    alertDialog.setNegativeButton("NO",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });

                    alertDialog.show();





                }



            }
        });




    }



    private void registerMyReceiver() {

        try
        {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(BROADCAST_ACTION);
            registerReceiver(myBroadCastReceiver, intentFilter);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }

    /**
     * MyBroadCastReceiver is responsible to receive broadCast from register action
     * */
    public class MyBroadCastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            try {
                Log.d(TAG, "onReceive() called");




                Cursor mCur = getApplicationContext().getContentResolver().query(ModeTimeTable.CONTENT_URI,
                        null,
                        null, null,
                        null);

                String checkNameMode = "";

                if (mCur != null && mCur.moveToFirst()) {
                    do {
                        boolean getStatus = mCur.getInt(mCur.getColumnIndex(ModeTimeTable.STATUS)) > 0;
                        Log.v("mmm", "Mode Name getStatus : " + getStatus);
                        //TO get the
                        if (getStatus) {
                            checkNameMode = mCur.getString(mCur.getColumnIndex(ModeTimeTable.MODE_NAME));

                            Log.v("mmm", "Device Mode Name if checkNameMode : " + checkNameMode);
                        } else {
                            Log.v("mmm", "Device Mode Name else  checkNameMode : " + checkNameMode);
                        }
                    } while (mCur.moveToNext());
                }

                if(checkNameMode.equals(ModeTimeTable.DUAL_LINE_MODE))
                {

                    mSwitchControl.setEnabled(false);
                }
                else
                {
                    mSwitchControl.setEnabled(true);
                }







                if (intent != null) {
                     dataReceived = intent.getStringExtra("data");

                    Log.d(TAG,"BluetoothLeService Display data received set serial no"+dataReceived);

                    if (dataReceived.equals("0")) {

                        String code = "LC108D000000";
                        etserialno.setText(code);
                        Log.d(TAG, "data==" + dataReceived);
                        Log.d(TAG,"BluetoothLeService Display data received set serial no if"+dataReceived);


                        Selection.setSelection(etserialno.getText(), etserialno.getText().length());


                        mBtnsetserial.setEnabled(true);
                        saveserialno.setVisibility(View.VISIBLE);
                        saveserialno.setEnabled(false);

                    }

                    else if(dataReceived.equals("disable"))
                    {
                        mBtnsetserial.setEnabled(false);
                        saveserialno.setEnabled(false);
                        saveserialno.setVisibility(View.GONE);

                        etserialno.setClickable(false);
                        etserialno.clearFocus();
                        etserialno.setFocusable(false);
                    }


                    else
                    {
                        StringBuilder serialnovalue  = new StringBuilder();

                        serialnovalue.append("LC108D");

                        serialnovalue.append(dataReceived);

                        Log.d(TAG,"BluetoothLeService Display data received set serial no else"+String.valueOf(serialnovalue));



                        Cursor valuetest = getApplicationContext().getContentResolver().
                                query(ModeTimeTable.CONTENT_URI,
                                        null,
                                        ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.TRACK_MODE},
                                        null);


                        if (valuetest != null && valuetest.moveToFirst()) {

                            String BOARD_SERIAL_NO = valuetest.getString(valuetest.getColumnIndex(ModeTimeTable.BOARD_SERIAL_NO));

                            Log.v("LC108D-LOG", "UUUUU 2 ModeTimeTable updated mode reset val"+BOARD_SERIAL_NO);

                            ContentValues values = new ContentValues();

                            values.put(ModeTimeTable.BOARD_SERIAL_NO, String.valueOf(serialnovalue));


                            getApplicationContext().getContentResolver().update(ModeTimeTable.CONTENT_URI, values, ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.TRACK_MODE});



                        }

                        Cursor valuetestread = getApplicationContext().getContentResolver().
                                query(ModeTimeTable.CONTENT_URI,
                                        null,
                                        ModeTimeTable.MODE_NAME + "=?", new String[]{ModeTimeTable.TRACK_MODE},
                                        null);


                        if (valuetestread != null && valuetestread.moveToFirst()) {

                            String BOARD_SERIAL_NO = valuetestread.getString(valuetestread.getColumnIndex(ModeTimeTable.BOARD_SERIAL_NO));

                            Log.v("LC108D-LOG", "UUUUU 2 ModeTimeTable updated mode reset val read "+BOARD_SERIAL_NO);




                        }




                        etserialno.setText(String.valueOf(serialnovalue));
                    }
                }



            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (mBluetoothLeService != null) {
            final boolean result = mBluetoothLeService.connect(mDeviceAddress);
            Log.d(TAG, "Connect request result=" + result);
        }
        BluetoothLeService.countheader = 0;

        myBroadCastReceiver = new MyBroadCastReceiver();
        registerMyReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();



        BluetoothLeService.countheader = 0;


        unregisterReceiver(mGattUpdateReceiver);
        unregisterReceiver(myBroadCastReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        unbindService(mServiceConnection);
        mBluetoothLeService = null;

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gatt_services, menu);
        if (mConnected) {
            menu.findItem(R.id.menu_connect).setVisible(false);
            menu.findItem(R.id.menu_disconnect).setVisible(true);
        } else {
            menu.findItem(R.id.menu_connect).setVisible(true);
            menu.findItem(R.id.menu_disconnect).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_connect:
                mBluetoothLeService.connect(mDeviceAddress);
                return true;
            case R.id.menu_disconnect:
                mBluetoothLeService.disconnect();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateConnectionState(final int resourceId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //mConnectionState.setText(resourceId);
            }
        });
    }

    private void displayData(String data) {
        if (data != null) {
            mDataField.setText(data);
        }
    }

    // Demonstrates how to iterate through the supported GATT Services/Characteristics.
    // In this sample, we populate the data structure that is bound to the ExpandableListView
    // on the UI.
    private void displayGattServices(List<BluetoothGattService> gattServices) {
        if (gattServices == null) return;
        String uuid = null;
        String unknownServiceString = getResources().getString(R.string.unknown_service);
        String unknownCharaString = getResources().getString(R.string.unknown_characteristic);
        ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<HashMap<String, String>>();
        ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData
                = new ArrayList<ArrayList<HashMap<String, String>>>();
        mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {
            HashMap<String, String> currentServiceData = new HashMap<String, String>();
            uuid = gattService.getUuid().toString();
            currentServiceData.put(
                    LIST_NAME, SampleGattAttributes.lookup(uuid, unknownServiceString));
            currentServiceData.put(LIST_UUID, uuid);
            gattServiceData.add(currentServiceData);

            ArrayList<HashMap<String, String>> gattCharacteristicGroupData =
                    new ArrayList<HashMap<String, String>>();
            List<BluetoothGattCharacteristic> gattCharacteristics =
                    gattService.getCharacteristics();
            ArrayList<BluetoothGattCharacteristic> charas =
                    new ArrayList<BluetoothGattCharacteristic>();

            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                charas.add(gattCharacteristic);
                HashMap<String, String> currentCharaData = new HashMap<String, String>();
                uuid = gattCharacteristic.getUuid().toString();
                currentCharaData.put(
                        LIST_NAME, SampleGattAttributes.lookup(uuid, unknownCharaString));
                currentCharaData.put(LIST_UUID, uuid);
                gattCharacteristicGroupData.add(currentCharaData);
            }
            mGattCharacteristics.add(charas);
            gattCharacteristicData.add(gattCharacteristicGroupData);
        }

        SimpleExpandableListAdapter gattServiceAdapter = new SimpleExpandableListAdapter(
                this,
                gattServiceData,
                android.R.layout.simple_expandable_list_item_2,
                new String[]{LIST_NAME, LIST_UUID},
                new int[]{android.R.id.text1, android.R.id.text2},
                gattCharacteristicData,
                android.R.layout.simple_expandable_list_item_2,
                new String[]{LIST_NAME, LIST_UUID},
                new int[]{android.R.id.text1, android.R.id.text2}
        );
        mGattServicesList.setAdapter(gattServiceAdapter);
    }

    public void onClickWrite(View v) {
        if (mBluetoothLeService != null) {
            //mBluetoothLeService.writeCustomCharacteristic(0xAA);
            byte[] BYTE_SEND_LOG_REQUEST = {0x41, 0x54, 0x02, 0x01, 0x03, 0x04, 0x05,
                    0x06, 0x07, 0x08, 0x09, 0x41,
                    0x56, 0x45, 0x66, 0x6d, 0x74,
                    0x20, 0x0D, 0x0A};

            mBluetoothLeService.writeCustomCharacteristic(BYTE_SEND_LOG_REQUEST);
        }
    }

    public void onClickRead(View v) {
        if (mBluetoothLeService != null) {
            mBluetoothLeService.readCustomCharacteristic();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }


}
